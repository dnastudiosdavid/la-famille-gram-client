import {IExercise} from "./exercise-interface";
import {EventEmitter} from "@angular/core";
import {IScoreComponent} from "./score-component-interface";
import {IActivity} from "./activity-interface";

export interface IGame {

  /**
   * An event indicating that the game's current progression changed.
   */
  progressionChanged: EventEmitter<void>;

  /**
   * An event indicating that the game's current progression changed.
   */
  ended: EventEmitter<IGameEndedType>;

  /**
   * Gets the hero chosen by the player.
   */
  getHero(): string;

  /**
   * Gets the student's nickname.
   */
  getStudent(): string;

  /**
   * Gets the mode.
   */
  getMode(): string;

  /**
   * Gets the game's exercise.
   */
  getExercise(): IExercise;

  /**
   * Gets the game's activity.
   */
  getActivity(): IActivity;

  /**
   * Used to know if the game is in progress.
   */
  isInProgress(): boolean;

  /**
   * TODO: document
   */
  isScoreLocked(): boolean;

  /**
   * Gets the game's score.
   */
  getScore(): number;

  /**
   * Gets the label used to understand the step / amount.
   */
  getProgressLabel(): string;

  /**
   * Gets the game's step.
   */
  getCurrentStep(): number;

  /**
   * Gets the maximum step.
   */
  getStepsAmount(): number;

  /**
   * Gets the previous results for this game.
   */
  getResults(): IScoreComponent[];

  /**
   * Starts the given exercise.
   */
  start(exercise: IExercise, hero: string, student: string, mode: string);

  /**
   * Locks the score so we can't access leaderboard.
   */
  lockScore(): void;
}

/**
 * Type of event emitted when the game ends.
 */
export interface IGameEndedType {

  /**
   * The filled activity.
   */
  activity: IActivity;
}
