import {Type} from "class-transformer";
import {IActivity} from "./activity-interface";
import {IActivityAction} from "./activity-action";

export abstract class ActivityAbstract implements IActivity {

  private readonly id: number;

  private readonly student: string;

  @Type(() => Date)
  private readonly date: Date;

  private duration: number = 0;

  private completionRate: number = 0;

  private errorRate: number = 0;

  private actions: IActivityAction[] = [];

  private readonly exercise: string;

  get Id(): number {
    return this.id || 10;
  }

  get Student(): string {
    return this.student;
  }

  get Date(): Date {
    return this.date;
  }

  get Duration(): number {
    return this.duration;
  }

  get CompletionRate(): number {
    return this.completionRate;
  }

  get ErrorRate(): number {
    return this.errorRate;
  }

  get Exercise(): string {
    return this.exercise;
  }

  protected constructor(student: string, exercise: string) {
    this.date = new Date();
    this.student = student;
    this.exercise = exercise;
  }

  /**
   * @inheritDoc
   */
  complete() {
    this.duration = Math.round((new Date().getTime() - this.date.getTime()) / 1000);
    this.completionRate = this.getCompletionRate();
    this.errorRate = this.getErrorRate();
  }

  abstract getCompletionRate(): number;

  abstract getErrorRate(): number;

  getDate(): Date {
    return this.date;
  }

  getStudent(): string {
    return this.student;
  }

  getDuration(): number {
    return this.duration;
  }

  addAction(action: IActivityAction): void {
    this.actions.push(action);
  }
}
