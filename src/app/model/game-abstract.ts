import {IGame, IGameEndedType} from "./game-interface";
import {IExercise} from "./exercise-interface";
import {EventEmitter} from "@angular/core";
import {IScoreComponent} from "./score-component-interface";
import {Exclude} from "class-transformer";
import {IActivity} from "./activity-interface";

export abstract class AbstractGame implements IGame {

  /**
   * @see IGame.progressionChanged
   */
  @Exclude()
  progressionChanged: EventEmitter<void> = new EventEmitter<void>();

  /**
   * @see IGame.ended
   */
  @Exclude()
  ended: EventEmitter<IGameEndedType> = new EventEmitter<IGameEndedType>();

  /**
   * The environment of the game.
   */
  protected hero: string;

  /**
   * The student's name.
   */
  protected student: string;

  /**
   * The game's mode.
   */
  protected mode: string;

  /**
   * The game's exercise.
   */
  protected exercise: IExercise;

  /**
   * What the score is made of.
   */
  protected results: IScoreComponent[] = [];

  /**
   * @inheritDoc
   */
  getHero(): string {
    return this.hero;
  }

  /**
   * @inheritDoc
   */
  getStudent(): string {
    return this.student;
  }

  /**
   * @inheritDoc
   */
  getMode(): string {
    return this.mode;
  }

  /**
   * @inheritDoc
   */
  getExercise(): IExercise {
    return this.exercise;
  }

  /**
   * @inheritDoc
   */
  getActivity(): IActivity {
    return null;
  }

  /**
   * @inheritDoc
   */
  abstract isInProgress();

  /**
   * @inheritDoc
   */
  abstract isScoreLocked(): boolean;

  /**
   * @inheritDoc
   */
  abstract getProgressLabel(): string;

  /**
   * @inheritDoc
   */
  abstract getScore(): number;

  /**
   * @inheritDoc
   */
  getCurrentStep(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  getStepsAmount(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  start(exercise: IExercise, hero: string, student: string, mode: string) {
    this.exercise = exercise;
    this.hero = hero;
    this.student = student;
    this.mode = mode;
  }

  /**
   * Locks the score - players won't be able to store any new score.
   */
  lockScore(): void {
  }

  /**
   * @inheritDoc
   */
  getResults(): IScoreComponent[] {
    return this.results;
  }

  /**
   * Adds a result.
   */
  protected addResult(result: IScoreComponent): void {
    this.results.push(result);
  }
}
