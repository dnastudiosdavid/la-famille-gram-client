export interface IScoreComponent {

  /**
   * Gets the label describing the score.
   */
  getLabel(): string;

  /**
   * Gets the score done.
   */
  getScore(): number;

  /**
   * Gets the max possible score.
   */
  getMaxScore(): number;
}
