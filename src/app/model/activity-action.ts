export interface IActivityAction {
  time: Date;
  scope: string;
  action: string;
  outcome: string;
  data: any;
}

export const GlobalScope = "global";

export enum GlobalActions {
  Save = "save",
  SpeechSynthesis = "speech-synthesis",
  AccessibleFont = "accessible-font",
}

export function makeAction(scope: string, action: string, outcome: string = "", data: any = null): IActivityAction {
  return {
    time: new Date(),
    scope: scope,
    action: action,
    outcome: outcome,
    data: data
  };
}

export function makeGlobalAction(action: string, outcome: string = "", data: any = null): IActivityAction {
  return makeAction(GlobalScope, action, outcome, data);
}

export function makeAccessibleFontAction(isActive: boolean): IActivityAction {
  return makeAction(GlobalScope, GlobalActions.AccessibleFont, isActive ? "enabled" : "disabled", {isActive: isActive});
}

// TODO: not in use atm
export class MouseTracker {
  static Position: [number, number];
}

export function initCursorTracker(): void {
  document.onmousemove = (evt) => {
    MouseTracker.Position = [evt.pageX, evt.pageY];
  };
}
