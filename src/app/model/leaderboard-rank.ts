/**
 * This entity is responsible for holding data about
 * a player result in the leaderboard.
 *
 * TODO: the ranks could be wrapped in an exercise.
 */
export class LeaderboardRank {

  /**
   * @see Nickname
   */
  private nickname: string;

  /**
   * @see Score
   */
  private score: number;

  /**
   * The nickname of the player.
   * @returns {string}
   */
  get Nickname(): string {
    return this.nickname;
  }

  /**
   * The player's score.
   * @returns {number}
   */
  get Score(): number {
    return this.score;
  }

  /**
   * Ctor.
   * @param {string} nickname
   * @param {number} score
   */
  constructor(nickname: string, score: number) {
    this.nickname = nickname;
    this.score = score;
  }
}
