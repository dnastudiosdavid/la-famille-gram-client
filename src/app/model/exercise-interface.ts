export interface IExercise {

  /**
   * The exercise's code.
   */
  readonly Code: string;

  /**
   * The exercise's name.
   */
  readonly Name: string;

  /**
   * The exercise's language.
   */
  readonly LanguageCode: string;

  /**
   * The exercise's degree.
   */
  readonly Degree: number;

  /**
   * Returns true if this exercise is locked.
   */
  readonly IsDefault: boolean;

  /**
   * True if we have a leaderboard with this exercise.
   */
  readonly HasLeaderboard: boolean;

  /**
   * Locks the exercise - meaning it becomes impossible
   * to remove it from the list.
   */
  lock(): void;
}
