/**
 * A queue is a FIFO data structure.
 */
export class SQueue<T> {

  /**
   * Array of objects stored in the queue.
   * @type {any[]}
   */
  private _store: T[] = [];

  /**
   * The length of the queue.
   * @returns {number}
   */
  get Count(): number {
    return this._store.length;
  }

  /**
   * Enqueues a new value.
   * @param {T} val
   */
  enqueue(val: T) {
    this._store.push(val);
  }

  /**
   * Dequeues.
   * @returns {T | undefined}
   */
  dequeue(): T | undefined {
    return this._store.shift();
  }
}
