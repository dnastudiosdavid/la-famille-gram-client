import {IExercise} from "./exercise-interface";

export abstract class AbstractExercise implements IExercise {

  /**
   * @see Code
   */
  protected code: string;

  /**
   * @see Name
   */
  protected name: string;

  /**
   * @see LanguageCode
   */
  protected language: string = "fr";

  /**
   * @see Degree
   */
  protected degree: number = -1;

  /**
   * @see IsDefault
   */
  protected isLocked: boolean = false;

  /**
   * The generated code for this exercise.
   */
  get Code(): string {
    return this.code;
  }

  /**
   * Sets the code.
   */
  set Code(value: string) {
    this.code = value;
  }

  /**
   * The name of the exercise.
   */
  get Name(): string {
    return this.name;
  }

  /**
   * Sets the name of this exercise.
   */
  set Name(value: string) {
    this.name = value;
  }

  /**
   * Returns true if the exercise is protected (comes from the defaults)
   */
  get IsDefault(): boolean {
    return this.isLocked;
  }

  /**
   * @inheritDoc
   */
  get HasLeaderboard(): boolean {
    return false;
  }

  /**
   * The exercise's language
   */
  get LanguageCode(): string {
    return this.language;
  }

  /**
   * The exercise's language
   */
  get Degree(): number {
    return this.degree;
  }

  /**
   * Locks the exercise (meaning it *should* not be
   * editable from the view).
   */
  lock() {
    this.isLocked = true;
  }
}
