import {IActivityAction} from "./activity-action";

/**
 * This interface describes an activity. An activity
 * holds all the data and progression of a completed
 * exercise.
 */
export interface IActivity {

  /**
   * Adds an action to the stack.
   */
  addAction(action: IActivityAction);

  /**
   * Gets the date and time when the
   * exercise was done.
   */
  getDate(): Date;

  /**
   * Gets the student's name.
   */
  getStudent(): string;

  /**
   * Gets the duration of the exercise in seconds.
   */
  getDuration(): number;

  /**
   * Gets the rate of completion. It corresponds
   * to the total amount of solutions found / the
   * total amount of possible solutions.
   */
  getCompletionRate(): number;

  /**
   * Gets the error rate. It corresponds to the
   * amount of errors / the amount of tries.
   */
  getErrorRate(): number;
}
