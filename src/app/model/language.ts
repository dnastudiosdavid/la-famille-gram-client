export class Language {
  private name: string;

  private code: string;

  private flag: string;

  private availableModes: string[] = [];

  get Name(): string {
    return this.name;
  }

  set Name(value: string) {
    this.name = value;
  }

  get Code(): string {
    return this.code;
  }

  set Code(value: string) {
    this.code = value;
  }

  get Flag(): string {
    return `${this.flag}.svg`;
  }

  set Flag(value: string) {
    this.flag = value;
  }

  get FlagCode(): string {
    return this.flag;
  }

  set FlagCode(value: string) {
    this.flag = value;
  }

  get AvailableModes(): string[] {
    return this.availableModes;
  }
}
