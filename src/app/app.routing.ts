import {HomeComponent} from "./views/home/home.component";
import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {PlayComponent} from "./views/play/play.component";
import {ReplayerComponent} from "./views/replayer/replayer.component";

export const routes = [
  {path: "", component: HomeComponent},
  {path: "play", component: PlayComponent},
  {path: "replay", component: ReplayerComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
