import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule} from "@angular/forms";
import {DragulaModule} from "ng2-dragula";
import {FlexLayoutModule} from "@angular/flex-layout";
import {AppComponent} from "./app.component";
import {HomeComponent} from "./views/home/home.component";
import {AppService} from "./services/app.service";
import {HttpClientModule} from "@angular/common/http";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {SaveSnackbarComponent} from "./components/save-snackbar/save-snackbar.component";
import {TiretteComponent} from "./components/tirette/tirette.component";
import {LeaderboardService} from "./services/leaderboard.service";
import {LoadDialogComponent} from "./components/load-dialog/load-dialog.component";
import {LoadingService} from "./services/loading.service";
import {SpinnerComponent} from "./components/spinner/spinner.component";
import {LocalStorageService} from "./services/local-storage.service";
import {LeaderboardComponent} from "./components/leaderboard/leaderboard.component";
import {SummaryComponent} from "./components/summary/summary.component";
import {LeaderboardDialogComponent} from "./components/leaderboard-dialog/leaderboard-dialog.component";
import {AppRoutingModule} from "./app.routing";
import {AppMaterialModule} from "./app.material.module";
import {GameTirettesComponent} from "./games/sentences/components/game-tirettes/game-tirettes.component";
import {PlayComponent} from "./views/play/play.component";
import {AnimationComponent} from "./components/animation/animation.component";
import {EnvironmentComponent} from "./components/environment/environment.component";
import {VikingComponent} from "./components/heroes/viking/viking.component";
import {GameScoreComponent} from "./components/game-score/game-score.component";
import {GirlComponent} from "./components/heroes/girl/girl.component";
import {GameGramClassComponent} from "./games/sentences/components/game-gram-class/game-gram-class.component";
import {GameChainsComponent} from "./games/sentences/components/game-chains/game-chains.component";
import {NgPipesModule} from "ngx-pipes";
import {BoyComponent} from "./components/heroes/boy/boy.component";
import {SolutionsDialogComponent} from "./components/solutions-dialog/solutions-dialog.component";
import {LoadSaveDialogComponent} from "./components/load-save-dialog/load-save-dialog.component";
import {GameTextComponent} from "./games/substitutes/components/game-text/game-text.component";
import {PlayInsertDirective} from "./views/play-insert.directive";
import {FlagComponent} from "./components/flag/flag.component";
import {LanguagesDropdownComponent} from "./components/languages-dropdown/languages-dropdown.component";
import {BaseChartDirective} from "ng2-charts";
import {GameSilhouettesComponent} from "./games/sentences/components/game-silhouettes/game-silhouettes.component";
import {ReplayerComponent} from "./views/replayer/replayer.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GameTirettesComponent,
    GameSilhouettesComponent,
    GameGramClassComponent,
    GameChainsComponent,
    PlayComponent,
    SaveSnackbarComponent,
    TiretteComponent,
    LoadDialogComponent,
    SpinnerComponent,
    LeaderboardComponent,
    SummaryComponent,
    LeaderboardDialogComponent,
    AnimationComponent,
    EnvironmentComponent,
    FlagComponent,
    SolutionsDialogComponent,
    VikingComponent,
    GirlComponent,
    BoyComponent,
    GameScoreComponent,
    LoadSaveDialogComponent,
    GameTextComponent,
    PlayInsertDirective,
    LanguagesDropdownComponent,
    ReplayerComponent
  ],
  imports: [
    AppMaterialModule,
    BaseChartDirective,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgPipesModule,
    AppRoutingModule,
    FormsModule,
    DragulaModule.forRoot(),
    FlexLayoutModule

  ],
  providers: [
    AppService,
    LeaderboardService,
    LoadingService,
    LocalStorageService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  exports: [
    TiretteComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
