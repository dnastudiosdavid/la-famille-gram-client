import {Directive, ViewContainerRef} from "@angular/core";

@Directive({
  selector: "[appPlayInsert]"
})
export class PlayInsertDirective {

  /**
   * Ctor.
   */
  constructor(public viewContainerRef: ViewContainerRef) {
  }
}
