import {Directive, Input} from "@angular/core";
import {EnvironmentComponent} from "../components/environment/environment.component";
import {GameScoreComponent} from "../components/game-score/game-score.component";

@Directive()
export class GameBaseComponent {

  /**
   * Reference to the environment.
   */
  @Input()
  environment: EnvironmentComponent;

  /**
   * Should we show the score?
   */
  @Input()
  showScore: boolean;

  /**
   * Should we allow the player to go back?
   */
  @Input()
  showBackButton: boolean;

  /**
   * The score panel.
   */
  @Input()
  score: GameScoreComponent;
}
