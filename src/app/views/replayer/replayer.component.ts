import {Component, Inject, OnInit} from "@angular/core";
import {DOCUMENT} from "@angular/common";
import {SentencesService} from "../../games/sentences/sentences.service";
import {Sentence} from "../../games/sentences/model/core/sentence";
import {SentencesExercise} from "../../games/sentences/model/core/sentences-exercise";
import {IActivityAction} from "../../model/activity-action";

@Component({
  selector: "replayer",
  templateUrl: "./replayer.component.html",
  styleUrl: "./replayer.component.scss"
})
export class ReplayerComponent implements OnInit {

  private window: Window;

  sentence: Sentence;

  actions: IActivityAction[];

  constructor(@Inject(DOCUMENT) document: Document,
              private sentencesService: SentencesService) {
    this.window = document.defaultView;
  }

  ngOnInit(): void {
    this.window.addEventListener("message", (evt) => this.receiveMessage(evt), false);
  }

  private receiveMessage(event: any): void {
    // TODO: this is complete temporary code
    if (!event.data.exercise?.code) {
      return;
    }
    const code = event.data.exercise.code;
    this.actions = this.getActions(event.data.actions);
    this.sentencesService.loadExercise(code).then(ex => this.sentence = (<SentencesExercise>ex).Sentences[0]);
  }

  private getActions(raw: any[]): IActivityAction[] {
    const actions: IActivityAction[] = [];
    for (const rawAction of raw) {
      actions.push({
        time: new Date(rawAction.time),
        scope: rawAction.scope,
        action: rawAction.action,
        outcome: rawAction.outcome,
        data: rawAction.data
      });
    }
    return actions;
  }
}
