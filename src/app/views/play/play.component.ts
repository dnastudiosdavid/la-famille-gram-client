import {ApplicationRef, Component, ComponentFactoryResolver, OnDestroy, OnInit, RendererFactory2, Type, ViewChild, ViewEncapsulation} from "@angular/core";
import {LeaderboardService} from "../../services/leaderboard.service";
import {Router} from "@angular/router";
import {LoadingService} from "../../services/loading.service";
import {LeaderboardRank} from "../../model/leaderboard-rank";
import {SaveSnackbarComponent} from "../../components/save-snackbar/save-snackbar.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {EnvironmentComponent} from "../../components/environment/environment.component";
import {ILayoutSettings, LayoutService} from "../../services/layout.service";
import {Hep3DriverService, MessageType} from "../../services/hep3-driver.service";
import {filter} from "rxjs/operators";
import {Subscription} from "rxjs";
import {AppService} from "../../services/app.service";
import {IGame} from "../../model/game-interface";
import {SentencesService} from "../../games/sentences/sentences.service";
import {PlayInsertDirective} from "../play-insert.directive";
import {GameScoreComponent} from "../../components/game-score/game-score.component";
import {GameBaseComponent} from "../game-base.component";
import {SaveService} from "../../services/save.service";
import {IExercise} from "../../model/exercise-interface";
import {GlobalActions, GlobalScope, makeAction} from "../../model/activity-action";

/**
 * This view is the main view when playing.
 */
@Component({
  selector: "app-play",
  templateUrl: "./play.component.html",
  styleUrls: ["./play.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class PlayComponent implements OnInit, OnDestroy {

  /**
   * The current game.
   */
  game: IGame;

  /**
   * Whether the end is in summary view.
   */
  summarySelected: boolean;

  /**
   * The current leaderboard ranks.
   */
  ranks: LeaderboardRank[];

  /**
   * Subscription to the sentenceChanged event of the game.
   */
  progressionChangeSubscription: any;

  /**
   * Subscription to the ended event of the game.
   */
  endedSubscription: any;

  /**
   * The layout settings contains data on how to display
   * some components.
   */
  layoutSettings: ILayoutSettings;

  /**
   * Our subscription to hep3 driver.
   */
  hep3DriverSub: Subscription;

  /**
   * The environment.
   */
  @ViewChild("environment") environment: EnvironmentComponent;

  /**
   * Game container.
   */
  @ViewChild(PlayInsertDirective) insertPoint: PlayInsertDirective;

  /**
   * Game score
   */
  @ViewChild(GameScoreComponent) score: GameScoreComponent;

  /**
   * Ctor.
   */
  constructor(private appService: AppService,
              private leaderboardService: LeaderboardService,
              private saveService: SaveService,
              private sentencesService: SentencesService,
              private loadingService: LoadingService,
              private layoutService: LayoutService,
              private hepDriverService: Hep3DriverService,
              private router: Router,
              private snackBar: MatSnackBar,
              private rendererFactory2: RendererFactory2,
              private appRef: ApplicationRef,
              private componentFactoryResolver: ComponentFactoryResolver) {
    this.summarySelected = true;
    this.ranks = [];
  }

  /**
   * Initialization.
   */
  ngOnInit() {
    this.game = this.appService.Game;
    this.layoutSettings = this.layoutService.Settings;
    if (!this.game) {
      this.router.navigate(["/"]);
      return;
    } else {
      this.subscribeTo(this.game);
    }

    this.hep3DriverSub = this.hepDriverService.MessageStream.pipe(
      filter((m: MessageType) => m.type === "degree" || m.type === "mode")
    ).subscribe(m => {
      const degree = m.degree || 4;
      const languageCode = m.language || "fr";
      this.changeExercise(this.appService.GameService.getDefaultExerciseByDegree(degree, languageCode));
      this.hepDriverService.sendDegree(degree);
    });

    setTimeout(() => {
      this.injectGame(this.appService.GameService.ComponentType);
    }, 1);
  }

  /**
   * Releases resources.
   */
  ngOnDestroy() {
    this.unsubscribeFromGame();
    if (this.hep3DriverSub) {
      this.hep3DriverSub.unsubscribe();
    }
  }

  /**
   * Shows the leaderboard of the game.
   */
  showLeaderboard() {
    // get the leaderboard
    this.loadingService.show();
    this.leaderboardService.getLeaderboard(this.game.getExercise().Code, this.game.getMode()).then((ranks: LeaderboardRank[]) => {
      this.loadingService.hide();
      this.ranks = ranks;
    }).catch(() => {
      this.loadingService.hide();
      this.snackBar.openFromComponent(SaveSnackbarComponent, {
        data: {message: "Une erreur s'est produite, impossible de récupérer le classement!"},
        duration: 2000
      });
    });
    this.summarySelected = false;
  }

  /**
   * Shows the summary of the game.
   */
  showSummary() {
    this.summarySelected = true;
  }

  /**
   * Saves the current game.
   */
  save() {
    this.saveService.saveGame(this.game);
    this.game.getActivity().addAction(makeAction(GlobalScope, GlobalActions.Save));
  }

  /**
   * Handles the event raised by the summary component.
   */
  onScoreSavedHandler() {
    this.showLeaderboard();
  }

  /**
   * Switches the exercise - used when contained within the hep3 container.
   */
  changeExercise(code: string) {
    this.appService.GameService.loadExercise(code)
      .then((exercise: IExercise) => {
        const prevGame = this.game;
        this.game = null;
        if (prevGame != null) {
          setTimeout(() => {
            const hero = ["viking", "girl", "boy"][Math.floor(Math.random() * 3)];
            this.game = this.appService.GameService.startGame(exercise, hero, prevGame.getStudent(), prevGame.getMode());
            this.hepDriverService.sendStatsStart();
            this.subscribeTo(this.game);
            setTimeout(() => {
              this.injectGame(this.appService.GameService.ComponentType);
            }, 1);
          }, 10);
        }
      });
  }

  /**
   * Replays the current exercise.
   */
  replay() {
    this.game = this.appService.GameService.startGame(this.game.getExercise(), this.game.getHero(), this.game.getStudent(), this.game.getMode());
    this.subscribeTo(this.game);
    this.summarySelected = true;
    this.environment.getHero().wait();
    this.environment.setProgression(0);
    this.hepDriverService.sendStatsStart();
    setTimeout(() => {
      this.injectGame(this.appService.GameService.ComponentType);
    }, 1);
  }

  /**
   * Tells the top window to leave the game.
   */
  boundOut() {
    this.hepDriverService.back();
  }

  /**
   * Subscribes to the given game.
   */
  private subscribeTo(game: IGame) {
    this.unsubscribeFromGame();

    this.progressionChangeSubscription = game.progressionChanged.subscribe(() => {
      this.environment.setProgression((this.game.getCurrentStep() - 1) / Math.max(this.game.getStepsAmount() - 1, 1));
    });

    this.endedSubscription = game.ended.subscribe(() => {
      this.hepDriverService.sendStatsEnd(game.getActivity());
      this.environment.getHero().celebrate(false);
    });
  }

  /**
   * Removes any of the subscriptions.
   */
  private unsubscribeFromGame() {
    if (this.progressionChangeSubscription) {
      this.progressionChangeSubscription.unsubscribe();
    }
    if (this.endedSubscription) {
      this.endedSubscription.unsubscribe();
    }
  }

  /**
   * Injects the game at the directive's place.
   */
  private injectGame(type: Type<GameBaseComponent>) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(type);
    const viewContainerRef = this.insertPoint.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<GameBaseComponent>(componentFactory);
    componentRef.instance.environment = this.environment;
    componentRef.instance.score = this.score;
  }
}
