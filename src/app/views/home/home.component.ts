import {Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from "@angular/core";
import {App} from "../../model/app";
import {AppService} from "../../services/app.service";
import {SentencesExercise} from "../../games/sentences/model/core/sentences-exercise";
import {ActivatedRoute, Router} from "@angular/router";
import {LoadDialogComponent} from "../../components/load-dialog/load-dialog.component";
import {LoadingService} from "../../services/loading.service";
import {SaveSnackbarComponent} from "../../components/save-snackbar/save-snackbar.component";
import {IHeroComponent} from "../../components/heroes/hero-component-interface";
import {AnimationComponent} from "../../components/animation/animation.component";
import {LoadSaveDialogComponent} from "../../components/load-save-dialog/load-save-dialog.component";
import {ILayoutSettings, LayoutService} from "../../services/layout.service";
import {Hep3DriverService, MessageType} from "../../services/hep3-driver.service";
import {filter} from "rxjs/operators";
import {Observable, Subscription} from "rxjs";
import {SentencesService} from "../../games/sentences/sentences.service";
import {SubstitutesService} from "../../games/substitutes/substitutes.service";
import {IGameService} from "../../services/game-service-interface";
import {IExercise} from "../../model/exercise-interface";
import {IGame} from "../../model/game-interface";
import {SaveService} from "../../services/save.service";
import {SubstitutesGame} from "../../games/substitutes/model/substitutes-game";
import {Language} from "../../model/language";
import {LanguageService} from "../../services/languages.service";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";

/**
 * This is the default view of the app.
 */
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, OnDestroy {

  /**
   * The app model.
   */
  app: App;

  /**
   * List of available exercises for the chosen game.
   */
  exercises: IExercise[];

  /**
   * The selected exercise.
   */
  selectedExercise: IExercise;

  /**
   * The viking.
   */
  @ViewChild("viking") viking: IHeroComponent;

  /**
   * The girl.
   */
  @ViewChild("girl") girl: IHeroComponent;

  /**
   * The boy.
   */
  @ViewChild("boy") boy: IHeroComponent;

  /**
   * The animations of the modes.
   */
  @ViewChildren(AnimationComponent) modesAnimations: QueryList<AnimationComponent>;

  /**
   * For HEP3 container.
   */
  pendingGame: string = "sentences";

  /**
   * For HEP3 container.
   */
  pendingMode: string;

  /**
   * For HEP3 container.
   */
  pendingCode: string;

  /**
   * Where are we in the main menu?
   */
  step: Step = 0;

  /**
   * Selected game id.
   */
  selectedGame: string;

  /**
   * The selected game service.
   * TODO: rename
   */
  gameService: IGameService;

  /**
   * The selected game mode.
   */
  mode: string;

  /**
   * The selected hero.
   */
  hero: string;

  /**
   * The student's name.
   */
  student: string = "";

  /**
   * The selected exercise.
   */
  exercise: SentencesExercise;

  /**
   * The animation controller.
   */
  controller: any;

  /**
   * The layout settings contains data on how to display
   * some components.
   */
  layoutSettings: ILayoutSettings;

  /**
   * Our subscription to hep3 driver.
   */
  hep3DriverSub: Subscription;

  /**
   * Available languages.
   */
  languages$: Observable<Language[]>;

  /**
   * The selected language code.
   */
  languageCode: string = "fr";

  /**
   * Ctor.
   */
  constructor(private appService: AppService,
              private saveService: SaveService,
              private loadingService: LoadingService,
              private layoutService: LayoutService,
              private sentencesService: SentencesService,
              private substitutesService: SubstitutesService,
              private hepDriverService: Hep3DriverService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private languageService: LanguageService) {
  }

  /**
   * Temp
   */
  get SentencesExercise(): SentencesExercise {
    return this.selectedExercise instanceof SentencesExercise ? this.selectedExercise : null;
  }

  /**
   * Initialization of the component.
   */
  ngOnInit() {
    this.languages$ = this.languageService.getLanguages();
    this.layoutSettings = this.layoutService.Settings;
    this.checkAutoLaunch();
  }

  /**
   * Releases resources.
   */
  ngOnDestroy() {
    if (this.hep3DriverSub) {
      this.hep3DriverSub.unsubscribe();
    }
  }

  checkAutoLaunch() {
    this.appService.getApp().then((app: App) => {
      this.app = app;

      this.route.queryParams.subscribe(params => {
        let contained = false;
        if (params.contained) {
          contained = true;
        }

        this.layoutService.setSettings({
          contained: contained
        });

        if (params.mode) {
          this.pendingMode = params.mode;
        }

        if (params.game) {
          this.pendingGame = params.game;
        }

        if (params.code) {
          this.pendingCode = params.code;
        }

        this.hep3DriverSub = this.hepDriverService.MessageStream.pipe(
          filter((m: MessageType) => m.type === "setup")
        ).subscribe((m: MessageType) => {
          this.gameService = this.getService(this.pendingGame);
          const degree = m.degree;
          const codeToLoad = this.pendingCode ? this.pendingCode : this.gameService.getDefaultExerciseByDegree(degree, m.language || "fr");
          this.loadExerciseByCode(codeToLoad, (ex: IExercise) => {
            this.selectedExercise = ex;
            this.mode = this.pendingMode;
            this.hero = ["viking", "girl", "boy"][Math.floor(Math.random() * 3)];
            this.student = `GamesHub - ${m.nickname}`;
            this.hepDriverService.enable();
            this.hepDriverService.handleMode(m.mode);
            this.hepDriverService.sendDegree(Math.max(4, degree));
            this.hepDriverService.sendStatsStart();
            this.start(ex);
          });
        });
        this.layoutSettings = this.layoutService.Settings;
      });

      // DEV
      /* this.setGameService(this.substitutesService);
      this.substitutesService.getExercisesList().then(ex => {
        // ex[2].Code
        this.loadExerciseByCode("96_aaaa", (e: IExercise) => {
          this.selectedExercise = e;
          this.mode = "gram-class";
          this.hero = "girl";
          this.student = "Jean Valjean";
          this.start(e);
        });
      }); */
      // END DEV
    });
  }

  refreshExercises(): void {
    this.gameService.getExercisesList().then(ex => {
      if (this.selectedGame === "sentences") {
        this.exercises = ex.filter(e => e.LanguageCode === this.languageCode);
      } else {
        this.exercises = ex;
      }
    });
  }

  /**
   * Selects the given exercise.
   */
  selectExercise(exercise: IExercise) {
    this.loadExerciseByCode(exercise.Code, (ex: IExercise) => {
      this.selectedExercise = ex;
      if (this.gameService.hasModes()) {
        this.playModes();
        this.pauseHeroes();
        this.step = Step.ModeSelection;
      } else {
        this.step = Step.HeroSelection;
        this.playHeroes();
      }
    });
  }

  /**
   * Removes the given exercise.
   */
  removeExercise(exercise: IExercise) {
    this.gameService.removeExercise(exercise);
    this.refreshExercises();
  }

  /**
   * Loads an exercise.
   */
  addExercise() {
    const dialogRef: MatDialogRef<LoadDialogComponent> = this.dialog.open(LoadDialogComponent);
    dialogRef.afterClosed().subscribe(code => {
      if (code) {
        this.loadExerciseByCode(code, () => {
          this.refreshExercises();
        });
      }
    });
  }

  /**
   * Loads an exercise given its code.
   */
  loadExerciseByCode(code: string, cb: (exercise: IExercise) => void = null) {
    this.loadingService.show();
    this.gameService.loadExercise(code)
      .then((exercise: IExercise) => {
        if (cb) {
          cb(exercise);
        }
        this.loadingService.hide();
      })
      .catch((err) => {
        this.loadingService.hide();
        this.snackBar.openFromComponent(SaveSnackbarComponent, {
          data: {
            message: "Erreur de chargement. Avez-vous entré le bon code?"
          },
          duration: 2000
        });
      });
  }

  /**
   * Set a new game up.
   */
  newGame() {
    this.step = Step.GameSelection;
  }

  /**
   * Selects the game.
   */
  selectGame(game: string) {
    this.step = Step.ExerciseSelection;
    this.selectedGame = game;
    switch (game) {
      case "sentences":
        this.languageCode = "fr";
        this.setGameService(this.sentencesService);
        break;
      case "substitutes":
        this.setGameService(this.substitutesService);
        break;
    }
  }

  /**
   * Selects the game mode.
   */
  selectMode(mode: string) {
    this.mode = mode;
    this.step = Step.HeroSelection;
    this.pauseModes();
    this.playHeroes();
  }

  /**
   * Selects the hero.
   */
  selectHero(hero: string) {
    if (this.hero !== hero) {
      this.hero = hero;
      switch (hero) {
        case "viking":
          this.viking.cheer(true);
          break;
        case "girl":
          this.girl.cheer(true);
          break;
        case "boy":
          this.boy.cheer(true);
          break;
      }
    }
  }

  /**
   * Starts the exercise.
   */
  start(exercise: IExercise) {
    this.gameService.startGame(exercise, this.hero, this.student, this.mode);
    this.appService.setCurrentGameService(this.gameService);
    this.router.navigate(["/play"]);
  }

  /**
   * Goes back to the main menu
   */
  setStep(step: number) {
    this.step = step;
  }

  /**
   * Gets the exercises.
   */
  setGameService(service: IGameService) {
    this.gameService = service;
    this.refreshExercises();
  }

  /**
   * Goes back from hero selection to mode selection.
   */
  backToMode() {
    this.hero = null;
    this.mode = null;
    this.playModes();
    this.pauseHeroes();
    if (this.gameService.hasModes()) {
      this.step = Step.ModeSelection;
    } else {
      this.step = Step.ExerciseSelection;
    }
  }

  /**
   * Goes back from mode selection to exercise selection.
   */
  backToExercise() {
    this.selectedExercise = null;
    this.pauseModes();
    this.step = Step.ExerciseSelection;
  }

  /**
   * Opens the dialog to load a saved exercise.
   */
  loadSavedGame() {
    const dialogRef: MatDialogRef<LoadSaveDialogComponent> = this.dialog.open(LoadSaveDialogComponent);
    dialogRef.componentInstance.deleted.subscribe(game => {
      this.saveService.deleteSavedGame(game);
      dialogRef.componentInstance.savedGames = this.saveService.SavedGames;
    });
    dialogRef.componentInstance.savedGames = this.saveService.SavedGames;
    dialogRef.afterClosed().subscribe((game: IGame) => {
      if (!game) {
        return;
      }
      this.gameService = game instanceof SubstitutesGame ? this.substitutesService : this.sentencesService;
      this.appService.setCurrentGameService(this.gameService);
      this.gameService.setGame(game);

      this.router.navigate(["/play"]);
    });
  }

  /**
   * Handles when the selected language changes.
   */
  languageChangedHandler(language: Language) {
    this.languageCode = language.Code;
    this.refreshExercises();
  }

  /**
   * Pauses the animations of the modes.
   */
  private pauseModes() {
    this.modesAnimations.forEach((animation: AnimationComponent) => {
      animation.pause();
    });
  }

  /**
   * Plays the animations of the modes.
   */
  private playModes() {
    this.modesAnimations.forEach((animation: AnimationComponent) => {
      animation.resume();
    });
  }

  /**
   * Pauses the animations of the modes.
   */
  private pauseHeroes() {
    this.viking.pauseAnimation();
    this.boy.pauseAnimation();
    this.girl.pauseAnimation();
  }

  /**
   * Plays the animations of the modes.
   */
  private playHeroes() {
    this.viking.resumeAnimation();
    this.boy.resumeAnimation();
    this.girl.resumeAnimation();
  }

  /**
   * Gets the correct service given a game id.
   */
  private getService(game: string): IGameService {
    switch (game) {
      case "sentences":
        return this.sentencesService;
      case "substitutes":
        return this.substitutesService;
    }
    return null;
  }
}

enum Step {
  Main,
  GameSelection,
  ExerciseSelection,
  ModeSelection,
  HeroSelection
}
