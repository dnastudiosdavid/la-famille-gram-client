import {Component, ViewEncapsulation} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";

/**
 * This dialog let the user load an exercise into the app.
 */
@Component({
  selector: "app-load-dialog",
  templateUrl: "./load-dialog.component.html",
  styleUrls: ["./load-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class LoadDialogComponent {

  /**
   * The code of the exercise to load.
   */
  code: string;

  /**
   * Ctor.
   * @param {MatDialogRef<LoadDialogComponent>} dialogRef
   */
  constructor(private dialogRef: MatDialogRef<LoadDialogComponent>) {
  }

  /**
   * Validates the given input.
   */
  validate() {
    this.dialogRef.close(this.code);
  }

  /**
   * Cancels the operation and closes the dialog.
   */
  cancel() {
    this.dialogRef.close(null);
  }
}
