import {Component, ElementRef, Input, OnInit, ViewEncapsulation} from "@angular/core";

declare var bodymovin: any;

@Component({
  selector: "app-animation",
  templateUrl: "./animation.component.html",
  styleUrls: ["./animation.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class AnimationComponent implements OnInit {

  /**
   * The animation to play.
   */
  @Input()
  animation: string;

  /**
   * The default segment to play.
   */
  @Input()
  defaultSegment: number[];

  /**
   * True if the animation should play
   * automagically.
   */
  @Input()
  autoplay = false;

  /**
   * The delas before playing the first
   * animation (works only with defaultSegment
   * or autoplay).
   */
  @Input()
  delay = 0;

  /**
   * The animation controller.
   */
  controller: any;

  /**
   * The current chain controller.
   */
  chainCtrl: AnimationChain;

  /**
   * True if the animation already started once.
   */
  started: boolean;

  /**
   * Ctor.
   * @param {ElementRef} element
   */
  constructor(private element: ElementRef) {
    this.started = false;
  }

  /**
   * Initialization of the component.
   */
  ngOnInit() {
    this.bootstrap();
  }

  /**
   * Plays the given segment.
   *
   * @param segment
   * @param {boolean} force
   *
   * @return AnimationChain
   */
  playSegment(segment: number[], force: boolean): AnimationChain {
    const ctrl: AnimationChain = new AnimationChain();
    this.controller.playSegments(segment, force);
    this.chainCtrl = ctrl;
    return ctrl;
  }

  /**
   * Pauses the animation.
   */
  pause() {
    if (this.controller) {
      this.controller.pause();
    }
  }

  /**
   * Just plays the animation again.
   */
  resume() {
    if (this.started) {
      this.controller.play();
    } else {
      this.start();
    }
  }

  /**
   * Bootstraps the animation.
   */
  private bootstrap() {
    const self = this;
    const anim = bodymovin.loadAnimation({
      container: this.element.nativeElement,
      renderer: "svg",
      loop: true,
      autoplay: false,
      rendererSettings: {},
      path: `assets/animations/${this.animation}.json`
    });
    anim.addEventListener("DOMLoaded", function () {
      if (self.autoplay) {
        self.start();
      }
    });
    anim.addEventListener("loopComplete", function () {
      if (self.chainCtrl != null) {
        self.chainCtrl.notifiy();
        self.chainCtrl = null;
      }
    });
    this.controller = anim;
  }

  /**
   * Starts the animation for the first time.
   */
  private start() {
    if (!this.started) {
      this.started = true;
      if (this.delay <= 0) {
        this.playDefault();
      } else {
        setTimeout(() => {
          this.playDefault();
        }, this.delay);
      }
    }
  }

  /**
   * Plays the default situation.
   */
  private playDefault() {
    if (this.defaultSegment) {
      this.controller.playSegments(this.defaultSegment, true);
    } else {
      this.controller.play();
    }
  }
}

/**
 * This class is used to chain animations.
 */
export class AnimationChain {

  /**
   * The callback to raise.
   */
  callback: () => void;

  /**
   * Method raised just after the animation asked has looped.
   * @param {() => void} callback
   */
  then(callback: () => void) {
    this.callback = callback;
  }

  /**
   * Notifies the caller that his request
   * has ben fulfilled.
   */
  notifiy() {
    if (this.callback) {
      this.callback();
    }
  }
}
