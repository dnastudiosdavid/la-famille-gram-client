import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation} from "@angular/core";
import {LeaderboardService} from "../../services/leaderboard.service";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {LeaderboardDialogComponent} from "../leaderboard-dialog/leaderboard-dialog.component";
import {LoadingService} from "../../services/loading.service";
import {animate, style, transition, trigger} from "@angular/animations";
import {IGame} from "../../model/game-interface";
import {AppService} from "../../services/app.service";
import {Chart, registerables} from "chart.js";

/**
 * This component displays a summary about the player's game.
 */
@Component({
  selector: "app-summary",
  templateUrl: "./summary.component.html",
  styleUrls: ["./summary.component.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger(
      "opening",
      [
        transition(
          ":enter", [
            style({transform: "translateY(-100%)"}),
            animate("250ms", style({transform: "translateY(0)"}))
          ]
        )]
    )
  ]
})
export class SummaryComponent implements OnInit, OnDestroy {

  /**
   * The current game.
   */
  @Input()
  game: IGame;

  /**
   * Should we restrict leaderboards / controls / ... ?
   */
  @Input()
  bound: boolean;

  /**
   * The event subscription to the game service.
   */
  subscription: any;

  /**
   * Event raised when the score has been saved.
   */
  @Output()
  scoreSaved: EventEmitter<void>;

  /**
   * Colours of the chart
   */
  lineChartColors: Array<any> = [{ // grey
    backgroundColor: "rgba(255,223,51,0.5)",
    borderColor: "rgba(255,188,50,1)",
    pointBackgroundColor: "rgba(68,186,80,1)",
    pointBorderColor: "#ffffff",
    pointHoverBackgroundColor: "#ffffff",
    pointHoverBorderColor: "rgba(148,159,177,0.8)"
  }];

  /**
   * Object holding the data displayed in the chart
   */
  lineChartData: Array<any> = [
    {
      data: [],
      ...this.lineChartColors[0]
    }
  ];

  /**
   * Labels (x axis) of the chart
   * TODO: define
   */
  lineChartLabels: Array<any> = ["", "", "", "", ""];

  /**
   * Options of the chart
   */
  lineChartOptions: any = {
    responsive: true,
    scales: {
      y: {
        min: 0,
        suggestedMax: 150,
        grid: {
          display: false
        }
      }
    },
    elements: {
      line: {
        tension: 0
      }
    }
  };

  /**
   * Ctor.
   */
  constructor(private appService: AppService,
              private leaderboardService: LeaderboardService,
              private loadingService: LoadingService,
              private dialog: MatDialog) {
    this.scoreSaved = new EventEmitter();
    Chart.register(...registerables);
  }

  /**
   * Initialization of the component.
   */
  ngOnInit() {
    // we assume the game is already
    // over at this point
    this.lineChartData[0].data = this.appService.GameService.getPreviousScores();
  }

  /**
   * Saves the score to the leaderboard of the exercise.
   */
  saveScore() {
    const dialogRef: MatDialogRef<LeaderboardDialogComponent> = this.dialog.open(LeaderboardDialogComponent);
    dialogRef.componentInstance.score = this.game.getScore();
    dialogRef.afterClosed().subscribe(nickname => {
      if (nickname) {
        this.loadingService.show();
        this.leaderboardService.saveScore(this.game, nickname).then(() => {
          this.loadingService.hide();
          this.scoreSaved.emit();
        });
      }
    });
  }

  /**
   * Destruction of the component.
   */
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
