import {Component, Inject, ViewEncapsulation} from "@angular/core";
import {MAT_SNACK_BAR_DATA} from "@angular/material/snack-bar";

/**
 * This component is used to show a message to
 * notify the user of an event (like a saved exercise,
 * a loading failure, ...)
 */
@Component({
  selector: "app-save-snackbar",
  templateUrl: "./save-snackbar.component.html",
  styleUrls: ["./save-snackbar.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class SaveSnackbarComponent {

  /**
   * The message displayed in the snackbar.
   */
  message: string;

  /**
   * Ctor.
   * @param data the data provided by other components
   */
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {
    this.message = data.message;
  }
}
