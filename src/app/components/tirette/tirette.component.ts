import {Component, EventEmitter, Input, NgZone, OnChanges, OnDestroy, Output, SimpleChanges, ViewEncapsulation} from "@angular/core";
import {Sentence} from "../../games/sentences/model/core/sentence";
import {StateInfo} from "../../app.constants";

// trick to get jQuery's $ - though ugly
// and should be imported via webpack
declare var window: any;

/**
 * This component wraps the tirette plugin written with jQuery.
 */
@Component({
  selector: "app-tirette",
  templateUrl: "./tirette.component.html",
  styleUrls: ["./tirette.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class TiretteComponent implements OnChanges, OnDestroy {

  /**
   * The state of the tirette.
   */
  @Input()
  stateInfo: StateInfo;

  /**
   * The sentence which is built in the tirette.
   */
  @Input()
  sentence: Sentence;

  /**
   * The current combination.
   */
  @Output()
  combination: string;

  /**
   * Event emitter when the selected combination changes.
   */
  @Output()
  onCombinationChanged = new EventEmitter<string>();

  /**
   * Event emitter when the selected combination changes.
   */
  @Output()
  onSpeechSynthesisUsed = new EventEmitter();

  /**
   * Reference to the prototype of the plugin.
   */
  plugin: any;

  /**
   * Ctor.
   * @param {NgZone} ngZone
   */
  constructor(private ngZone: NgZone) {
  }

  /**
   * Releases resources.
   */
  ngOnDestroy() {
    if (this.plugin) {
      this.plugin.release();
    }
  }

  /**
   * Requests to start the speech synthesis
   */
  speak() {
    this.onSpeechSynthesisUsed.emit();
  }

  /**
   * Catches changes from the zone.
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes.sentence) {
      this.plugin?.release();
      // trick the lifecycle because the DOM
      // only changes one tick after the
      // event has been raised
      setTimeout(() => {
        this.ngZone.runOutsideAngular(() => {
          this.plugin = window.$("#exercice").tirettes({
            onChanged: (combination: string) => {
              this.combination = combination;
              this.onCombinationChanged.emit(combination);
            }
          });
        });
      }, 1);
    }
  }
}
