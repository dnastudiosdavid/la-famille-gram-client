/**
 * This interface describes a hero component.
 */
export interface IHeroComponent {

  /**
   * Goes idle.
   */
  wait();

  /**
   * Cheers. This one is called when the player selects
   * the hero.
   */
  cheer(thenIdle: boolean);

  /**
   * Runs. Called when the environment advances.
   */
  run(thenIdle: boolean);

  /**
   * Schnetz. Called when the player makes a wrong choice.
   */
  schnetz(thenIdle: boolean);

  /**
   * Celebrates. This method is called when the player
   * ends an exercise.
   *
   * @param {boolean} thenIdle
   */
  celebrate(thenIdle: boolean);

  /**
   * Pauses the animation.
   */
  pauseAnimation();

  /**
   * Resume the animation.
   */
  resumeAnimation();
}
