import {IHeroComponent} from "./hero-component-interface";
import {Directive, Input, ViewChild} from "@angular/core";
import {AnimationComponent} from "../animation/animation.component";

/**
 * This is the base class for a hero component.
 */
@Directive()
export abstract class AbstractHeroComponent implements IHeroComponent {

  /**
   * Automagically plays the first animation?
   */
  @Input()
  autoplay: boolean;

  /**
   * The viking
   */
  @ViewChild("renderer") renderer: AnimationComponent;

  /**
   * The current state of our character.
   */
  state: string;

  /**
   * Array of animations.
   * @type any
   */
  protected animations = {
    "idle": [0, 0],
    "cheer": [[0, 0], [0, 0], [0, 0]],
    "run": [0, 0],
    "schnetz": [0, 0],
    "celebrate": [0, 0]
  };

  /**
   * @inheritDoc
   */
  cheer(thenIdle: boolean) {
    this.playAnimation("cheer", this.animations.cheer, thenIdle);
  }

  /**
   * @inheritDoc
   */
  run(thenIdle: boolean) {
    this.playAnimation("run", this.animations.run, thenIdle);
  }

  /**
   * @inheritDoc
   */
  celebrate(thenIdle: boolean) {
    this.playAnimation("celebrate", this.animations.celebrate, thenIdle);
  }

  /**
   * @inheritDoc
   */
  schnetz(thenIdle: boolean) {
    this.playAnimation("schnetz", this.animations.schnetz, thenIdle);
  }

  /**
   * @inheritDoc
   */
  wait() {
    if (this.state !== "idle") {
      this.state = "idle";
      this.renderer.playSegment(this.animations.idle, true);
    }
  }

  /**
   * @inheritDoc
   */
  pauseAnimation() {
    this.renderer.pause();
  }

  /**
   * @inheritDoc
   */
  resumeAnimation() {
    this.renderer.resume();
  }

  /**
   * Plays the given frames with the given key. This method tells
   * the renderer to run the keys
   *
   * @param {string} key
   * @param {any} frames
   * @param {boolean} thenIdle
   */
  private playAnimation(key: string, frames: any, thenIdle: boolean) {
    if (this.state !== key) {
      this.state = key;
      let chosenFrames: number[] = frames;

      if (frames.length > 0 && Array.isArray(frames[0])) {
        const index: number = Math.floor(Math.random() * frames.length);
        chosenFrames = frames[index];
      }

      this.renderer.playSegment(chosenFrames, true)
        .then(() => {
          // state can change between the call
          // and the callback so we need to
          // ensure the key is still the same!
          if (thenIdle && this.state === key) {
            this.wait();
          }
        });
    }
  }
}
