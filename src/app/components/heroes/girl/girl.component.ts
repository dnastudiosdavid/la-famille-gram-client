import {Component, ViewEncapsulation} from "@angular/core";
import {AbstractHeroComponent} from "../hero-component-abstract";

/**
 * This class handles the girl hero.
 *
 * -----------------*
 * Animation frames |
 * -----------------*
 * Idle: 0 -> 40 (idle)
 * Saute: 40 -> 81 (cheer)
 * Amour: 81 -> 146 (cheer)
 * Mega contente: 146 -> 214 (celebrate)
 * False: 214 -> 236 (schnetz)
 * Run: 236 -> 305 (run)
 * True: 305 -> 341 (cheer)
 */
@Component({
  selector: "app-girl",
  templateUrl: "./girl.component.html",
  styleUrls: ["./girl.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class GirlComponent extends AbstractHeroComponent {

  /**
   * Array of animations.
   * @type any
   */
  animations = {
    "idle": [0, 40],
    "cheer": [[40, 81], [81, 146], [305, 341]],
    "run": [236, 305],
    "schnetz": [214, 236],
    "celebrate": [146, 214]
  };
}
