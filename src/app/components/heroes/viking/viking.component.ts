import {Component, ViewEncapsulation} from "@angular/core";
import {AbstractHeroComponent} from "../hero-component-abstract";

/**
 * This class handles the viking hero.
 *
 * -----------------*
 * Animation frames |
 * -----------------*
 * Idle: 0 -> 47 (idle)
 * Saute: 47 -> 103 (celebrate)
 * Salut: 103 -> 176 (cheer)
 * Siffle: 176 -> 300 (cheer)
 * Run: 300 -> 387 (run)
 * False: 387 -> 404 (schnetz)
 * True: 404 -> 450 (cheer)
 */
@Component({
  selector: "app-viking",
  templateUrl: "./viking.component.html",
  styleUrls: ["./viking.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class VikingComponent extends AbstractHeroComponent {

  /**
   * Array of animations.
   * @type any
   */
  animations = {
    "idle": [0, 47],
    "cheer": [[103, 176], [176, 300], [404, 450]],
    "run": [300, 387],
    "schnetz": [387, 404],
    "celebrate": [47, 103]
  };
}
