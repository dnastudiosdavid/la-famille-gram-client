import {Component, ViewEncapsulation} from "@angular/core";
import {AbstractHeroComponent} from "../hero-component-abstract";

/**
 * This class handles the girl hero.
 *
 * -----------------*
 * Animation frames |
 * -----------------*
 * Idle: 0 -> 83 (idle)
 * Gym: 83 -> 181 (cheer)
 * Gâteau: 181 -> 258 (cheer)
 * Pomme: 258 -> 335 (cheer)
 * Saute: 335 -> 388 (celebrate)
 * False: 617 -> 644 (schnetz)
 * Run: 388 -> 500 (run)
 * True: 644 -> 674 (cheer)
 */
@Component({
  selector: "app-boy",
  templateUrl: "./boy.component.html",
  styleUrls: ["./boy.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class BoyComponent extends AbstractHeroComponent {

  /**
   * Array of animations.
   * @type any
   */
  animations = {
    "idle": [0, 83],
    "cheer": [[83, 181], [181, 258], [258, 335], [644, 674]],
    "run": [388, 500],
    "schnetz": [617, 644],
    "celebrate": [335, 388]
  };
}
