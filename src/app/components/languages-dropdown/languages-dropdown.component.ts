import {Component, EventEmitter, HostListener, Input, OnChanges, Output, SimpleChanges} from "@angular/core";
import {Language} from "../../model/language";
import {bumpAnimation} from "../../animations/bump.animation";

@Component({
  selector: "app-languages-dropdown",
  templateUrl: "./languages-dropdown.component.html",
  styleUrls: ["./languages-dropdown.component.scss"],
  animations: [bumpAnimation]
})
export class LanguagesDropdownComponent implements OnChanges {

  @Input()
  languages: Language[];

  @Output()
  languageChange: EventEmitter<Language> = new EventEmitter<Language>();

  language: Language;

  uiState: UiState = {
    open: false
  };

  ngOnChanges(changes: SimpleChanges): void {
    this.language = this.languages ? this.languages[0] : null;
  }

  @HostListener("click", ["$event"])
  clickInside($event) {
    this.toggle();
    $event.stopPropagation();
  }

  @HostListener("document:click")
  onDocumentClickedHandler() {
    this.uiState.open = false;
  }

  toggle() {
    this.uiState.open = !this.uiState.open;
  }

  select(language: Language, $event) {
    $event.stopPropagation();
    this.uiState.open = false;
    this.language = language;
    this.languageChange.emit(language);
  }
}

interface UiState {
  open: boolean;
}
