import {Component, Input, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {LoadingService} from "../../services/loading.service";

/**
 * This is just a simple loading spinner which
 * registers itself in the loading service so that
 * it can be controlled by other components via the service.
 */
@Component({
  selector: "app-spinner",
  templateUrl: "./spinner.component.html",
  styleUrls: ["./spinner.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent implements OnInit, OnDestroy {

  /**
   * Flag used to show / hide the spinner.
   */
  @Input()
  isVisible: boolean;

  /**
   * Constructor of the spinner.
   * @param {LoadingService} loadingService
   */
  constructor(private loadingService: LoadingService) {
  }

  /**
   * Initialization of the components.
   */
  ngOnInit() {
    this.loadingService._register(this);
  }

  /**
   * Raised when the component gets destroyed.
   */
  ngOnDestroy() {
    this.loadingService._unregister(this);
  }

  /**
   * Shows the spinner.
   */
  show() {
    this.isVisible = true;
  }

  /**
   * Hides the spinner.
   */
  hide() {
    this.isVisible = false;
  }
}
