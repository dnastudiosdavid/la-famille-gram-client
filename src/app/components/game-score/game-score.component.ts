import {Component, Input, OnInit, ViewEncapsulation} from "@angular/core";
import {animate, AnimationEvent, state, style, transition, trigger} from "@angular/animations";
import {ResultState} from "../../games/sentences/model/core/answer-feedback-interface";
import {IGame} from "../../model/game-interface";

/**
 * This component shows the current score of the game.
 */
@Component({
  selector: "app-game-score",
  templateUrl: "./game-score.component.html",
  styleUrls: ["./game-score.component.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: [
    // this animation is used to display
    // the validate action results
    trigger("scoreVisibility", [
      state("inactive", style({
        "top": "-30px",
        "opacity": 0
      })),
      state("active", style({
        "top": "110px",
        "opacity": 1
      })),
      transition("inactive => active", animate("250ms ease-in")),
      transition("active => inactive", animate("250ms 1000ms ease-out"))
    ])
  ]
})
export class GameScoreComponent implements OnInit {

  /**
   * The game model.
   */
  @Input()
  game: IGame;

  /**
   * Should we display the score?
   */
  @Input()
  showScore: boolean;

  /**
   * Animation trigger.
   */
  trigger: string;

  /**
   * Current state.
   */
  state: ResultState;

  /**
   * By how much the score changed.
   */
  scoreChange: number;

  /**
   * Initialization of the component.
   */
  ngOnInit() {
    this.trigger = "inactive";
  }

  /**
   * Shows the score.
   * @param {number} scoreChange
   * @param {ResultState} state
   */
  show(scoreChange: number, state: ResultState) {
    this.state = state;
    this.scoreChange = scoreChange;
    this.trigger = "active";
  }

  /**
   * Handles when the animation has been done.
   * @param {AnimationEvent} event
   */
  onAnimationDone(event: AnimationEvent) {
    if (event.toState === "active") {
      this.trigger = "inactive";
    }
  }
}
