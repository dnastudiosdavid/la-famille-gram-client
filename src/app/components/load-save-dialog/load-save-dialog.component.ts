import {Component, EventEmitter} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";
import {IGame} from "../../model/game-interface";
import * as _ from "lodash";

@Component({
  selector: "app-load-save-dialog",
  templateUrl: "./load-save-dialog.component.html",
  styleUrls: ["./load-save-dialog.component.scss"]
})
export class LoadSaveDialogComponent {

  /**
   * An array of the saved games.
   */
  savedGames: IGame[] = [];

  /**
   * This event is raised when the user wants to delete a saved
   * game from the list.
   */
  deleted: EventEmitter<IGame>;

  /**
   * Ctor.
   */
  constructor(private dialogRef: MatDialogRef<LoadSaveDialogComponent>) {
    this.deleted = new EventEmitter<IGame>();
  }

  /**
   * Closes the dialog with the given game to select.
   */
  select(game: IGame) {
    this.dialogRef.close(_.cloneDeep(game));
  }

  /**
   * Closes the dialog with the given game to remove.
   */
  remove(game: IGame) {
    this.deleted.emit(game);
  }

  /**
   * Cancels the operation.
   */
  cancel() {
    this.dialogRef.close();
  }
}
