import {Component, Input, ViewChild, ViewEncapsulation} from "@angular/core";
import {IHeroComponent} from "../heroes/hero-component-interface";

/**
 * This is the component displaying the nevironment.
 */
@Component({
  selector: "app-environment",
  templateUrl: "./environment.component.html",
  styleUrls: ["./environment.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class EnvironmentComponent {

  /**
   * The selected hero.
   */
  @Input()
  hero: string;

  /**
   * Progression between 0 and 1.
   */
  @Input()
  progression: number;

  /**
   * The score panel.
   */
  @ViewChild("hero") heroRig: IHeroComponent;

  /**
   * Layer 1 left offset.
   */
  p1Left: number;

  /**
   * Layer 2 left offset.
   */
  p2Left: number;

  /**
   * Layer 3 left offset.
   */
  p3Left: number;

  /**
   * Updates the progression of the environment.
   * From 0 to 1.
   */
  setProgression(progression: number) {
    if (progression > 1) {
      progression = 1;
    } else if (progression < 0) {
      progression = 0;
    }
    this.progression = progression;

    // p1: -245vw
    this.p1Left = this.progression * -245;

    // p2: -130vw
    this.p2Left = this.progression * -130;

    // p3 : -15vw
    this.p3Left = this.progression * -15;

    this.heroRig.run(true);
  }

  /**
   * Gets the hero.
   * @return {IHeroComponent}
   */
  getHero(): IHeroComponent {
    return this.heroRig;
  }
}
