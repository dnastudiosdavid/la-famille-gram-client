import {Component, Input, ViewEncapsulation} from "@angular/core";
import {LeaderboardRank} from "../../model/leaderboard-rank";
import {animate, style, transition, trigger} from "@angular/animations";

/**
 * This component is nothing but a classic leaderboard.
 */
@Component({
  selector: "app-leaderboard",
  templateUrl: "./leaderboard.component.html",
  styleUrls: ["./leaderboard.component.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger(
      "opening",
      [
        transition(
          ":enter", [
            style({transform: "translateY(-100%)"}),
            animate("250ms", style({transform: "translateY(0)"}))
          ]
        )]
    )
  ]
})
export class LeaderboardComponent {

  /**
   * The code of the exercise to load ranks.
   */
  @Input()
  exerciseCode: string;

  /**
   * The ranks displayed.
   */
  @Input()
  ranks: LeaderboardRank[];
}
