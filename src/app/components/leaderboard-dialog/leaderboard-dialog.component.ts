import {Component, ViewEncapsulation} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";

/**
 * This component is the dialog where the player
 * can register his nickname into the leaderboard.
 */
@Component({
  selector: "app-leaderboard-dialog",
  templateUrl: "./leaderboard-dialog.component.html",
  styleUrls: ["./leaderboard-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class LeaderboardDialogComponent {

  /**
   * The nickname of the player.
   */
  nickname: string;

  /**
   * The score amount of the player.
   */
  score: number;

  /**
   * Ctor.
   * @param {MatDialogRef<LeaderboardDialogComponent>} dialogRef
   */
  constructor(private dialogRef: MatDialogRef<LeaderboardDialogComponent>) {
  }

  /**
   * Validates the given input.
   */
  validate() {
    this.dialogRef.close(this.nickname);
  }

  /**
   * Cancels the operation and closes the dialog.
   */
  cancel() {
    this.dialogRef.close(null);
  }
}
