import {Injectable} from "@angular/core";
import {LeaderboardRank} from "../model/leaderboard-rank";
import {plainToClass} from "class-transformer";
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {IGame} from "../model/game-interface";

/**
 * This service is responsible for handling leaderboards.
 */
@Injectable({
  providedIn: "root"
})
export class LeaderboardService {

  /**
   * Base location of the exercises models
   */
  private readonly exerciseEndpoint: string = environment.api_endpoint + "/p/exercise";

  /**
   * Ctor.
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Saves the score on the server, in the leaderboard. This
   * method is called from a controller and is not part of the
   * essential flow of a game because players are free to
   * participate in the leaderboard ot not.
   */
  saveScore(game: IGame, nickname: string): Promise<any> {
    const code: string = game.getExercise().Code;
    const url = `${this.exerciseEndpoint}/${code}/score`;
    const promise: Promise<any> = this.http.post(url, {
      score: game.getScore(),
      nickname: nickname,
      mode: game.getMode()
    }).toPromise();
    promise.then(() => {
      game.lockScore();
    });
    return promise;
  }

  /**
   * Gets the rank of the current exercise.
   */
  getLeaderboard(code: string, mode: string): Promise<LeaderboardRank[]> {
    return new Promise<LeaderboardRank[]>((resolve, reject) => {
      const params = new HttpParams().set("mode", mode);
      const url = `${this.exerciseEndpoint}/${code}/score`;
      this.http.get(url, {params: params})
        .toPromise()
        .then((response) => {
          const ranks: LeaderboardRank[] = plainToClass(LeaderboardRank, response as Object[]);
          resolve(ranks);
        })
        .catch(() => {
          reject();
        });
    });
  }
}
