import {IExercise} from "../model/exercise-interface";
import {IGame} from "../model/game-interface";
import {Type} from "@angular/core";
import {GameBaseComponent} from "../views/game-base.component";

export interface IGameService {

  /**
   * The game component type which will be injected in the DOM.
   */
  readonly ComponentType: Type<GameBaseComponent>;

  /**
   * The current game model.
   */
  readonly Game: IGame;

  /**
   * Returns the list of exercises. Includes only the headers of
   * the exercises.
   */
  getExercisesList(): Promise<IExercise[]>;

  /**
   * Loads an exercise given its code.
   */
  loadExercise(code: string): Promise<IExercise>;

  /**
   * Removes the given exercise.
   */
  removeExercise(exercise: IExercise): void;

  /**
   * Starts the game given its exercise.
   */
  startGame(exercise: IExercise, hero: string, student: string, mode: string): IGame;

  /**
   * Sets a game.
   */
  setGame(game: IGame);

  /**
   * Returns true if the game requires a mode selection.
   * TODO: should not be generic as any game can be implemented differently
   */
  hasModes(): boolean;

  /**
   * Returns true if the service can load exercises other than those by default.
   */
  canLoad(): boolean;

  /**
   * Goes to the next step.
   * TODO: should not be generic as any game can be implemented differently
   */
  next();

  /**
   * Gets the previous scores.
   */
  getPreviousScores(): number[];

  /**
   * Gets the default exercise code according to the given degree.
   * TODO: this could be set in the language itself and not be part of the game
   */
  getDefaultExerciseByDegree(degree: number, languageCode: string): string;
}
