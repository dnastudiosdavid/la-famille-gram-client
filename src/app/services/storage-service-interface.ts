/**
 * This interface describes a key-value pair
 * storage service.
 */
export interface IStorageService {

  /**
   * Stores the given value assigned
   * to the given key.
   */
  store(key: string, value: any);

  /**
   * Gets the value associated to the given key.
   */
  get(key: string): any;
}
