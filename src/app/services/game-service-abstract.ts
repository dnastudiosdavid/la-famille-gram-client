import {IGameService} from "./game-service-interface";
import {IGame} from "../model/game-interface";
import {IExercise} from "../model/exercise-interface";
import {GameBaseComponent} from "../views/game-base.component";
import {Type} from "@angular/core";
import {ExerciseHeader} from "./app.service";
import {LocalStorageService} from "./local-storage.service";

/**
 * This is a base class for any game service - providing a basic
 * implementation to manage a game.
 */
export abstract class AbstractGameService implements IGameService {

  /**
   * @inheritDoc
   */
  abstract readonly ComponentType: Type<GameBaseComponent>;

  /**
   * A list of codes to know what exercise can be
   * started from the main menu.
   */
  protected exercisesHeaders: ExerciseHeader [] = [];

  /**
   * An array of previous scores made on this exercise.
   */
  protected previousScores: number[] = [];

  /**
   * Amount of score to keep in history.
   */
  private readonly ScoresToTrack: number = 5;

  /**
   * Ctor.
   */
  protected constructor(protected storageService: LocalStorageService) {
  }

  /**
   * @inheritDoc
   */
  abstract get Game(): IGame;

  /**
   * @inheritDoc
   */
  abstract getExercisesList(): Promise<IExercise[]>;

  /**
   * @inheritDoc
   */
  abstract hasModes(): boolean;

  /**
   * @inheritDoc
   */
  abstract canLoad(): boolean;

  /**
   * @inheritDoc
   */
  abstract loadExercise(code: string): Promise<IExercise>;

  /**
   * @inheritDoc
   */
  abstract next();

  /**
   * @inheritDoc
   */
  abstract removeExercise(exercise: IExercise): void;

  /**
   * @inheritDoc
   */
  abstract startGame(exercise: IExercise, hero: string, student: string, mode: string): IGame;

  /**
   * @inheritDoc
   */
  abstract setGame(game: IGame);

  /**
   * @see IGameService.getDefaultExerciseByDegree
   */
  abstract getDefaultExerciseByDegree(degree: number, languageCode: string): string;

  /**
   * @see IGameService.getPreviousScores
   */
  getPreviousScores(): number[] {
    return this.previousScores;
  }

  /**
   * Sets the previous scores.
   */
  protected setPreviousScores(code: string, mode: string) {
    this.previousScores = this.storageService.get(`sc_${code}_${mode}`) || [];
  }

  /**
   * Saves the given score in the local storage.
   */
  protected saveScore(code: string, mode: string, score: number) {
    this.previousScores.unshift(score);
    this.previousScores = this.previousScores.slice(0, this.ScoresToTrack);
    this.storageService.store(`sc_${code}_${mode}`, this.previousScores);
    this.previousScores.reverse();
  }

  /**
   * Adds an exercise to the list.
   */
  protected addExerciseCode(key: string, exercise: IExercise) {
    if (!this.exercisesHeaders.find(e => e.Code === exercise.Code)) {
      this.exercisesHeaders.push(new ExerciseHeader(exercise.Code, exercise.Name, key, exercise.LanguageCode, -1, false));
      this.storeCodes(key, this.exercisesHeaders);
    }
  }

  /**
   * Gets the stored codes of loaded exercises.
   */
  protected getCodesArray(key: string): ExerciseHeader[] {
    let raw: any[];
    try {
      raw = this.storageService.get(`codes_${key}`);
    } catch (e) {
      raw = [];
    }

    const codes: ExerciseHeader[] = [];
    if (!raw) {
      raw = [];
    }
    for (const r of raw) {
      if (r.game === key) {
        codes.push(new ExerciseHeader(r.code, r.name, r.game, r.languageCode, r.degree, false));
      }
    }
    return codes;
  }

  /**
   * Stores the given codes in the local storage
   * of the browser.
   */
  protected storeCodes(key: string, codes: ExerciseHeader[]) {
    const raw: any[] = [];
    for (const header of codes) {
      if (header.IsDefault) {
        continue;
      }
      raw.push({
        code: header.Code,
        name: header.Name,
        game: header.Game,
        languageCode: header.LanguageCode,
        degree: header.Degree
      });
    }
    this.storageService.store(`codes_${key}`, raw);
  }
}
