import {Injectable} from "@angular/core";
import {App} from "../model/app";
import {IExercise} from "../model/exercise-interface";
import {IGame} from "../model/game-interface";
import {IGameService} from "./game-service-interface";

/**
 * This is the main service of the app. It is responsible
 * for providing the tools to the components and to bootstrap
 * the main model, which is App.
 */
@Injectable({
  providedIn: "root"
})
export class AppService {

  /**
   * App model.
   */
  private app: App;

  /**
   * The game service in use.
   */
  private gameService: IGameService;

  get Game(): IGame {
    return this.gameService ? this.gameService.Game : null;
  }

  get GameService(): IGameService {
    return this.gameService;
  }

  /**
   * Gets the app main model.
   */
  getApp(): Promise<App> {
    if (this.app) {
      return new Promise<App>((resolve) => {
        resolve(this.app);
      });
    } else {
      return new Promise<App>((resolve) => {
        this.app = new App();
        resolve(this.app);
      });
    }
  }

  /**
   * Sets the current game service responsible for loading & scoring
   * of a specific game.
   */
  setCurrentGameService(gameService: IGameService) {
    this.gameService = gameService;
  }
}

/**
 * This class represents a structure which wraps
 * an exercise's required data to start it.
 */
export class ExerciseHeader implements IExercise {

  /**
   * @see IExercise.Code
   */
  get Code(): string {
    return this.code;
  }

  /**
   * @see IExercise.Name
   */
  get Name(): string {
    return this.name;
  }

  /**
   * @see IExercise.Game
   */
  get Game(): string {
    return this.game;
  }

  /**
   * @see IExercise.IsDefault
   */
  get IsDefault(): boolean {
    return this.isDefault;
  }

  /**
   * @see IExercise.HasLeaderboard
   */
  get HasLeaderboard(): boolean {
    return false;
  }

  get LanguageCode(): string {
    return this.languageCode;
  }

  get Degree(): number {
    return this.degree;
  }

  /**
   * Ctor.
   */
  constructor(private code: string,
              private name: string,
              private game: string,
              private languageCode: string,
              private degree: number,
              private isDefault: boolean = true) {
  }

  /**
   * Rename the definition.
   */
  rename(name: string) {
    this.name = name;
  }

  /**
   * @see IExercise.lock()
   */
  lock(): void {
    throw new Error("Method not implemented.");
  }
}
