import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {IActivity} from "../model/activity-interface";
import {classToPlain} from "class-transformer";
import {SentencesActivity} from "../games/sentences/model/activity/sentences-activity";

export interface MessageType {
  degree: number;
  nickname: string;
  mode: number;
  type: string;
  language: string;
}

@Injectable({
  providedIn: "root"
})
export class Hep3DriverService {

  /**
   * Our subject to broadcast messages.
   */
  private messageStream: Subject<any> = new Subject();

  /**
   * Is the service enabled?
   */
  private isEnabled: boolean = false;

  /**
   * The current gameshub mode we are in.
   * 1 = train
   * 2 = evaluate
   * 3 = explore
   * 4 = create
   * We currently support only train & evaluate.
   */
  private mode: number = 1;

  /**
   * Returns the message stream.
   */
  get MessageStream(): Subject<MessageType> {
    return this.messageStream;
  }

  /**
   * Ctor.
   */
  constructor() {
    window.onmessage = (e) => {
      this.handleMessageFromContainer(e);
    };
  }

  /**
   * Tells the service that it is actually used.
   */
  enable() {
    this.isEnabled = true;
  }

  /**
   * Tells the container to get back.
   */
  back() {
    this.send(this.getData("back"));
  }

  /**
   * Sends the degree.
   */
  sendDegree(value: number) {
    this.send({
      value: value,
      ...this.getData("degree")
    });
  }

  /**
   * Handles when the mode changes.
   */
  handleMode(value: number) {
    this.mode = value;
    this.sendMode(this.mode);
  }

  /**
   * Sends the mode.
   */
  sendMode(value: number) {
    this.send({
      value: value,
      ...this.getData("mode")
    });
  }

  /**
   * Tells the API above that we started the game.
   */
  sendStatsStart() {
    // we don't send stats if the user chose the mode explore or create
    if (this.mode === 3 || this.mode === 4) {
      return;
    }
    this.send({
      event: "start",
      mode: this.mode,
      ...this.getData("stats")
    });
  }

  /**
   * Tells the API above that we completed the exercise.
   */
  sendStatsEnd(activity: IActivity) {
    // we don't send stats if the user chose the mode explore or create
    if (this.mode === 3 || this.mode === 4) {
      return;
    }
    this.send({
      event: "end",
      evaluation: this.getEvaluationGrade(activity),
      details: classToPlain(activity),
      ...this.getData("stats")
    });
  }

  /**
   * Sends a message to the container.
   */
  private send(data: any) {
    if (!this.isEnabled) {
      return;
    }
    window.top.postMessage(data, "*");
  }

  /**
   * Returns a new data object which is ready to be sent to the top window.
   */
  private getData(type: string): any {
    return {
      type: type
    };
  }

  /**
   * Handles a message from the parent container.
   */
  private handleMessageFromContainer(e: MessageEvent) {
    const data = e.data;
    switch (data.type) {
      case "mode":
        this.handleMode(data.mode);
        break;
    }
    this.messageStream.next(data);
  }

  /**
   * Returns an evaluation grade based on the activity.
   */
  private getEvaluationGrade(activity: IActivity): number {
    if (!activity) {
      return 0;
    }

    if (activity instanceof SentencesActivity) {
      // this is the old implementation but we want to keep
      // it for exercises based on sentences
      const errorRate = activity.getErrorRate();
      return errorRate <= 10 ? 2 : errorRate <= 35 ? 1 : 0;
    } else {
      const completionRate = activity.getCompletionRate();
      return Math.round(Math.min(completionRate, 100) / 100 * 2 / 0.01) * 0.01;
    }
  }
}
