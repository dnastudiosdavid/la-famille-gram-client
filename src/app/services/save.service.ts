import {Injectable} from "@angular/core";
import * as _ from "lodash";
import {SaveSnackbarComponent} from "../components/save-snackbar/save-snackbar.component";
import {List} from "linqts";
import {instanceToPlain} from "class-transformer";
import {ChainsGame} from "../games/sentences/model/chains/chains-game";
import {TirettesGame} from "../games/sentences/model/tirettes/tirettes-game";
import {SilhouettesGame} from "../games/sentences/model/silhouettes/silhouettes-game";
import {GramClassGame} from "../games/sentences/model/gram-class/gram-class-game";
import {LocalStorageService} from "./local-storage.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {IGame} from "../model/game-interface";
import {SubstitutesGame} from "../games/substitutes/model/substitutes-game";

/**
 * Handles saved games.
 */
@Injectable({
  providedIn: "root"
})
export class SaveService {

  /**
   * @see SavedGames
   */
  private savedGames: IGame [];

  /**
   * The saved games.
   */
  get SavedGames(): IGame[] {
    return this.savedGames;
  }

  constructor(private storageService: LocalStorageService,
              private snackBar: MatSnackBar) {
    this.savedGames = this.getSavedGames();
  }

  /**
   * Save the current game.
   */
  saveGame(game: IGame) {
    this.savedGames.push(_.cloneDeep(game));
    this.persistSavedGames();

    this.snackBar.openFromComponent(SaveSnackbarComponent, {
      data: {message: "La partie a été sauvegardée!"},
      duration: 2000
    });
  }

  /**
   * Deletes the given saved game.
   */
  deleteSavedGame(game: IGame) {
    this.savedGames = new List<IGame>(this.savedGames).Except(new List<IGame>([game])).ToArray();
    this.persistSavedGames();
  }

  /**
   * Persists the saved games array.
   */
  private persistSavedGames() {
    const data: Object[] = [];
    for (const game of this.savedGames) {
      data.push(instanceToPlain(game));
    }
    this.storageService.store("saved_games", data);
  }

  /**
   * Loads the saved games.
   */
  private getSavedGames(): IGame[] {
    const result: IGame[] = [];
    const raw: any = this.storageService.get("saved_games");
    if (!raw) {
      return [];
    }
    for (const data of raw) {
      const game: IGame = this.deseralizeGame(data);
      if (game !== null) {
        result.push(game);
      }
    }
    return result;
  }

  /**
   * Deserializes the given game data.
   * TODO: make it generic
   */
  private deseralizeGame(data: Object): IGame {
    const mode: string = data["mode"];
    switch (mode) {
      case "chains":
        return ChainsGame.load(data);
      case "tirettes":
        return TirettesGame.load(data);
      case "silhouettes":
        return SilhouettesGame.load(data);
      case "gram-class":
        return GramClassGame.load(data);
      case "std":
        return SubstitutesGame.load(data);
    }
    return null;
  }
}
