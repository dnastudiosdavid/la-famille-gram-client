import {Injectable} from "@angular/core";
import {IStorageService} from "./storage-service-interface";

/**
 * This service abstracts any kind of storage method
 * and provides a single facade to the app to store
 * key-value pairs.
 */
@Injectable({
  providedIn: "root"
})
export class LocalStorageService implements IStorageService {

  /**
   * Stores the given value assigned
   * to the given key.
   */
  store(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  /**
   * Gets the value associated to the given key.
   */
  get(key: string): any {
    return JSON.parse(localStorage.getItem(key));
  }
}
