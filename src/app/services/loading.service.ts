import {Injectable} from "@angular/core";
import {SpinnerComponent} from "../components/spinner/spinner.component";

/**
 * This service handles any loader in the application
 * and is a centralized point which connects controllers
 * and loaders components together.
 */
@Injectable({
  providedIn: "root"
})
export class LoadingService {

  /**
   * Cache of registered spinners.
   */
  private spinnerCache = new Set<SpinnerComponent>();

  /**
   * Registers a spinner in the cache.
   */
  _register(spinner: SpinnerComponent) {
    this.spinnerCache.add(spinner);
  }

  /**
   * Unregisters the given spinner from the cache.
   */
  _unregister(spinnerToRemove: SpinnerComponent) {
    this.spinnerCache.forEach(spinner => {
      if (spinner === spinnerToRemove) {
        this.spinnerCache.delete(spinner);
      }
    });
  }

  /**
   * Shows all the 'visible' spinners.
   */
  show() {
    this.spinnerCache.forEach(spinner => {
      spinner.show();
    });
  }

  /**
   * Hides all the 'visible' spinners.
   */
  hide() {
    this.spinnerCache.forEach(spinner => {
      spinner.hide();
    });
  }
}
