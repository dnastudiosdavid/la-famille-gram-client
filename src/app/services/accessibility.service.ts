import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class AccessibilityService {
  private isSpeechPaused: boolean;
  private isSpeechActive: boolean;

  get IsSpeechPaused(): boolean {
    return this.isSpeechPaused;
  }

  get IsSpeechActive(): boolean {
    return this.isSpeechActive;
  }

  /**
   * Starts the speech synthesis.
   */
  startSpeech(text: string) {
    if (!("speechSynthesis" in window)) {
      return;
    }
    const synth = window.speechSynthesis;
    if (!synth.speaking) {
      const msg = new SpeechSynthesisUtterance(text);
      msg.rate = 0.8;
      msg.pitch = 1;
      synth.speak(msg);
    }

    this.isSpeechPaused = false;
    this.isSpeechActive = true;
  }

  /**
   * Stops the speech synthesis.
   */
  stopSpeech(): void {
    const synth = window.speechSynthesis;
    synth.cancel();
    this.isSpeechPaused = true;
    this.isSpeechActive = false;
  }

  /**
   * Toggle the speech synthesis pause state.
   */
  toggleSpeechPause(): void {
    const synth = window.speechSynthesis;
    if (!this.isSpeechPaused) {
      synth.pause();
      this.isSpeechPaused = true;
    } else {
      synth.resume();
      this.isSpeechPaused = false;
    }
  }
}
