import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class LayoutService {

  /**
   * Requested layout settings.
   */
  private settings: ILayoutSettings;

  /**
   * Subject used to trigger subscriptions.
   */
  private settingsSubject: BehaviorSubject<ILayoutSettings>;

  /**
   * Gets the current settings.
   */
  get Settings(): ILayoutSettings {
    return this.settings;
  }

  /**
   * Gets notified when the settings change.
   */
  get Settings$(): Observable<ILayoutSettings> {
    return this.settingsSubject;
  }

  /**
   * Ctor.
   */
  constructor() {
    this.settings = {
      contained: false
    };
    this.settingsSubject = new BehaviorSubject<ILayoutSettings>(this.settings);
  }

  /**
   * Sets the settings.
   */
  setSettings(settings: ILayoutSettings) {
    this.settings = settings;
    this.settingsSubject.next(this.settings);
  }
}

/**
 * Describes how the layout should be set up.
 */
export interface ILayoutSettings {
  contained: boolean;
}
