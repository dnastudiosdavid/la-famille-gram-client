import {EventEmitter, Injectable, Type} from "@angular/core";
import {IExercise} from "../../model/exercise-interface";
import {ExerciseHeader} from "../../services/app.service";
import {SentencesExercise} from "./model/core/sentences-exercise";
import {classToPlain, plainToClass} from "class-transformer";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {ISentencesGame} from "./model/core/sentences-game-interface";
import {GameMode} from "./model/core/game-mode";
import {TirettesGame} from "./model/tirettes/tirettes-game";
import {GramClassGame} from "./model/gram-class/gram-class-game";
import {SilhouettesGame} from "./model/silhouettes/silhouettes-game";
import {ChainsGame} from "./model/chains/chains-game";
import {LocalStorageService} from "../../services/local-storage.service";
import {IGame} from "../../model/game-interface";
import {IActivity} from "../../model/activity-interface";
import {GameBaseComponent} from "../../views/game-base.component";
import {GameTirettesComponent} from "./components/game-tirettes/game-tirettes.component";
import {GameGramClassComponent} from "./components/game-gram-class/game-gram-class.component";
import {GameSilhouettesComponent} from "./components/game-silhouettes/game-silhouettes.component";
import {GameChainsComponent} from "./components/game-chains/game-chains.component";
import {AbstractGameService} from "../../services/game-service-abstract";

@Injectable({
  providedIn: "root"
})
export class SentencesService extends AbstractGameService {

  /**
   * Game identifier.
   */
  private static readonly GameId: string = "sentences";

  /**
   * This event is raised when the exercise
   * has been ended.
   */
  gameEnded: EventEmitter<void>;

  /**
   * Base location of the exercises models
   */
  private readonly ApiEndpoint: string = `${environment.api_endpoint}/p/exercise`;

  /**
   * @see CurrentGame
   */
  private currentGame: ISentencesGame;

  /**
   * Ctor.
   */
  constructor(private http: HttpClient,
              protected storageService: LocalStorageService) {
    super(storageService);
    this.gameEnded = new EventEmitter();
    this.exercisesHeaders = this.getDefaultExercises();
  }

  /**
   * The current game being played.
   */
  get Game(): ISentencesGame {
    return this.currentGame;
  }

  /**
   * @inheritDoc
   */
  get ComponentType(): Type<GameBaseComponent> {
    switch (this.currentGame.getMode()) {
      case GameMode.Tirettes:
        return GameTirettesComponent;
      case GameMode.GramClass:
        return GameGramClassComponent;
      case GameMode.Silhouettes:
        return GameSilhouettesComponent;
      case GameMode.Chains:
        return GameChainsComponent;
    }
  }

  /**
   * Returns the list of exercises.
   */
  getExercisesList(): Promise<IExercise[]> {
    return Promise.resolve(this.exercisesHeaders);
  }

  /**
   * Loads an exercise given its code.
   */
  loadExercise(code: string): Promise<IExercise> {
    return new Promise((resolve, reject) => {
      const url = `${this.ApiEndpoint}/${code}`;
      this.http.get(url)
        .toPromise()
        .then((response) => {
          const exercise: SentencesExercise = plainToClass(SentencesExercise, response as Object);
          this.addExerciseCode(SentencesService.GameId, exercise);
          resolve(exercise);
        })
        .catch((err) => {
          reject();
        });
    });
  }

  /**
   * @inheritDoc
   */
  removeExercise(exercise: IExercise): void {
    this.exercisesHeaders = this.exercisesHeaders.filter(c => c.Code !== exercise.Code);
    this.storeCodes(SentencesService.GameId, this.exercisesHeaders);
  }

  /**
   * The "sentences" game has multiple modes.
   */
  hasModes(): boolean {
    return true;
  }

  /**
   * This game accepts laoding new exercises.
   */
  canLoad(): boolean {
    return true;
  }

  /**
   * Starts a new game.
   */
  startGame(exercise: IExercise, hero: string, student: string, mode: string): IGame {
    if (!exercise) {
      throw new Error("The exercise provided is null.");
    }

    // well the game service could receive a game object
    // so it's not aware of the different game modes
    // but for the sake of simplicity let's just let it create it
    let game: ISentencesGame = null;
    switch (mode) {
      case GameMode.Tirettes:
        game = new TirettesGame();
        break;
      case GameMode.GramClass:
        game = new GramClassGame();
        break;
      case GameMode.Silhouettes:
        game = new SilhouettesGame();
        break;
      case GameMode.Chains:
        game = new ChainsGame();
        break;
    }

    this.currentGame = game;
    this.currentGame.start(exercise as SentencesExercise, hero, student);
    this.setPreviousScores(exercise.Code, game.getIdentifier());
    return this.currentGame;
  }

  /**
   * Sets an existing game.
   */
  setGame(game: IGame) {
    this.currentGame = <ISentencesGame>game;
    this.setPreviousScores(this.currentGame.getExercise().Code, this.currentGame.getIdentifier());
  }

  /**
   * Goes to the next sentence of the exercise.
   */
  next() {
    if (!this.currentGame) {
      throw new Error("No exercise started.");
    }

    if (this.currentGame.next()) {
      this.handleEnd();
    }
  }

  /**
   * Ends the current exercise.
   */
  handleEnd() {
    this.saveScore(this.currentGame.getExercise().Code, this.currentGame.getIdentifier(), this.currentGame.getScore());
    this.saveActivity(this.currentGame.getActivity());
    this.gameEnded.emit();
  }

  /**
   * Saves the given activity.
   */
  saveActivity(activity: IActivity) {
    const code: string = this.currentGame.getExercise().Code;
    const url = `${this.ApiEndpoint}/${code}/activity`;
    this.http.post(url, classToPlain(activity)).toPromise();
  }

  /**
   * @inheritDoc
   */
  getDefaultExerciseByDegree(degree: number, languageCode: string): string {
    const exercise = this.exercisesHeaders.find(e => e.IsDefault && e.Degree === degree && e.LanguageCode === languageCode);
    return exercise ? exercise.Code : "f8fd20a02bf7";
  }

  /**
   * Gets the exercises by default.
   */
  private getDefaultExercises(): ExerciseHeader[] {
    const def: ExerciseHeader[] = [
      new ExerciseHeader("f8fd20a02bf7", "4H (2P)", SentencesService.GameId, "fr", 4),
      new ExerciseHeader("9d5d6affb15c", "5H (3P)", SentencesService.GameId, "fr", 5),
      new ExerciseHeader("9495ac37de94", "6H (4P)", SentencesService.GameId, "fr", 6),
      new ExerciseHeader("dea7c05ccb30", "7H (5P)", SentencesService.GameId, "fr", 7),
      new ExerciseHeader("f9421ddc218f", "8H (6P)", SentencesService.GameId, "fr", 8),
      new ExerciseHeader("1096_3992", "4H (2P)", SentencesService.GameId, "de", 4)
    ];
    return def.concat(this.getCodesArray(SentencesService.GameId));
  }
}
