import {Sentence} from "../core/sentence";
import {AbstractSentencesGame} from "../core/sentences-game-abstract";
import {IResult} from "../core/result-interface";
import {GramClassSentenceUnit} from "./gram-class-sentence-unit";
import {IAnswerFeedback} from "../core/answer-feedback-interface";
import {GramClassResult} from "./gram-class-result";
import {GameMode} from "../core/game-mode";
import {SentencesActivity} from "../activity/sentences-activity";
import {plainToInstance} from "class-transformer";
import {ISentencesActivityInterface} from "../activity/sentences-activity-interface";

/**
 * Implementation of the gram class game. In this
 * mode, you have to build the structure of the
 * given sentence.
 */
export class GramClassGame extends AbstractSentencesGame {

  public static readonly Actions = {
    drop: "drop",
    validate: "validate",
    next: "next"
  };

  /**
   * Ctor.
   */
  constructor() {
    super();
  }

  /**
   * Gets the current result casted.
   */
  get CurResult(): GramClassResult {
    return <GramClassResult>this.CurrentResult;
  }

  /**
   * Crates a new instance of this game based on raw data.
   */
  static load(raw: Object): GramClassGame {
    const game: GramClassGame = plainToInstance(GramClassGame, raw);
    AbstractSentencesGame.loadInto(game, raw, (raw) => GramClassResult.load(raw));
    return game;
  }

  /**
   * @inheritDoc
   */
  getIdentifier(): string {
    return GameMode.GramClass;
  }

  /**
   * Records an answer.
   */
  validate(combinations: GramClassSentenceUnit[]): IAnswerFeedback {
    return this.CurResult.validate(combinations);
  }

  /**
   * Gets a new result object.
   */
  protected getResult(sentence: Sentence): IResult {
    return new GramClassResult(sentence);
  }

  /**
   * Gets a new activity.
   */
  protected makeActivity(student: string, exercise: string): ISentencesActivityInterface {
    return new SentencesActivity(student, exercise, this.getIdentifier());
  }
}
