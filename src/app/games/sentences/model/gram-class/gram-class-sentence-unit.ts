import {Word} from "../core/word";
import {GrammaticalClass} from "../core/grammatical-class";

/**
 * This class links a word with the grammatical
 * class the players put below it.
 */
export class GramClassSentenceUnit {

  /**
   * The word which is part of the sentence.
   */
  private readonly word: Word;

  /**
   * The associated grammatical class.
   */
  private chosenClass: GrammaticalClass[];

  /**
   * @see IsLocked
   */
  private isLocked: boolean;

  /**
   * Gets the word
   */
  get Word(): Word {
    return this.word;
  }

  /**
   * Sets the chosen class.
   */
  set ChosenClass(value: GrammaticalClass[]) {
    this.chosenClass = value;
  }

  /**
   * Gets the chosen class.
   */
  get ChosenClass(): GrammaticalClass[] {
    return this.chosenClass;
  }

  /**
   * Returns the is locked flag. This
   * is a stupid property in a dumb object
   * and does not prevent the unit to be
   * actually locked.
   */
  get IsLocked(): boolean {
    return this.isLocked;
  }

  /**
   * Ctor.
   */
  constructor(word: Word) {
    this.word = word;
    this.chosenClass = [];
  }

  /**
   * Turn the locked flag on.
   */
  lock() {
    this.isLocked = true;
  }
}
