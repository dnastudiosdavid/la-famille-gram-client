import {Sentence} from "../core/sentence";
import {ResultCompletionLevel} from "../core/result-interface";
import {AbstractResult} from "../core/result-abstract";
import {GramClassSentenceUnit} from "./gram-class-sentence-unit";
import {Tirette} from "../core/tirette";
import {IAnswerFeedback, ResultState} from "../core/answer-feedback-interface";
import {GramClassAnswerFeedback} from "./gram-class-answer-feedback";
import {ActivityStep} from "../activity/activity-step";
import {List} from "linqts";
import {Word} from "../core/word";
import {ActivityInputBlock} from "../activity/activity-input-block";
import {ActivityInputLine} from "../activity/activity-input-line";
import {plainToClass} from "class-transformer";

/**
 * This is the implementation of the result object
 * in the game of gram class.
 */
export class GramClassResult extends AbstractResult {

  /**
   * Incremented score value when the player
   * gives a valid answer.
   */
  static readonly SCORE_CORRECT: number = 50;

  /**
   * Decremented score value when the player
   * gives a wrong answer.
   */
  static readonly SCORE_INCORRECT: number = -10;

  /**
   * The base score.
   */
  static readonly SCORE_BASE: number = 50;

  /**
   * The current score of this result.
   */
  private score: number;

  /**
   * The completion level.
   */
  private completionLevel: ResultCompletionLevel;

  /**
   * The given combinations. This is an ordered
   * array of strings corresponding to the
   * grammatical classes given.
   */
  private combinations: string[][];

  /**
   * The random correct combination to make
   * a base sentence.
   */
  private combination: string;

  /**
   * Ctor.
   */
  constructor(sentence: Sentence) {
    super(sentence);
    this.score = GramClassResult.SCORE_BASE;
    this.combinations = [];
    this.completionLevel = ResultCompletionLevel.NotEnough;
  }

  /**
   * Crates a new instance of this result based on raw data.
   */
  static load(raw: Object) {
    // @ts-ignore
    const sentence: Sentence = plainToClass(Sentence, raw["sentence"]);
    const result: GramClassResult = new GramClassResult(sentence);
    result.score = raw["score"];
    result.completionLevel = raw["completionLevel"];
    result.combination = raw["combination"];
    result.combinations = raw["combinations"];
    // @ts-ignore
    result.step = plainToClass(ActivityStep, raw["step"]);
    return result;
  }

  /**
   * @inheritDoc
   */
  getCompletionLevel(): ResultCompletionLevel {
    return this.completionLevel;
  }

  /**
   * @inheritDoc
   */
  getScore(): number {
    return this.score;
  }

  /**
   * @inheritDoc
   */
  getMaxScore(): number {
    return GramClassResult.SCORE_BASE + GramClassResult.SCORE_CORRECT;
  }

  /**
   * Gets the combination of randomly chosen sentence.
   */
  getCombination(): string {
    return this.combination;
  }

  /**
   * Validates the given combination.
   */
  validate(combinations: GramClassSentenceUnit[]): IAnswerFeedback {
    let result = true;
    const sequence: string[] = [];
    const blocks: ActivityInputBlock[] = [];
    const wrongClassSeqIndexes: number[] = [];

    for (let i = 0; i < combinations.length; i++) {
      const tirette: Tirette = this.sentence.Tirettes[i];
      const chosenClass = combinations[i].ChosenClass[0];
      let classResult: boolean = true;

      sequence.push(chosenClass ? chosenClass.Id : null);

      if (combinations[i].ChosenClass[0] == null) {
        classResult = false;
        result = false;
      } else {
        classResult = tirette.GrammaticalClass.Id === combinations[i].ChosenClass[0].Id;
        result = result && classResult;
      }

      if (!classResult) {
        wrongClassSeqIndexes.push(i);
      }
    }

    if (!this.hasSequence(this.combinations, sequence)) {
      this.combinations.push(sequence);

      for (let i = 0; i < combinations.length; i++) {
        const unit: GramClassSentenceUnit = combinations[i];
        if (unit.ChosenClass) {
          const reasons: string[] = [];
          const isCorrect: boolean = wrongClassSeqIndexes.indexOf(i) === -1;
          if (!isCorrect) {
            reasons.push("classe");
          }
          blocks.push(new ActivityInputBlock(unit.ChosenClass.length > 0 ? unit.ChosenClass[0].Name : "", isCorrect, reasons));
        } else {
          blocks.push(new ActivityInputBlock("Invariable", true));
        }
      }

      this.step.addInput(new ActivityInputLine(result, blocks));

      let scoreChange = 0;
      if (result) {
        scoreChange = GramClassResult.SCORE_CORRECT;
        this.score += scoreChange;
        this.completionLevel = ResultCompletionLevel.Full;
      } else {
        scoreChange = GramClassResult.SCORE_INCORRECT;
        this.score += scoreChange;
        if (this.score < 0) {
          this.score = 0;
        }
      }
      return new GramClassAnswerFeedback(scoreChange, result ? ResultState.Correct : ResultState.Incorrect);
    } else {
      return new GramClassAnswerFeedback(0, ResultState.Already);
    }
  }

  /**
   * @inheritDoc
   */
  protected makeActivityStep(sentence: Sentence): ActivityStep {
    this.combination = this.getRandomCombination(sentence);
    return new ActivityStep(sentence.Name, 1, new List<Word>(sentence.getWordsOf(this.combination)).Select(w => w.Value).ToArray());
  }

  /**
   * Determines if the given combinations contains the given
   * sequence. This method is used to ensure the player does
   * not get a penalty for giving two times the same answer.
   */
  private hasSequence(combinations: string[][], sequence: string[]): boolean {
    for (let i = 0; i < combinations.length; i++) {
      const combination: string[] = combinations[i];
      if (combination.length !== sequence.length) {
        console.warn("Recorded combination length does not equal the sequence length.");
      }
      let sequenceCheck = true;
      for (let j = 0; j < combination.length; j++) {
        sequenceCheck = sequenceCheck && combination[j] === sequence[j];
      }
      if (sequenceCheck) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gets a random combination.
   */
  private getRandomCombination(sentence: Sentence): string {
    const combinations: string[] = sentence.getCorrectCombinations();
    return combinations[Math.floor(Math.random() * combinations.length)];
  }
}
