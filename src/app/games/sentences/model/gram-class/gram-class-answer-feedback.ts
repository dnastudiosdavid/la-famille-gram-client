import {IAnswerFeedback, ResultState} from "../core/answer-feedback-interface";
import {ActivityInputBlock} from "../activity/activity-input-block";

/**
 * This is the implementation of the feedback object
 * in the game of gram class.
 */
export class GramClassAnswerFeedback implements IAnswerFeedback {

  /**
   * Ctor.
   */
  constructor(private scoreChange: number, private resultState: ResultState) {
  }

  /**
   * @inheritDoc
   */
  getResultState(): ResultState {
    return this.resultState;
  }

  /**
   * @inheritDoc
   */
  getScoreChange(): number {
    return this.scoreChange;
  }

  /**
   * @inheritDoc
   */
  getInput(): ActivityInputBlock[] {
    return undefined;
  }
}
