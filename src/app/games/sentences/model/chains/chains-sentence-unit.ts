import {Word} from "../core/word";
import {Chain} from "../core/chain";
import {Tirette} from "../core/tirette";

/**
 * This class links a word with the grammatical
 * class the players put below it.
 */
export class ChainsSentenceUnit {

  /**
   * The word which is part of the sentence.
   */
  private readonly word: Word;

  /**
   * The associated chain.
   */
  private chosenChain: Chain;

  /**
   * The tirette.
   */
  private tirette: Tirette;

  /**
   * Gets the word
   */
  get Word(): Word {
    return this.word;
  }

  /**
   * Gets the tirette.
   */
  get Tirette(): Tirette {
    return this.tirette;
  }

  /**
   * Sets the chosen chain.
   */
  set ChosenChain(value: Chain) {
    this.chosenChain = value;
  }

  /**
   * Gets the chosen chain.
   */
  get ChosenChain(): Chain {
    return this.chosenChain;
  }

  /**
   * Ctor.
   */
  constructor(word: Word, tirette: Tirette, setChain: boolean) {
    this.word = word;
    this.tirette = tirette;
    if (setChain) {
      this.chosenChain = tirette.Chain;
    } else {
      this.chosenChain = null;
    }
  }
}
