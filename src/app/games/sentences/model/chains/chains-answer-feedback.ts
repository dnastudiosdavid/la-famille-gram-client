import {IAnswerFeedback, ResultState} from "../core/answer-feedback-interface";
import {ActivityInputBlock} from "../activity/activity-input-block";

/**
 * This is the implementation of the feedback object
 * in the game of silhouettes.
 */
export class ChainsAnswerFeedback implements IAnswerFeedback {

  /**
   * Ctor.
   */
  constructor(private scoreChange: number, private resultState: ResultState) {
  }

  /**
   * @inheritDoc
   */
  getResultState(): ResultState {
    return this.resultState;
  }

  /**
   * @inheritDoc
   */
  getScoreChange(): number {
    return this.scoreChange;
  }

  /**
   * @inheritDoc
   */
  getInput(): ActivityInputBlock[] {
    return undefined;
  }
}
