import {Sentence} from "../core/sentence";
import {AbstractSentencesGame} from "../core/sentences-game-abstract";
import {IResult} from "../core/result-interface";
import {ChainsSentenceUnit} from "./chains-sentence-unit";
import {IAnswerFeedback} from "../core/answer-feedback-interface";
import {ChainsResult} from "./chains-result";
import {GameMode} from "../core/game-mode";
import {SentencesActivity} from "../activity/sentences-activity";
import {plainToInstance} from "class-transformer";
import {ISentencesActivityInterface} from "../activity/sentences-activity-interface";

/**
 * Implementation of the chains game. In this
 * mode, you have to outline the chains of a
 * sentence.
 */
export class ChainsGame extends AbstractSentencesGame {

  public static readonly Actions = {
    chain: "chain",
    validate: "validate",
    next: "next"
  };

  constructor() {
    super();
  }

  /**
   * Gets the current result cast.
   */
  get CurResult(): ChainsResult {
    return <ChainsResult>this.CurrentResult;
  }

  /**
   * Crates a new instance of this game based on raw data.
   */
  static load(raw: Object): ChainsGame {
    const game: ChainsGame = plainToInstance(ChainsGame, raw);
    AbstractSentencesGame.loadInto(game, raw, (raw) => ChainsResult.load(raw));
    return game;
  }

  /**
   * @inheritDoc
   */
  getIdentifier(): string {
    return GameMode.Chains;
  }

  /**
   * Records an answer.
   */
  validate(combinations: ChainsSentenceUnit[]): IAnswerFeedback {
    return this.CurResult.validate(combinations);
  }

  /**
   * Gets a new result object.
   */
  protected getResult(sentence: Sentence): IResult {
    return new ChainsResult(sentence);
  }

  /**
   * Gets a new activity.
   */
  protected makeActivity(student: string, exercise: string): ISentencesActivityInterface {
    return new SentencesActivity(student, exercise, this.getIdentifier());
  }
}
