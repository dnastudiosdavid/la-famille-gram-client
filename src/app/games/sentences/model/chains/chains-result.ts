import {Sentence} from "../core/sentence";
import {ResultCompletionLevel} from "../core/result-interface";
import {AbstractResult} from "../core/result-abstract";
import {ChainsSentenceUnit} from "./chains-sentence-unit";
import {IAnswerFeedback, ResultState} from "../core/answer-feedback-interface";
import {ChainsAnswerFeedback} from "./chains-answer-feedback";
import {Chain} from "../core/chain";
import {List} from "linqts";
import {ActivityStep} from "../activity/activity-step";
import {ActivityInputBlock} from "../activity/activity-input-block";
import {ActivityInputLine} from "../activity/activity-input-line";
import {Word} from "../core/word";
import {plainToClass} from "class-transformer";

/**
 * This is the implementation of the result object
 * in the game of chains.
 */
export class ChainsResult extends AbstractResult {

  /**
   * Incremented score value when the player
   * gives a valid answer.
   */
  static readonly SCORE_CORRECT: number = 50;

  /**
   * Decremented score value when the player
   * gives a wrong answer.
   */
  static readonly SCORE_INCORRECT: number = -10;

  /**
   * The base score.
   */
  static readonly SCORE_BASE: number = 50;

  /**
   * The current score of this result.
   */
  private score: number;

  /**
   * The completion level.
   */
  private completionLevel: ResultCompletionLevel;

  /**
   * The given combinations. This is an ordered
   * array of strings corresponding to the coded
   * combinations given.
   */
  private combinations: string[];

  /**
   * The random correct combination to make
   * a base sentence.
   */
  private combination: string;

  /**
   * Ctor.
   */
  constructor(sentence: Sentence) {
    super(sentence);
    this.score = ChainsResult.SCORE_BASE;
    this.combinations = [];
    this.completionLevel = ResultCompletionLevel.NotEnough;
  }

  /**
   * Crates a new instance of this result based on raw data.
   */
  static load(raw: Object): ChainsResult {
    // @ts-ignore
    const sentence: Sentence = plainToClass(Sentence, raw["sentence"]);
    const result: ChainsResult = new ChainsResult(sentence);
    result.score = raw["score"];
    result.completionLevel = raw["completionLevel"];
    result.combination = raw["combination"];
    result.combinations = raw["combinations"];
    // @ts-ignore
    result.step = plainToClass(ActivityStep, raw["step"]);
    return result;
  }

  /**
   * @todo: implementation
   */
  getCompletionLevel(): ResultCompletionLevel {
    return this.completionLevel;
  }

  /**
   * @inheritDoc
   */
  getScore(): number {
    return this.score;
  }

  /**
   * @inheritDoc
   */
  getMaxScore(): number {
    return ChainsResult.SCORE_CORRECT + ChainsResult.SCORE_BASE;
  }

  /**
   * Gets the combination of randomly chosen sentence.
   */
  getCombination(): string {
    return this.combination;
  }

  /**
   * Gets the chains available.
   */
  getChains(difficulty: number): Chain[] {
    let chains: List<Chain> = new List<Chain>();
    for (const tirette of this.sentence.Tirettes) {
      if (tirette.Chain) {
        chains.Add(tirette.Chain);
      }
    }

    // the OrderBy strangely fixes a weird issue
    // by linqts which doesn't give a correct list as
    // Distinct() ouput
    chains = chains.OrderBy(c => c.Name).Distinct();
    const minChains = chains.Count();

    // add some fake chains
    // maximum: 5
    const chainsToAdd: number = Math.max(Math.floor(difficulty / 2), Math.max(2 - minChains, 0));

    for (let i = 1; i <= Math.min(5 - minChains, chainsToAdd); i++) {
      chains.Add(new Chain(`Chaine ${minChains + i}`));
    }
    return chains.OrderBy(c => c.Number).ToArray();
  }

  /**
   * Validates the given combination. Group the words by the chain.
   * Do the same using the base sentence and then compare the
   * 2 groups.
   */
  validate(sequence: ChainsSentenceUnit[]): IAnswerFeedback {
    // get the given combination
    const inputCombination: string = this.getChainsCombination(sequence);
    // get the combination of the sentence
    const correctSequence: ChainsSentenceUnit[] = [];
    // create the block array
    const blocks: ActivityInputBlock[] = [];
    for (const tirette of this.sentence.Tirettes) {
      const setChain: boolean = !tirette.GrammaticalClass.IsInvariable;
      correctSequence.push(new ChainsSentenceUnit(null, tirette, setChain));
    }
    const correctCombination: string = this.getChainsCombination(correctSequence);
    const result: boolean = inputCombination === correctCombination;

    if (this.combinations.indexOf(inputCombination) === -1) {

      this.combinations.push(inputCombination);
      for (let i = 0; i < sequence.length; i++) {
        const unit: ChainsSentenceUnit = sequence[i];
        if (unit.ChosenChain) {
          const indexResult: boolean = this.compareAt(i, correctCombination, inputCombination);
          const reasons: string [] = indexResult ? [] : ["accord"];
          blocks.push(new ActivityInputBlock(unit.ChosenChain.Name, indexResult, reasons));
        }
      }

      this.step.addInput(new ActivityInputLine(result, blocks));

      if (result) {
        this.completionLevel = ResultCompletionLevel.Full;
        this.score += ChainsResult.SCORE_CORRECT;
        return new ChainsAnswerFeedback(ChainsResult.SCORE_CORRECT, ResultState.Correct);
      } else {
        this.score += ChainsResult.SCORE_INCORRECT;
        return new ChainsAnswerFeedback(ChainsResult.SCORE_INCORRECT, ResultState.Incorrect);
      }
    } else {
      return new ChainsAnswerFeedback(0, ResultState.Already);
    }
  }

  /**
   * @inheritDoc
   */
  protected makeActivityStep(sentence: Sentence): ActivityStep {
    this.combination = this.getRandomCombination(sentence);
    return new ActivityStep(sentence.Name, 1, new List<Word>(sentence.getWordsOf(this.combination)).Select(w => w.Value).ToArray());
  }

  /**
   * Gets a string representation of the combination of chains.
   */
  private getChainsCombination(sequence: ChainsSentenceUnit[]): string {
    const buffer: List<Chain> = new List<Chain>();
    let result = "";
    for (let i = 0; i < sequence.length; i++) {
      const cUnit = sequence[i].ChosenChain;
      if (!cUnit) {
        result += "x";
      } else {
        const cBuffer: Chain = buffer.SingleOrDefault((c) => c.compare(cUnit));
        if (!cBuffer) {
          buffer.Add(cUnit);
          result += buffer.IndexOf(cUnit);
        } else {
          result += buffer.IndexOf(cBuffer);
        }
      }
    }
    return result;
  }

  /**
   * Gets a random combination.
   */
  private getRandomCombination(sentence: Sentence): string {
    const combinations: string[] = sentence.getCorrectCombinations();
    return combinations[Math.floor(Math.random() * combinations.length)];
  }

  /**
   * Returns true if the input is correct at the
   * specified index.
   */
  private compareAt(index: number, correctCombination: string, inputCombination: string): boolean {
    if (inputCombination[index] === "x" && correctCombination[index] === "x") {
      return true;
    }

    const inputGroups: number[][] = this.getGroupsOf(inputCombination);
    const inputGroupOfIndex: number[] = this.getGroupOfIndex(index, inputGroups);
    const correctGroupOfIndex: number[] = this.getGroupOfIndex(index, this.getGroupsOf(correctCombination));

    if (inputGroupOfIndex) {
      const iGroup = new List<number>(inputGroupOfIndex);
      const cGroup = new List<number>(correctGroupOfIndex);
      const r = iGroup.Except(cGroup).Concat(cGroup.Except(iGroup));
      return r.Count() === 0;
    }

    return false;
  }

  /**
   * Groups the index of the given combination.
   */
  private getGroupsOf(combination: string): number[][] {
    const splitted: string[] = combination.split("");
    const groups: number[][] = [];
    for (let i = 0; i < splitted.length; i++) {
      if (splitted[i] === "x") {
        continue;
      }
      const group: number = parseInt(splitted[i], 10);
      if (groups.length - 1 < group) {
        groups.push([]);
      }
      groups[group].push(i);
    }
    return groups;
  }

  /**
   * Gets the group of the given index.
   */
  private getGroupOfIndex(index: number, inputGroups: number[][]): number[] | null {
    return new List<number[]>(inputGroups).SingleOrDefault(g => g.indexOf(index) !== -1);
  }
}
