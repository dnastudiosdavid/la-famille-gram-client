import {Sentence} from "./sentence";
import {Type} from "class-transformer";
import {AbstractExercise} from "../../../../model/exercise-abstract";

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function s() {
  return Sentence;
}

/**
 * This class holds all data related to an exercise.
 */
export class SentencesExercise extends AbstractExercise {

  /**
   * @see HasLeaderboard
   */
  private acceptsLeaderboard: boolean;

  /**
   * @see Sentences
   */
  @Type(s)
  private sentences: Sentence[];

  /**
   * @see Modes
   */
  private modes: string[];

  /**
   * The array of sentences contained by the exercise.
   * @returns {Sentence[]}
   */
  get Sentences(): Sentence[] {
    return this.sentences;
  }

  /**
   * The array of modes.
   * @return {string[]}
   */
  get Modes(): string [] {
    return this.modes;
  }

  /**
   * Does this exercise accepts a leaderboard ?
   * @returns {boolean}
   */
  get HasLeaderboard(): boolean {
    return this.acceptsLeaderboard === undefined || this.acceptsLeaderboard;
  }

  /**
   * Initializes a new instance of Exercise.
   * @param {string} name
   * @param modes
   */
  constructor(name: string, modes: string[] = ["tirettes", "chains", "gram-class", "silhouettes"]) {
    super();
    this.name = name;
    this.sentences = [];
    this.modes = modes;
  }

  /**
   * Adds a sentence to the exercise.
   * @param {Sentence} sentence
   */
  addSentence(sentence: Sentence) {
    this.sentences.push(sentence);
  }

  /**
   * Removes the given sentence from the list.
   * @param {Sentence} sentence
   */
  removeSentence(sentence: Sentence) {
    const index: number = this.sentences.indexOf(sentence);
    if (index !== -1) {
      this.sentences.splice(index, 1);
    }
  }

  /**
   * Locks the leaderboard. The lock only happens
   * on the client side, the server will always
   * accept new scores.
   */
  lockLeaderboard() {
    this.acceptsLeaderboard = false;
  }

  /**
   * Unlocks the leaderboard so that players
   * can register news scores.
   */
  unlockLeaderboard() {
    this.acceptsLeaderboard = true;
  }

  /**
   * Returns true if the exercise has the given mode.
   * @param {string} mode
   * @return {boolean}
   */
  hasMode(mode: string) {
    return this.modes.indexOf(mode) !== -1;
  }

  /**
   * Sets the modes.
   * @param {string[]} modes
   */
  setModes(modes: string[]) {
    this.modes = modes;
  }
}
