import {AbstractGrammaticalAttribute} from "./grammatical-attribute-abstract";

/**
 * This class represents a grammatical number.
 */
export class GrammaticalCase extends AbstractGrammaticalAttribute {
}
