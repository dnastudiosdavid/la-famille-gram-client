import {GameState, ISentencesGame} from "./sentences-game-interface";
import {SentencesExercise} from "./sentences-exercise";
import {Sentence} from "./sentence";
import {IResult, ResultCompletionLevel} from "./result-interface";
import {List} from "linqts";
import {SQueue} from "../../../../model/structures/queue";
import {EventEmitter} from "@angular/core";
import {IActivity} from "../../../../model/activity-interface";
import {Exclude, Type} from "class-transformer";
import {SentencesActivity} from "../activity/sentences-activity";
import {IGame, IGameEndedType} from "../../../../model/game-interface";
import {ISentencesActivityInterface} from "../activity/sentences-activity-interface";
import {makeAction} from "../../../../model/activity-action";

/**
 * This class is the base implementation of
 * a game and implements all the common features
 * of a game. It does not provide a fully playable
 * game by itself though, it can't be used alone.
 */
export abstract class AbstractSentencesGame implements ISentencesGame, IGame {

  /**
   * This event is raised when the sentence
   * changes.
   */
  @Exclude()
  sentenceChanged: EventEmitter<Sentence>;

  /**
   * This event is raised when the game
   * ends.
   */
  @Exclude()
  ended: EventEmitter<IGameEndedType>;

  /**
   * This event is raised when the progression changes.
   */
  @Exclude()
  progressionChanged: EventEmitter<void>;

  /**
   * The current state of the game.
   */
  protected state: GameState;

  /**
   * The exercise on which the game is based on.
   */
  @Type(() => SentencesExercise)
  protected exercise: SentencesExercise;

  /**
   * Each new sentence comes with a detailed result.
   */
  protected results: List<IResult>;

  /**
   * An activity holds all the data related to the
   * progress of a student during the exercise.
   */
  protected activity: ISentencesActivityInterface;

  /**
   * Queue of sentences.
   */
  protected sentences: SQueue<Sentence>;

  /**
   * The current sentence.
   */
  protected currentSentence: Sentence;

  /**
   * The environment of the game.
   */
  protected hero: string;

  /**
   * The student's name.
   */
  protected student: string;

  /**
   * Flag to know if the score has been locked.
   */
  protected scoreLocked: boolean;

  /**
   * The game mode as a string representation.
   */
  protected mode: string;

  /**
   * Ctor.
   */
  protected constructor() {
    this.state = GameState.Created;
    this.results = new List<IResult>();
    this.sentenceChanged = new EventEmitter();
    this.progressionChanged = new EventEmitter();
    this.ended = new EventEmitter();
    this.mode = this.getIdentifier();
  }

  /**
   * Gets the current result which holds the scores
   * as well as the current sentence data.
   */
  get CurrentResult(): IResult {
    if (this.results.Count() > 0) {
      return this.results.ElementAt(this.results.Count() - 1);
    } else {
      return null;
    }
  }

  /**
   * Loads the given raw data
   */
  static loadInto<T extends IResult>(game: AbstractSentencesGame, raw: Object, loadResultFunc: (raw: Object) => T) {

    // load results
    const results: List<IResult> = new List<IResult>();
    for (const res of raw["results"]["_elements"]) {
      const result: T = loadResultFunc(res);
      results.Add(result);
    }
    game.results = results;

    const sentences: Sentence[] = game.exercise.Sentences;
    const queue: SQueue<Sentence> = new SQueue<Sentence>();
    const queueLength = raw["sentences"]["_store"].length;
    for (let i = 0; i < queueLength; i++) {
      // @ts-ignore
      queue.enqueue(game.exercise.Sentences[sentences.length - queueLength + i]);
    }
    game.sentences = queue;
    // @ts-ignore
    game.activity = SentencesActivity.load(raw["activity"]);
    // @ts-ignore
    game.currentSentence = sentences[sentences.length - 1 - queue.Count];
  }

  /**
   * @inheritDoc
   */
  abstract getIdentifier(): string;

  /**
   * Gets the mode.
   */
  getMode(): string {
    return this.mode;
  }

  /**
   * @inheritDoc
   */
  getHero(): string {
    return this.hero;
  }

  /**
   * @inheritDoc
   */
  getStudent(): string {
    return this.student;
  }

  /**
   * @inheritDoc
   */
  start(exercise: SentencesExercise, hero: string, student: string) {
    this.exercise = exercise;
    this.hero = hero;
    this.student = student;
    this.state = GameState.InProgress;
    this.sentences = new SQueue<Sentence>();
    for (const sentence of exercise.Sentences) {
      this.sentences.enqueue(sentence);
    }
    this.activity = this.makeActivity(student, exercise.Code);
    this.next();
  }

  /**
   * @inheritDoc
   */
  isInProgress(): boolean {
    return this.state === GameState.InProgress;
  }

  /**
   * @inheritDoc
   */
  getProgressLabel(): string {
    return "Question";
  }

  /**
   * @inheritDoc
   */
  getCurrentStep(): number {
    return this.exercise.Sentences.indexOf(this.currentSentence) + 1;
  }

  /**
   * @inheritDoc
   */
  getStepsAmount(): number {
    return this.exercise.Sentences.length;
  }

  /**
   * Gets the current sentence.
   */
  getCurrentSentence(): Sentence {
    return this.CurrentResult.getSentence();
  }

  /**
   * @inheritDoc
   */
  canGoToNextStep(): boolean {
    if (!this.CurrentResult) {
      return true;
    }

    let result: boolean = this.CurrentResult.getCompletionLevel() === ResultCompletionLevel.Enough;
    result = result || this.CurrentResult.getCompletionLevel() === ResultCompletionLevel.Full;
    result = result && this.state === GameState.InProgress;
    return result;
  }

  /**
   * @inheritDoc
   */
  isStepCompleted(): boolean {
    // return this.CurrentResult.SolutionsFound.length === this.CurrentResult.SolutionsAmount;
    return this.CurrentResult.getCompletionLevel() === ResultCompletionLevel.Full;
  }

  /**
   * @inheritDoc
   */
  getScore(): number {
    return this.results.Sum(r => r.getScore());
  }

  /**
   * @inheritDoc
   */
  getResults(): IResult[] {
    return this.results.ToArray();
  }

  /**
   * @inheritDoc
   */
  getActivity(): IActivity {
    return this.activity;
  }

  /**
   * @inheritDoc
   */
  getState(): GameState {
    return this.state;
  }

  /**
   * @inheritDoc
   */
  getExercise(): SentencesExercise {
    return this.exercise;
  }

  /**
   * @inheritDoc
   */
  next(): boolean {
    if (this.canGoToNextStep()) {
      const sentence: Sentence = this.getNextSentence();
      if (this.CurrentResult) {
        this.activity.addStep(this.CurrentResult.getStep());
      }
      if (sentence) {
        this.setSentence(sentence);
        this.results.Add(this.getResult(sentence));
        this.sentenceChanged.emit();
        this.progressionChanged.emit();
      } else {
        this.end();
        return true;
      }
    }
    return false;
  }

  /**
   * @inheritDoc
   */
  isScoreLocked(): boolean {
    return this.scoreLocked;
  }

  /**
   * @inheritDoc
   */
  lockScore() {
    this.scoreLocked = true;
  }

  /**
   * Provides a convenient way of adding an action to the activity without
   * specifying time and scope each time.
   */
  addAction(action: string, outcome: string = "", data: any = null): void {
    this.activity.addAction(makeAction(this.getIdentifier(), action, outcome, data));
  }

  /**
   * Ends the game.
   */
  protected end() {
    this.activity.complete();
    this.state = GameState.Over;
    this.ended.emit({activity: this.activity});
  }

  /**
   * Gets the next sentence in the queue.
   */
  protected getNextSentence(): Sentence {
    return this.sentences.dequeue();
  }

  /**
   * Gets a new result.
   */
  protected abstract getResult(sentence: Sentence): IResult;

  /**
   * Gets a new activity.
   */
  protected abstract makeActivity(student: string, exercise: string): ISentencesActivityInterface;

  /**
   * Sets the sentence in progress.
   */
  protected setSentence(sentence: Sentence) {
    this.currentSentence = sentence;
  }
}
