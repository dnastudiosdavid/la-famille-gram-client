/**
 * This class represents a grammatical class of a word.
 */
export class GrammaticalClass {

  /**
   * @see Id
   */
  private id: string;

  /**
   * @see Name
   */
  private name: string;

  /**
   * @see RequireChain
   */
  private requireChain: boolean;

  /**
   * @see RequireGender
   */
  private requireGender: boolean;

  /**
   * The default gender.
   */
  private defaultGender: string;

  /**
   * @see RequireNumber
   */
  private requireNumber: boolean;

  /**
   * The default number.
   */
  private defaultNumber: string;

  /**
   * @see RequirePerson
   */
  private requirePerson: boolean;

  /**
   * @see DefaultPerson
   */
  private defaultPerson: string;

  /**
   * @see IsInvariable
   */
  private isInvariable: boolean;

  /**
   * The identifier of this grammatical class.
   */
  get Id(): string {
    return this.id;
  }

  /**
   * The name of this grammatical class.
   */
  get Name(): string {
    return this.name;
  }

  /**
   * Indicates if this class requires a chain
   * of agreement.
   */
  get RequireChain(): boolean {
    return this.requireChain;
  }

  /**
   * Indicates if the class requires a gender.
   */
  get RequireGender(): boolean {
    return this.requireGender;
  }

  /**
   * Indicates if the class requires a number.
   */
  get RequireNumber(): boolean {
    return this.requireNumber;
  }

  /**
   * Indicates if the class requires a person.
   */
  get RequirePerson(): boolean {
    return this.requirePerson;
  }

  /**
   * The default person.
   */
  get DefaultPerson(): string {
    return this.defaultPerson;
  }

  /**
   * The default number.
   */
  get DefaultNumber(): string {
    return this.defaultNumber;
  }

  /**
   * The default person.
   */
  get DefaultGender(): string {
    return this.defaultGender;
  }

  /**
   * Indicates if the class is invariable or not.
   */
  get IsInvariable(): boolean {
    return this.isInvariable || this.id === "inv";
  }

  /**
   * Initializes a new instance of a grammatical class.
   */
  public constructor(id: string, name: string, requireChain: boolean,
                     requireGender: boolean, requireNumber: boolean, requirePerson: boolean,
                     defaultGender: string = "", defaultNumber: string = "",
                     defaultPerson: string = "", isInvariable: boolean = false) {
    this.id = id;
    this.name = name;
    this.requireChain = requireChain;
    this.requireGender = requireGender;
    this.requireNumber = requireNumber;
    this.requirePerson = requirePerson;
    this.defaultGender = defaultGender;
    this.defaultNumber = defaultNumber;
    this.defaultPerson = defaultPerson;
    this.isInvariable = isInvariable;
  }

  /**
   * Compares this class with another.
   */
  compare(otherClass: GrammaticalClass): boolean {
    return otherClass.Id === this.id;
  }
}
