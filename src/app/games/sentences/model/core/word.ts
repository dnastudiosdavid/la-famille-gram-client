import {Chain} from "./chain";

/**
 * This class defines a word which fills
 * one tirette.
 */
export class Word {

  public static readonly ValueSeparator = ";";
  public static readonly FootprintSeparator = "-";
  public static readonly CurrentVersion = "2";

  /**
   * @see Value
   */
  private value: string;

  /**
   * Word version
   */
  private version: string;

  /**
   * @see Gender
   */
  private genders: string;

  /**
   * @see Number
   */
  private numbers: string;

  /**
   * @see Person
   */
  private persons: string;

  /**
   * @see Cases
   */
  private cases: string;

  /**
   * Initializes a new instance of a Word.
   */
  constructor() {
    this.genders = "";
    this.persons = "";
    this.numbers = "";
    this.cases = "";
  }

  /**
   * The string value of the word.
   * @returns {string}
   */
  get Value(): string {
    return this.value;
  }

  /**
   * The version of the word.
   */
  get Version(): string {
    return this.version;
  }

  /**
   * Sets the value.
   */
  set Value(value: string) {
    this.value = value;
  }

  /**
   * Returns the genders of the word.
   * @returns {string}
   */
  get Genders(): string {
    return this.genders;
  }

  get GendersArray(): string[] {
    return this.getAttributeAsArray(this.genders);
  }

  /**
   * Sets the genders.
   */
  set Genders(value: string) {
    this.genders = value;
  }

  /**
   * Returns the numbers of the word.
   */
  get Numbers(): string {
    return this.numbers;
  }

  get NumbersArray(): string[] {
    return this.getAttributeAsArray(this.numbers);
  }

  /**
   * Sets the numbers.
   */
  set Numbers(value: string) {
    this.numbers = value;
  }

  /**
   * Returns the persons of the word.
   */
  get Persons(): string {
    return this.persons;
  }

  get PersonsArray(): string[] {
    return this.getAttributeAsArray(this.persons);
  }

  /**
   * Sets the persons.
   */
  set Persons(value: string) {
    this.persons = value;
  }

  /**
   * Returns the cases of the word.
   */
  get Cases(): string {
    return this.cases;
  }

  get CasesArray(): string[] {
    return this.getAttributeAsArray(this.cases);
  }

  /**
   * Sets the cases.
   */
  set Cases(value: string) {
    this.cases = value;
  }

  /**
   * Returns the footprint of the word.
   */
  get Footprint(): string {
    return `${this.genders}${Word.FootprintSeparator}${this.persons}${Word.FootprintSeparator}${this.numbers}${Word.FootprintSeparator}${this.cases}`;
  }

  /**
   * Compare this word with the given words and chain to determine if
   * they share the same attributes (gender, person, number and case).
   */
  compare(words: Word[], chain: Chain): boolean {
    return this.getUnmatchingWords(words).length === 0
      && (!chain || this.shareAttributes(this.CasesArray, chain.Cases));
  }

  /**
   * Gets an array of word within the given ones which
   * cannot match with this word.
   */
  getUnmatchingWords(words: Word[]): Word[] {
    const results: Word[] = [];
    for (const word of words) {
      const matches: boolean =
        this.shareAttributes(this.GendersArray, word.GendersArray)
        && this.shareAttributes(this.NumbersArray, word.NumbersArray)
        && this.shareAttributes(this.PersonsArray, word.PersonsArray)
        && this.shareAttributes(this.CasesArray, word.CasesArray);

      if (!matches) {
        results.push(word);
      }
    }
    return results;
  }


  /**
   * Returns true if the two arrays share at least one
   * same value or if one of the arrays is empty.
   */
  shareAttributes(array1: string[], array2: string[]): boolean {
    if (array1.length === 0 || array2.length === 0) {
      return true;
    }
    return array1.filter(v => array2.indexOf(v) !== -1).length > 0;
  }

  /**
   * Returns the given attribute as an array.
   */
  private getAttributeAsArray(attribute: string): string[] {
    if (attribute === "") {
      return [];
    }
    return attribute.split(this.version === Word.CurrentVersion ? Word.ValueSeparator : "");
  }
}
