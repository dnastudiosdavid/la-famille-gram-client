/**
 * This class defines a chain. A chain is composed of "tirettes"
 * which are word selectors (they act like a <select> component).
 * A chain wraps these selectors in a grammatical group (logically
 * speaking) so that the app can compare the selected words and
 * determine if the given part of sentence is correct or not.
 */
export class Chain {

  /**
   * @see Name
   */
  private readonly name: string;

  /**
   * @see Cases
   */
  private cases: string[];

  /**
   * Gets the name of the chain.
   */
  get Name(): string {
    return this.name;
  }

  /**
   * Gets the number of the chain wrapped
   * in a string type.
   */
  get Number(): string {
    return this.name.split(" ")[1];
  }

  /**
   * Gets the chain's cases.
   */
  get Cases(): string[] {
    return this.cases;
  }

  /**
   * Sets the chain's case.
   */
  set Cases(value: string[]) {
    this.cases = value;
  }

  /**
   * Initializes a new instance of Chain.
   */
  constructor(name: string, cases: string[] = null) {
    this.name = name;
    this.cases = cases || [];
  }

  /**
   * Compares this chain wit the other chain
   * and returns true if it matches.
   */
  compare(otherChain: Chain) {
    return otherChain.Number === this.Number;
  }
}
