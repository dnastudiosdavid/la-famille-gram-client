import {Chain} from "./chain";
import {Tirette} from "./tirette";
import {Type} from "class-transformer";
import {Word} from "./word";
import {GrammaticalClass} from "./grammatical-class";
import {List} from "linqts";

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function t() {
  return Tirette;
}

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function c() {
  return Chain;
}

/**
 * This class describes a sentence to be solved
 * by the player.
 * TODO: this structure holds specific methods used by the tirettes mode - they should be moved out of this class
 */
export class Sentence {

  /**
   * The maximum chains in a sentence.
   */
  static readonly MAX_CHAINS_PER_SENTENCE = 5;

  /**
   * @see Name
   */
  private name: string;

  /**
   * @see Tirettes
   */
  @Type(t)
  private tirettes: Tirette[];

  /**
   * @see Chains
   */
  @Type(c)
  private chains: Chain[];

  /**
   * Initializes a new intance of a sentence.
   */
  public constructor(name: string) {
    this.name = name;
    // the sentence will always have
    // at least one chain of agreement
    this.chains = [new Chain("Chaîne 1")];
    this.tirettes = [];
  }

  /**
   * The name (used to identify) of this sentence.
   */
  get Name(): string {
    return this.name;
  }

  /**
   * The logical chains of agreement contained
   * in this sentence.
   */
  get Chains(): Chain[] {
    return this.chains;
  }

  /**
   * The list of tirettes of this chain.
   */
  get Tirettes(): Tirette[] {
    return this.tirettes;
  }

  /**
   * Renames the sentence.
   */
  rename(name: string) {
    this.name = name;
  }

  /**
   * Adds a tirette in the sentence.
   */
  addTirette(tirette: Tirette) {
    this.tirettes.push(tirette);
  }

  /**
   * Removes the given tirette from this sentence.
   */
  removeTirette(tirette: Tirette) {
    const index: number = this.tirettes.indexOf(tirette);
    if (index !== -1) {
      this.tirettes.splice(index, 1);
    }
  }

  /**
   * Adds a chain of agreement in the sentence.
   */
  addChain() {
    if (this.chains.length < Sentence.MAX_CHAINS_PER_SENTENCE) {
      this.chains.push(new Chain("Chaine " + (this.chains.length + 1)));
    }
  }

  /**
   * Removes a chain agreement from the sentence.
   */
  removeChain() {
    // we will never remove the first chain.
    if (this.chains.length > 1) {
      this.chains.splice(this.chains.length - 1, 1);
    }
  }

  /**
   * Gets all the possible solutions for this sentence.
   */
  getCorrectSentences(): string[] {
    const combinations: number[][] = this.getCombinations();
    const solutions: string[] = [];
    for (const combination of combinations) {
      if (this.isCorrect(combination.join(""))) {
        solutions.push(this.getTextByIndexes(combination.join("")));
      }
    }
    return solutions;
  }

  /**
   * Gets the correct combinations.
   */
  getCorrectCombinations(): string[] {
    const combinations: number[][] = this.getCombinations();
    const solutions: string[] = [];
    for (const combination of combinations) {
      if (this.isCorrect(combination.join(""))) {
        solutions.push(combination.join(""));
      }
    }
    return solutions;
  }

  /**
   * Gets the words of the given combination of indexes.
   */
  getWordsOf(indexes: string): Word[] {
    const words: Word[] = [];
    const splitted: string[] = indexes.split("");
    for (let i = 0; i < splitted.length; i++) {
      const tirette: Tirette = this.tirettes[i];
      words.push(tirette.Words[splitted[i]]);
    }
    return words;
  }

  /**
   * Gets the grammatical classes of the sentence in the
   * same order as the tirettes.
   */
  getGrammaticalClasses(): GrammaticalClass[] {
    return new List<Tirette>(this.tirettes).Select(t => t.GrammaticalClass).ToArray();
  }

  /**
   * Returns true if the sentence made by the indexes is correct.
   */
  isCorrect(indexes: string): boolean {
    return this.getWrongIndexes(indexes).length === 0;
  }

  /**
   * Returns the wrong indexes in the given
   * combination. An index is considered 'wrong'
   * whenever the word at its position doesn't match
   * at least half of the words of its chain (this is to
   * avoid considering everything is wrong).
   */
  getWrongIndexes(combination: string): number[] {
    const splitted: string[] = combination.split("");
    const chains: any = {};
    const result: number[] = [];
    // group words by chain
    for (let i = 0; i < splitted.length; i++) {
      const tirette: Tirette = this.tirettes[i];
      if (!tirette.Chain) {
        continue;
      }
      if (!chains.hasOwnProperty(tirette.Chain.Name)) {
        chains[tirette.Chain.Name] = [];
      }
      chains[tirette.Chain.Name].push(tirette.Words[splitted[i]]);
    }
    // compute the result
    for (const chainName in chains) {
      const chain = this.tirettes.find(tir => tir.Chain && tir.Chain.Name === chainName).Chain;
      if (chains.hasOwnProperty(chainName)) {
        const chainWords: Word[] = chains[chainName];
        const wrongWords: any[] = [];
        for (const word of chainWords) {
          // TODO: this doesn't take into account cases on chain
          if (!word.compare(chainWords, chain)) {
            wrongWords.push({
              word: word,
              count: word.getUnmatchingWords(chainWords).length
            });
          }
        }

        if (wrongWords.length === 0) {
          continue;
        }

        const max: number = new List<any>(wrongWords).Max(w => w.count);
        const sorted: any[] = new List<any>(wrongWords)
          .OrderByDescending(w => w.count).Where(w => w.count === max).ToArray();

        for (const word of sorted) {
          result.push(this.tirettes.indexOf(this.getTiretteOf(word.word)));
        }
      }
    }
    return result;
  }

  /**
   * Gets the word at the given tirette and at the given index.
   */
  getWordAt(tiretteIndex: number, wordIndex: number) {
    return this.tirettes[tiretteIndex].Words[wordIndex];
  }

  /**
   * Gets the text value in a sentence given the indexes.
   */
  getTextByIndexes(indexes: string): string {
    let result: string = "";
    indexes.split("").forEach((c, i) => {
      const word: string = this.getWordAt(i, parseInt(c, 10)).Value;
      result += word;
      if (i < indexes.length - 1 && !word.endsWith("'")) {
        result += " ";
      }
    });
    return result;
  }

  /**
   * Gets all the possible combinations of words in the sentence.
   */
  private getCombinations(): number[][] {
    const result: number[][] = [];
    const current: number[] = [];
    // start at [0,0,..]
    for (let i = 0; i < this.tirettes.length; i++) {
      current.push(0);
    }
    const firstTirette: Tirette = this.tirettes [0];
    if (firstTirette) {
      for (let i = 0; i < firstTirette.Words.length; i++) {

        current[0] = i;
        // trick to clone the array and
        // push a new one into the array
        // of combinations
        result.push(current.slice(0));

        if (i === firstTirette.Words.length - 1) {
          for (let j = 1; j < this.tirettes.length; j++) {
            const tirette: Tirette = this.tirettes[j];
            if (current[j] < tirette.Words.length - 1) {
              current[j]++;
              i = -1;
              break;
            } else {
              current[j] = 0;
            }
          }
        }
      }
    }
    return result;
  }

  /**
   * Gets the given chain of the given word.
   */
  private getTiretteOf(word: Word): Tirette {
    for (const tirette of this.tirettes) {
      if (tirette.hasWord(word)) {
        return tirette;
      }
    }
    return null;
  }
}
