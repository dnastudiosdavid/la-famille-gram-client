import {Sentence} from "./sentence";
import {IResult, ResultCompletionLevel} from "./result-interface";
import {ActivityStep} from "../activity/activity-step";

/**
 * Provides the base implementation of the IResult
 * interface.
 */
export abstract class AbstractResult implements IResult {

  /**
   * The sentence linked to this result.
   */
  protected readonly sentence: Sentence;

  /**
   * The step containing all the input
   * data of this result.
   */
  protected step: ActivityStep;

  /**
   * Ctor.
   */
  protected constructor(sentence: Sentence) {
    this.sentence = sentence;
    this.step = this.makeActivityStep(sentence);
  }

  /**
   * @inheritDoc
   */
  getLabel(): string {
    return this.sentence.Name;
  }

  /**
   * @inheritDoc
   */
  getSentence(): Sentence {
    return this.sentence;
  }

  /**
   * @inheritDoc
   */
  getStep(): ActivityStep {
    return this.step;
  }

  /**
   * @inheritDoc
   */
  abstract getCompletionLevel(): ResultCompletionLevel;

  /**
   * @inheritDoc
   */
  abstract getScore(): number;

  /**
   * @inheritDoc
   */
  abstract getMaxScore(): number;

  /**
   * Gets a new activity step.
   */
  protected abstract makeActivityStep(sentence: Sentence): ActivityStep;
}

