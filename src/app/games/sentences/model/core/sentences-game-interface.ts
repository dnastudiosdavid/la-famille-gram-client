import {SentencesExercise} from "./sentences-exercise";
import {Sentence} from "./sentence";
import {IResult} from "./result-interface";
import {IActivity} from "../../../../model/activity-interface";
import {IGame} from "../../../../model/game-interface";

/**
 * This interface describes a game. A game is
 * always based on an Exercise object and is
 * a sequence of Sentence objects. Each implementation
 * of this interface represents a game mode.
 */
export interface ISentencesGame extends IGame {

  /**
   * Gets the identifier of the game.
   */
  getIdentifier(): string;

  /**
   * Gets the mode.
   */
  getMode(): string;

  /**
   * Gets the hero chosen by the player.
   */
  getHero(): string;

  /**
   * Gets the student's nickname.
   */
  getStudent(): string;

  /**
   * Starts the given exercise.
   */
  start(exercise: SentencesExercise, hero: string, student: string);

  /**
   * Gets the exercise of the game.
   */
  getExercise(): SentencesExercise;

  /**
   * Gets the current state of the game.
   */
  getState(): GameState;

  /**
   * Returns true if the game is in progress. This
   * is a bit redundant with getState BUT it's painful
   * to play with enums in the view.
   */
  isInProgress(): boolean;

  /**
   * Gets the current sentence.
   */
  getCurrentSentence(): Sentence;

  /**
   * Gets the current step.
   */
  getCurrentStep(): number;

  /**
   * Gets the amount of steps.
   */
  getStepsAmount(): number;

  /**
   * Returns true if the requirements to go
   * to the next sentence are fulfilled.
   */
  canGoToNextStep(): boolean;

  /**
   * Returns true if the step is fully completed.
   */
  isStepCompleted(): boolean;

  /**
   * Gets the score. Every game has a score.
   */
  getScore(): number;

  /**
   * Gets the results of this game.
   */
  getResults(): IResult [];

  /**
   * Gets the activity of this game.
   */
  getActivity(): IActivity;

  /**
   * Goes to the next sentence.
   */
  next(): boolean;

  /**
   * Tells the game that the score has been saved.
   */
  lockScore();

  /**
   * Returns true if the score is locked and can't be
   * saved anymore. Should be somewhere else but well...
   */
  isScoreLocked(): boolean;
}

/**
 * Defines the different states a game can have.
 */
export enum GameState {
  Created,
  InProgress,
  Over
}
