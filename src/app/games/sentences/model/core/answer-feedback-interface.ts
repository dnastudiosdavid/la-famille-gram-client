import {ActivityInputBlock} from "../activity/activity-input-block";

/**
 * This interface describes the basic structure of a
 * feedback which is given when the player validates an
 * answer.
 */

export interface IAnswerFeedback {

  /**
   * Gets the score change after the given answer.
   */
  getScoreChange(): number;

  /**
   * Determines if the result was correct or not.
   */
  getResultState(): ResultState;

  /**
   * Gets the student's input in the form
   * of an array of blocks.
   */
  getInput(): ActivityInputBlock[];
}

/**
 * Gets the completion level of the result.
 */
export enum ResultState {
  Void = "clean",
  Correct = "correct",
  Incorrect = "incorrect",
  Already = "already"
}
