import {GrammaticalClass} from "./grammatical-class";
import {Word} from "./word";
import {Chain} from "./chain";
import {Type} from "class-transformer";
import {List} from "linqts";

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function g() {
  return GrammaticalClass;
}

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function c() {
  return Chain;
}

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function w() {
  return Word;
}

/**
 * This class is a group of word of the same
 * grammatical class, meaning that they share
 * the same "grammatical attributes" in terms
 * of gender, person and number.
 */
export class Tirette {

  /**
   * @see GrammaticalClass
   */
  @Type(g)
  private grammaticalClass: GrammaticalClass;

  /**
   * @see Chain
   */
  @Type(c)
  private chain: Chain;

  /**
   * @see Words
   */
  @Type(w)
  private words: Word[];

  /**
   * Gets the grammatical class of the tirette.
   */
  get GrammaticalClass(): GrammaticalClass {
    return this.grammaticalClass;
  }

  /**
   * Gets the grammatical class of the tirette.
   */
  set GrammaticalClass(value: GrammaticalClass) {
    this.grammaticalClass = value;
  }

  /**
   * Gets the chain of agreement of the tirette.
   */
  get Chain(): Chain {
    return this.chain;
  }

  /**
   * Sets the chain agreement of the tirette.
   */
  set Chain(value: Chain) {
    this.chain = value;
  }

  /**
   * Gets all the word of this tirette.
   */
  get Words(): Word[] {
    return this.words;
  }

  /**
   * Initializes a new instance of Tirette.
   */
  constructor() {
    this.words = [];
  }

  /**
   * Adds a word in the tirette.
   */
  addWord(word: Word) {
    this.words.push(word);
  }

  /**
   * Removes the word from the tirette.
   */
  removeWord(word: Word) {
    const index: number = this.words.indexOf(word);
    if (index !== -1) {
      this.words.splice(index, 1);
    }
  }

  /**
   * Checks if this tirette contains the given word.
   *
   * Checks on the lowercase value.
   */
  hasWord(word: Word): boolean {
    return new List<Word>(this.words).FirstOrDefault((w) => w.Value.toLocaleLowerCase() === word.Value.toLocaleLowerCase()) != null;
  }
}
