/**
 * This interface exposes the methods of a
 * grammatical attribute.
 */
export interface IGrammaticalAttribute {

  /**
   * Gets the string representation of the values.
   */
  getValue(): string;

  /**
   * Gets the value to display to the user.
   */
  getDisplayLabel(): string;
}
