import {Sentence} from "./sentence";
import {ActivityStep} from "../activity/activity-step";
import {IScoreComponent} from "../../../../model/score-component-interface";

/**
 * This interface describes a result. Each step
 * of a game comes with a result which can provide
 * more or less details about the score made by
 * the player.
 */
export interface IResult extends IScoreComponent {

  /**
   * Gets the completion level of the result.
   */
  getCompletionLevel(): ResultCompletionLevel;

  /**
   * Gets the sentence of this result.
   */
  getSentence(): Sentence;

  /**
   * Gets the step object of this result.
   */
  getStep(): ActivityStep;
}

/**
 * Gets the completion level of the result.
 */
export enum ResultCompletionLevel {
  // the player did not do enough to go to the next sentence
  NotEnough,

  // the player made enough to be able to go to the next sentence
  Enough,

  // the player completed everything
  Full
}
