import {Word} from "../core/word";
import {Tirette} from "../core/tirette";

/**
 * This class links a word with the grammatical
 * class the players put below it.
 */
export class SilhouettesSentenceUnit {

  /**
   * The tirette.
   */
  private readonly tirette: Tirette;

  /**
   * The associated word.
   */
  private chosenWord: Word[];

  /**
   * @see IsLocked
   */
  private isLocked: boolean;

  /**
   * Gets the tirette
   */
  get Tirette(): Tirette {
    return this.tirette;
  }

  /**
   * Sets the chosen word.
   */
  set ChosenWord(value: Word[]) {
    this.chosenWord = value;
  }

  /**
   * Gets the chosen class.
   */
  get ChosenWord(): Word[] {
    return this.chosenWord;
  }

  /**
   * Returns the is locked flag. This
   * is a stupid property in a dumb object
   * and does not prevent the unit to be
   * actually locked.
   */
  get IsLocked(): boolean {
    return this.isLocked;
  }

  /**
   * Ctor.
   */
  constructor(tirette: Tirette) {
    this.tirette = tirette;
    this.chosenWord = [];
  }

  /**
   * Turn the locked flag on.
   */
  lock() {
    this.isLocked = true;
  }
}
