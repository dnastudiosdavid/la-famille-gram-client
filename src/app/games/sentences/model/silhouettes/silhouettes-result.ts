import {Sentence} from "../core/sentence";
import {ResultCompletionLevel} from "../core/result-interface";
import {AbstractResult} from "../core/result-abstract";
import {SilhouettesSentenceUnit} from "./silhouettes-sentence-unit";
import {IAnswerFeedback, ResultState} from "../core/answer-feedback-interface";
import {SilhouettesAnswerFeedback} from "./silhouettes-answer-feedback";
import {GrammaticalClass} from "../core/grammatical-class";
import {Tirette} from "../core/tirette";
import {Word} from "../core/word";
import {ActivityStep} from "../activity/activity-step";
import {List} from "linqts";
import {ActivityInputBlock} from "../activity/activity-input-block";
import {ActivityInputLine} from "../activity/activity-input-line";
import {plainToClass} from "class-transformer";

/**
 * This is the implementation of the result object
 * in the game of silhouettes.
 */
export class SilhouettesResult extends AbstractResult {

  /**
   * Incremented score value when the player
   * gives a valid answer.
   */
  static readonly SCORE_CORRECT: number = 50;

  /**
   * Decremented score value when the player
   * gives a wrong answer.
   */
  static readonly SCORE_INCORRECT: number = -10;

  /**
   * The base score.
   */
  static readonly SCORE_BASE: number = 50;

  /**
   * The current score of this result.
   */
  private score: number;

  /**
   * The completion level.
   */
  private completionLevel: ResultCompletionLevel;

  /**
   * The given combinations. This is an ordered
   * array of strings corresponding to the
   * words given.
   */
  private combinations: string[][];

  /**
   * The random correct combination to make
   * a base sentence.
   */
  private combination: string;

  /**
   * Ctor.
   */
  constructor(sentence: Sentence) {
    super(sentence);
    this.score = SilhouettesResult.SCORE_BASE;
    this.combinations = [];
    this.completionLevel = ResultCompletionLevel.NotEnough;
  }

  /**
   * Crates a new instance of this result based on raw data.
   */
  static load(raw: Object) {
    // @ts-ignore
    const sentence: Sentence = plainToClass(Sentence, raw["sentence"]);
    const result: SilhouettesResult = new SilhouettesResult(sentence);
    result.score = raw["score"];
    result.completionLevel = raw["completionLevel"];
    result.combination = raw["combination"];
    result.combinations = raw["combinations"];
    // @ts-ignore
    result.step = plainToClass(ActivityStep, raw["step"]);
    return result;
  }

  /**
   * TODO: implementation
   */
  getCompletionLevel(): ResultCompletionLevel {
    return this.completionLevel;
  }

  /**
   * @inheritDoc
   */
  getScore(): number {
    return this.score;
  }

  /**
   * @inheritDoc
   */
  getMaxScore(): number {
    return -1;
  }

  /**
   * Gets the words available to make a sentence.
   */
  getWords(): Word[] {
    return this.sentence.getWordsOf(this.combination);
  }

  /**
   * Validates the given combination.
   *
   * There are 2 cases to check to know if the
   * given sequence is correct:
   *
   * (1) the word must have the same grammatical class as the tirette
   * (2) the words in the same chain must share the same grammatical attributes
   */
  validate(sequence: SilhouettesSentenceUnit[]): IAnswerFeedback {

    let result = true;
    const combination: string[] = [];
    const blocks: ActivityInputBlock[] = [];
    const wrongClassSeqIndexes: number[] = [];
    const wrongChainWords: Word[] = [];

    // just ensure we dont have different lengths
    if (sequence.length !== this.sentence.Tirettes.length) {
      console.warn("The given sequence length does not equal the amount of sequences in the base sentence.");
      result = false;
    } else {
      const chains: any = {};

      for (let i = 0; i < sequence.length; i++) {
        const unit: SilhouettesSentenceUnit = sequence[i];
        let unitResult = true;

        if (unit.ChosenWord.length <= 0) {
          unitResult = false;
          combination.push(null);
        } else {
          combination.push(unit.ChosenWord[0].Value.toLowerCase());
          // TODO: this could be put in the Sentence class just like the isCorrect method but with a different input (ordered array of words)
          let grammClass: GrammaticalClass = null;
          for (const tirette of this.sentence.Tirettes) {
            if (tirette.hasWord(unit.ChosenWord[0])) {
              grammClass = tirette.GrammaticalClass;
            }
          }

          // group words by chain
          const tirette: Tirette = this.sentence.Tirettes[i];
          if (tirette.Chain) {
            if (!chains.hasOwnProperty(tirette.Chain.Name)) {
              chains[tirette.Chain.Name] = [];
            }
            chains[tirette.Chain.Name].push(unit.ChosenWord[0]);

            // check for the same class
            if (!grammClass || !unit.Tirette.GrammaticalClass.compare(grammClass)) {
              unitResult = false;
            }
          }
        }
        result = result && unitResult;

        if (!unitResult) {
          wrongClassSeqIndexes.push(i);
        }
      }

      // check that chains are correct
      for (const chainName in chains) {
        if (chains.hasOwnProperty(chainName)) {
          let chainResult = true;
          for (const word of chains[chainName]) {
            const chainWords: Word[] = chains[chainName];
            const wordResult = word.compare(chainWords);
            const chainLength: number = chainWords.length;

            chainResult = chainResult && wordResult;
            if (!wordResult && word.getUnmatchingWords(chainWords).length >= Math.ceil(chainLength / 2)) {
              wrongChainWords.push(word);
            }
          }
          result = result && chainResult;
        }
      }
    }

    if (!this.hasSequence(this.combinations, combination)) {
      this.combinations.push(combination);
      for (let i = 0; i < sequence.length; i++) {
        const unit: SilhouettesSentenceUnit = sequence[i];
        if (unit.ChosenWord) {
          const word: Word = unit.ChosenWord[0];
          const reasons: string[] = [];
          const wrongClass: boolean = wrongClassSeqIndexes.indexOf(i) !== -1;
          const wrongChain: boolean = wrongChainWords.indexOf(word) !== -1;
          if (wrongClass) {
            reasons.push("classe");
          }
          if (wrongChain) {
            reasons.push("accord");
          }
          blocks.push(new ActivityInputBlock(word ? word.Value : "", !wrongClass && !wrongChain, reasons));
        } else {
          blocks.push(new ActivityInputBlock("Invariable", true));
        }
      }

      this.step.addInput(new ActivityInputLine(result, blocks));

      let scoreChange = 0;
      if (result) {
        scoreChange = SilhouettesResult.SCORE_CORRECT;
        this.score += scoreChange;
        this.completionLevel = ResultCompletionLevel.Enough;
      } else {
        scoreChange = SilhouettesResult.SCORE_INCORRECT;
        this.score += scoreChange;
        if (this.score < 0) {
          this.score = 0;
        }
      }
      return result ? new SilhouettesAnswerFeedback(SilhouettesResult.SCORE_CORRECT, ResultState.Correct) : new SilhouettesAnswerFeedback(SilhouettesResult.SCORE_INCORRECT, ResultState.Incorrect);
    } else {
      return new SilhouettesAnswerFeedback(0, ResultState.Already);
    }
  }

  /**
   * @inheritDoc
   */
  protected makeActivityStep(sentence: Sentence): ActivityStep {
    this.combination = this.getRandomCombination(sentence);
    return new ActivityStep(sentence.Name, 1, new List<GrammaticalClass>(sentence.getGrammaticalClasses()).Select(g => g.Name).ToArray());
  }

  /**
   * Determines if the given combinations contains the given
   * sequence. This method is used to ensure the player does
   * not get a penalty for giving to times the same answer.
   */
  private hasSequence(combinations: string[][], sequence: string[]): boolean {
    for (let i = 0; i < combinations.length; i++) {
      const combination: string[] = combinations[i];
      if (combination.length !== sequence.length) {
        console.warn("Recorded combination length does not equal the sequence length.");
      }
      let sequenceCheck = true;
      for (let j = 0; j < combination.length; j++) {
        sequenceCheck = sequenceCheck && combination[j] === sequence[j];
      }
      if (sequenceCheck) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gets a random combination.
   */
  private getRandomCombination(sentence: Sentence): string {
    const combinations: string[] = sentence.getCorrectCombinations();
    return combinations[Math.floor(Math.random() * combinations.length)];
  }
}
