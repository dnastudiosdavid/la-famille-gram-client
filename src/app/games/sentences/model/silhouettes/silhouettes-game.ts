import {Sentence} from "../core/sentence";
import {AbstractSentencesGame} from "../core/sentences-game-abstract";
import {IResult} from "../core/result-interface";
import {SilhouettesSentenceUnit} from "./silhouettes-sentence-unit";
import {IAnswerFeedback} from "../core/answer-feedback-interface";
import {SilhouettesResult} from "./silhouettes-result";
import {GameMode} from "../core/game-mode";
import {SentencesActivity} from "../activity/sentences-activity";
import {plainToInstance} from "class-transformer";
import {ISentencesActivityInterface} from "../activity/sentences-activity-interface";

/**
 * Implementation of the silhouettes game. In this
 * mode, you have to build the structure of the
 * given sentence.
 */
export class SilhouettesGame extends AbstractSentencesGame {

  public static readonly Actions = {
    drop: "drop",
    validate: "validate",
    next: "next"
  };

  /**
   * Gets the current result cast.
   */
  get CurResult(): SilhouettesResult {
    return <SilhouettesResult>this.CurrentResult;
  }

  /**
   * Ctor.
   */
  constructor() {
    super();
  }

  /**
   * Crates a new instance of this game based on raw data.
   */
  static load(raw: Object): SilhouettesGame {
    const game: SilhouettesGame = plainToInstance(SilhouettesGame, raw);
    AbstractSentencesGame.loadInto(game, raw, (raw) => SilhouettesResult.load(raw));
    return game;
  }

  /**
   * @inheritDoc
   */
  getIdentifier(): string {
    return GameMode.Silhouettes;
  }

  /**
   * Records an answer.
   */
  validate(combinations: SilhouettesSentenceUnit[]): IAnswerFeedback {
    return this.CurResult.validate(combinations);
  }

  /**
   * Gets a new result object.
   */
  protected getResult(sentence: Sentence): IResult {
    return new SilhouettesResult(sentence);
  }

  /**
   * Gets a new activity.
   */
  protected makeActivity(student: string, exercise: string): ISentencesActivityInterface {
    return new SentencesActivity(student, exercise, this.getIdentifier());
  }
}
