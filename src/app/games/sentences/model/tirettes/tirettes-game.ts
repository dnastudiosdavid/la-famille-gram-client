import {Sentence} from "../core/sentence";
import {AbstractSentencesGame} from "../core/sentences-game-abstract";
import {IResult} from "../core/result-interface";
import {TirettesResult} from "./tirettes-result";
import {TirettesAnswerFeedback} from "./tirettes-answer-feedback";
import {GameMode} from "../core/game-mode";
import {SentencesActivity} from "../activity/sentences-activity";
import {plainToInstance} from "class-transformer";
import {ISentencesActivityInterface} from "../activity/sentences-activity-interface";

/**
 * Implementation of the tirettes game. In this
 * mode, you have to build sentences using different
 * words. The sentences must be grammatically correct.
 */
export class TirettesGame extends AbstractSentencesGame {

  public static readonly Actions = {
    tirette: "tirette",
    validate: "validate",
    next: "next",
    solution: "show-solutions",
    save: "save"
  };

  /**
   * Gets the current result casted.
   */
  get CurResult(): TirettesResult {
    return <TirettesResult>this.CurrentResult;
  }

  /**
   * Ctor.
   */
  constructor() {
    super();
  }

  /**
   * Crates a new instance of this game based on raw data.
   */
  static load(raw: Object): TirettesGame {
    const game: TirettesGame = plainToInstance(TirettesGame, raw);
    AbstractSentencesGame.loadInto(game, raw, (raw) => TirettesResult.load(raw));
    return game;
  }

  /**
   * @inheritDoc
   */
  getIdentifier(): string {
    return GameMode.Tirettes;
  }

  /**
   * Records an answer.
   */
  validate(combination: string): TirettesAnswerFeedback {
    return this.CurResult.validate(combination);
  }

  /**
   * Gets a new result object.
   */
  protected getResult(sentence: Sentence): IResult {
    return new TirettesResult(sentence);
  }

  /**
   * Gets a new activity.
   */
  protected makeActivity(student: string, exercise: string): ISentencesActivityInterface {
    return new SentencesActivity(student, exercise, this.getIdentifier());
  }
}
