import {Sentence} from "../core/sentence";
import {ResultCompletionLevel} from "../core/result-interface";
import {AbstractResult} from "../core/result-abstract";
import {TirettesAnswerFeedback} from "./tirettes-answer-feedback";
import {ResultState} from "../core/answer-feedback-interface";
import {ActivityStep} from "../activity/activity-step";
import {ActivityInputLine} from "../activity/activity-input-line";
import {ActivityInputBlock} from "../activity/activity-input-block";
import {Word} from "../core/word";
import {plainToClass} from "class-transformer";

/**
 * This is the implementation of the result object
 * in the game of tirettes. There are multiple solutions
 * to be found in that game and the player must to
 * find at least X solutions.
 */
export class TirettesResult extends AbstractResult {

  /**
   * The minimum amount of solutions required to go to
   * the next sentence. Obviously, if the sentence has
   * less solutions than this value, it should not be
   * taken into account.
   */
  static readonly MIN_SOLUTIONS_TO_GO_NEXT: number = 1;

  /**
   * Incremented score value when the player
   * gives a valid answer.
   */
  static readonly SCORE_CORRECT: number = 30;

  /**
   * Decremented score value when the player
   * gives a wrong answer.
   */
  static readonly SCORE_INCORRECT: number = -10;

  /**
   * The current score of this result.
   */
  private score: number;

  /**
   * @see SolutionsFound
   */
  private solutionsFound: string[];

  /**
   * @see SolutionsAmount
   */
  private readonly solutionsAmount: number;

  /**
   * The given combinations.
   */
  private combinations: string[];

  /**
   * Solutions found in sentence.
   */
  get SolutionsFound(): string[] {
    return this.solutionsFound;
  }

  /**
   * Total of possible solutions.
   */
  get SolutionsAmount(): number {
    return this.solutionsAmount;
  }

  /**
   * Ctor.
   */
  constructor(sentence: Sentence) {
    super(sentence);
    this.solutionsAmount = this.sentence.getCorrectSentences().length;
    this.solutionsFound = [];
    this.score = 0;
    this.combinations = [];
  }

  /**
   * Crates a new instance of this result based on raw data.
   */
  static load(raw: Object) {
    // @ts-ignore
    const sentence: Sentence = plainToClass(Sentence, raw["sentence"]);
    const result: TirettesResult = new TirettesResult(sentence);
    result.score = raw["score"];
    result.combinations = raw["combinations"];
    result.solutionsFound = raw["solutionsFound"];
    // @ts-ignore
    result.step = plainToClass(ActivityStep, raw["step"]);
    return result;
  }

  /**
   * To be fully completed, this result must have all
   * the solutions found. To be enough, it must have
   * at least X solutions (defined in the constants).
   */
  getCompletionLevel(): ResultCompletionLevel {
    const isEnough: boolean = this.solutionsFound.length >= Math.min(this.solutionsAmount, TirettesResult.MIN_SOLUTIONS_TO_GO_NEXT);
    const isCompleted: boolean = this.solutionsFound.length === this.solutionsAmount;
    return isCompleted ? ResultCompletionLevel.Full : isEnough ? ResultCompletionLevel.Enough : ResultCompletionLevel.NotEnough;
  }

  /**
   * @inheritDoc
   */
  getScore(): number {
    return this.score;
  }

  /**
   * @inheritDoc
   */
  getMaxScore(): number {
    return this.solutionsAmount * TirettesResult.SCORE_CORRECT;
  }

  /**
   * Validates the given combination.
   * @param combination
   */
  validate(combination: string): TirettesAnswerFeedback {
    let result = ResultState.Already;
    let scoreChange = 0;
    const isCorrect: boolean = this.sentence.isCorrect(combination);
    const blocks: ActivityInputBlock[] = [];

    if (this.combinations.indexOf(combination) === -1) {

      scoreChange = isCorrect ? TirettesResult.SCORE_CORRECT : TirettesResult.SCORE_INCORRECT;
      this.score += scoreChange;
      if (this.score < 0) {
        this.score = 0;
      }
      this.combinations.push(combination);

      if (isCorrect) {
        this.solutionsFound.push(this.sentence.getTextByIndexes(combination));
        result = ResultState.Correct;
      } else {
        result = ResultState.Incorrect;
      }

      const splitted: string[] = combination.split("");
      const incorrectIndexes: number[] = this.sentence.getWrongIndexes(combination);
      for (let i = 0; i < splitted.length; i++) {
        const word: Word = this.sentence.getWordAt(i, parseInt(splitted[i], 10));
        const isCorrect: boolean = incorrectIndexes.indexOf(i) === -1;
        const reasons: string[] = isCorrect ? [] : ["accord"];
        blocks.push(new ActivityInputBlock(word.Value, isCorrect, reasons));
      }

      this.step.addInput(new ActivityInputLine(isCorrect, blocks));
    }
    return new TirettesAnswerFeedback(scoreChange, result);
  }

  /**
   * @inheritDoc
   */
  protected makeActivityStep(sentence: Sentence): ActivityStep {
    return new ActivityStep(sentence.Name, sentence.getCorrectCombinations().length, []);
  }
}
