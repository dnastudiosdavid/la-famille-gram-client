/**
 * This type represents a part of a complete
 * input given by the student.
 */
export class ActivityInputBlock {

  /**
   * @see Input
   */
  private readonly input: string;

  /**
   * @see IsCorrect
   */
  private readonly isCorrect: boolean;

  /**
   * @see Reasons
   */
  private readonly reasons: string[];

  /**
   * The input given.
   */
  get Input(): string {
    return this.input;
  }

  /**
   * True if this part was considered as correct.
   */
  get IsCorrect(): boolean {
    return this.isCorrect;
  }

  /**
   * Gets the reasons why the user did wrong.
   * @constructor
   */
  get Reasons(): string [] {
    return this.reasons;
  }

  /**
   * Ctor.
   * @param input
   * @param isCorrect
   * @param reasons
   */
  constructor(input: string, isCorrect: boolean, reasons: string[] = []) {
    this.input = input;
    this.isCorrect = isCorrect;
    this.reasons = reasons;
  }
}
