import {ISentencesActivityInterface} from "./sentences-activity-interface";
import {plainToInstance, Type} from "class-transformer";
import {ActivityStep} from "./activity-step";
import {IActivityStep} from "./activity-step-interface";
import {List} from "linqts";
import {ActivityInputLine} from "./activity-input-line";
import {ActivityAbstract} from "../../../../model/activity-abstract";

/**
 * This class is a skeleton of an activity.
 */
export class SentencesActivity extends ActivityAbstract implements ISentencesActivityInterface {

  /**
   * @see getMode
   */
  protected readonly mode: string;

  /**
   * @see getSteps
   */
  @Type(() => ActivityStep)
  protected steps: IActivityStep [];

  /**
   * Ctor.
   */
  constructor(student: string, exercise: string, mode: string) {
    super(student, exercise);
    this.mode = mode;
    this.steps = [];
  }

  /**
   * Crates a new instance of this activity based on raw data.
   * @param raw
   */
  static load(raw: Object): SentencesActivity {
    const activity: SentencesActivity = plainToInstance(SentencesActivity, raw);
    const steps: IActivityStep[] = [];
    for (const step of raw["steps"]) {
      steps.push(plainToInstance(ActivityStep, step));
    }
    activity.steps = steps;
    return activity;
  }

  /**
   * @inheritDoc
   * @return {string}
   */
  getMode(): string {
    return this.mode;
  }

  /**
   * @inheritDoc
   * @param step
   */
  addStep(step: IActivityStep) {
    this.steps.push(step);
  }

  /**
   * @inheritDoc
   * @return {IActivityStep}
   */
  getSteps(): IActivityStep[] {
    return this.steps;
  }

  /**
   * @inheritDoc
   * @return {number}
   */
  getErrorRate(): number {
    const tries: number = new List<IActivityStep>(this.steps).Sum(s => s.getInput().length);
    const wrong: number = new List<IActivityStep>(this.steps).Select(s => s.getInput()).Sum(inputs => new List<ActivityInputLine>(inputs).Count(input => !input.IsCorrect));
    return Math.round(100 * wrong / tries);
  }

  /**
   * @inheritDoc
   * @return {number}
   */
  getCompletionRate(): number {
    const possible: number = new List<IActivityStep>(this.steps).Sum(s => s.getSolutionsAmount());
    const correct: number = new List<IActivityStep>(this.steps).Select(s => s.getInput()).Sum(inputs => new List<ActivityInputLine>(inputs).Count(input => input.IsCorrect));
    return Math.round(100 * correct / possible);
  }
}
