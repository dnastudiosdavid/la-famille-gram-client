import {IActivityStep} from "./activity-step-interface";
import {IActivity} from "../../../../model/activity-interface";

export interface ISentencesActivityInterface extends IActivity {

  /**
   * Gets the game mode.
   */
  getMode(): string;

  /**
   * Adds a step.
   * @param step
   */
  addStep(step: IActivityStep);

  /**
   * Gets the detailled steps to get in here.
   */
  getSteps(): IActivityStep [];

  /**
   * Completes and computes anything that should be
   * done at the end of the game.
   */
  complete();
}
