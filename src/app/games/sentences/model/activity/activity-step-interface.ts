/**
 * This interface describes an activity. An activity
 * holds all the data and progression of a completed
 * exercise.
 */
import {ActivityInputLine} from "./activity-input-line";

export interface IActivityStep {

  /**
   * Gets the base instruction of the step.
   */
  getInstruction(): string;

  /**
   * Gets the amount of possible solutions.
   */
  getSolutionsAmount(): number;

  /**
   * Gets the array of input lines (tries).
   */
  getInput(): ActivityInputLine[];

  /**
   * Adds an input to the step.
   */
  addInput(input: ActivityInputLine);
}
