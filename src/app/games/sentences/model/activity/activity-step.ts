import {IActivityStep} from "./activity-step-interface";
import {ActivityInputLine} from "./activity-input-line";
import {Exclude, Type} from "class-transformer";

/**
 * This abstract class is a skeleton of an activity step.
 */
export class ActivityStep implements IActivityStep {

  /**
   * All the given input by the student.
   */
  @Type(() => ActivityInputLine)
  private readonly inputLines: ActivityInputLine[];

  /**
   * When the last input was given.
   */
  @Exclude()
  protected lastInputAt: Date;

  /**
   * Ctor.
   * @param {string} instruction
   * @param {number} solutionsAmount
   * @param {string[]} givenLine
   */
  constructor(private instruction: string,
              private solutionsAmount: number,
              private givenLine: string []) {
    this.inputLines = [];
    this.lastInputAt = new Date();
  }

  /**
   * @inheritDoc
   */
  getInstruction(): string {
    return this.instruction;
  }

  /**
   * @inheritDoc
   */
  getInput(): ActivityInputLine[] {
    return this.inputLines;
  }

  /**
   * @inheritDoc
   */
  getSolutionsAmount(): number {
    return this.solutionsAmount;
  }

  /**
   * @inheritDoc
   * @param input
   */
  addInput(input: ActivityInputLine) {
    input.setTimeTaken((new Date().getTime() - this.lastInputAt.getTime()) / 1000);
    this.inputLines.push(input);
    this.lastInputAt = new Date();
  }
}
