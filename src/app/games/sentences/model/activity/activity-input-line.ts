import {ActivityInputBlock} from "./activity-input-block";
import {Type} from "class-transformer";

/**
 * This type represents a complete input provided
 * by the student.
 */
export class ActivityInputLine {

  /**
   * @see IsCorrect
   */
  private readonly isCorrect: boolean;

  /**
   * @see Blocks
   */
  @Type(() => ActivityInputBlock)
  private readonly blocks: ActivityInputBlock [];

  /**
   * @see TimeTaken
   */
  private timeTaken: number;

  /**
   * True if this part was considered as correct.
   */
  get IsCorrect(): boolean {
    return this.isCorrect;
  }

  /**
   * Blocks forming this line.
   */
  get Blocks(): ActivityInputBlock [] {
    return this.blocks;
  }

  /**
   * Gets the time taken by the user to submit this input.
   */
  get TimeTaken(): number {
    return this.timeTaken;
  }

  /**
   * Ctor.
   * @param isCorrect
   * @param blocks
   */
  constructor(isCorrect: boolean, blocks: ActivityInputBlock[]) {
    this.isCorrect = isCorrect;
    this.blocks = blocks;
  }

  /**
   * Sets the time taken by the user to validate
   * this input.
   * @param time
   */
  setTimeTaken(time: number) {
    this.timeTaken = time;
  }
}
