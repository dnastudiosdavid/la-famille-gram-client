import {Component, ElementRef, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {App} from "../../../../model/app";
import {Router} from "@angular/router";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {SolutionsDialogComponent} from "../../../../components/solutions-dialog/solutions-dialog.component";
import {TirettesGame} from "../../model/tirettes/tirettes-game";
import {IAnswerFeedback, ResultState} from "../../model/core/answer-feedback-interface";
import {TirettesResult} from "../../model/tirettes/tirettes-result";
import {GameBaseComponent} from "../../../../views/game-base.component";
import {AppService} from "../../../../services/app.service";
import {AccessibilityService} from "../../../../services/accessibility.service";
import {GlobalActions, makeGlobalAction} from "../../../../model/activity-action";

/**
 * Game view for the tirettes mode.
 */
@Component({
  selector: "app-game-tirettes",
  templateUrl: "./game-tirettes.component.html",
  styleUrls: ["./game-tirettes.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class GameTirettesComponent extends GameBaseComponent implements OnInit, OnDestroy {

  /**
   * The app model.
   */
  app: App;

  /**
   * The game model in progress.
   */
  game: TirettesGame;

  /**
   * A string representation of the selected indexes
   * in the tirette. This value is managed by the jQuery plugin
   * and this component gets it via an event from the tirette
   * component.
   */
  combination: string;

  /**
   * The minimum amount of solutions to find to go to
   * the next sentence.
   */
  minSolutionsToGoNext: number;

  /**
   * The state of the tirette.
   */
  stateInfo: ResultState;

  /**
   * Ctor.
   */
  constructor(private appService: AppService,
              private dialog: MatDialog,
              private ref: ElementRef,
              private router: Router,
              private accessibility: AccessibilityService) {
    super();
  }

  /**
   * Initialization of the component.
   */
  ngOnInit() {
    this.game = <TirettesGame>this.appService.Game;
    if (!this.game) {
      this.router.navigate(["/"]);
    } else {
      this.ref.nativeElement.addEventListener("touchmove", this.onTouchmoveHandler);
      this.minSolutionsToGoNext = TirettesResult.MIN_SOLUTIONS_TO_GO_NEXT;
    }
  }

  /**
   * Destroy.
   */
  ngOnDestroy() {
    this.ref.nativeElement.removeEventListener("touchmove", this.onTouchmoveHandler);
  }

  /**
   * Goes to the next sentence.
   */
  next() {
    this.game.addAction(TirettesGame.Actions.next);
    this.appService.GameService.next();
    this.stateInfo = ResultState.Void;
  }

  /**
   * Validates the current sentence and
   * changes the state.
   */
  validate() {
    const result: IAnswerFeedback = this.game.validate(this.combination);
    this.score.show(result.getScoreChange(), result.getResultState());
    this.stateInfo = result.getResultState();
    this.game.addAction(TirettesGame.Actions.validate, this.stateInfo);

    switch (this.stateInfo) {
      case ResultState.Incorrect:
        this.environment.getHero().schnetz(true);
        break;
      case ResultState.Correct:
        this.environment.getHero().cheer(true);
        break;
    }
  }

  /**
   * Shows the solutions found at the moment.
   */
  showSolutions() {
    const dialogRef: MatDialogRef<SolutionsDialogComponent> = this.dialog.open(SolutionsDialogComponent);
    dialogRef.componentInstance.showSolutions(this.game.CurResult.SolutionsFound);
    this.game.addAction(TirettesGame.Actions.solution);
  }

  /**
   * Handles the event raised by the tirette component.
   */
  onCombinationChanged(combination: string) {
    if (combination !== this.combination) {
      this.game.addAction(TirettesGame.Actions.tirette, this.game.getCurrentSentence().getTextByIndexes(combination), {indexes: combination});
    }
    this.combination = combination;
  }

  /**
   * Handles when the speed synthesis gets used.
   */
  onSpeechSynthesisUsed(): void {
    this.game.getActivity().addAction(makeGlobalAction(GlobalActions.SpeechSynthesis));
    this.accessibility.startSpeech(this.game.getCurrentSentence().getTextByIndexes(this.combination));
  }

  /**
   * Handles the touch move event.
   */
  private onTouchmoveHandler(event: any) {
    event.preventDefault();
  }
}
