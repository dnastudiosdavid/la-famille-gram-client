import {Component, ElementRef, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {SilhouettesGame} from "../../model/silhouettes/silhouettes-game";
import {Router} from "@angular/router";
import {Sentence} from "../../model/core/sentence";
import {Word} from "../../model/core/word";
import {DragulaService} from "ng2-dragula";
import {SilhouettesSentenceUnit} from "../../model/silhouettes/silhouettes-sentence-unit";
import {IAnswerFeedback, ResultState} from "../../model/core/answer-feedback-interface";
import {shuffleArray} from "../../../../app.helpers";
import {Tirette} from "../../model/core/tirette";
import {GameBaseComponent} from "../../../../views/game-base.component";
import {AppService} from "../../../../services/app.service";

/**
 * Game view for the silhouettes mode.
 */
@Component({
  selector: "app-game-silhouettes",
  templateUrl: "./game-silhouettes.component.html",
  styleUrls: ["./game-silhouettes.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class GameSilhouettesComponent extends GameBaseComponent implements OnInit, OnDestroy {

  /**
   * The class container class.
   */
  static readonly CLASS_CONTAINER: string = "view-silhouettes-classes";

  /**
   * The game model in progress.
   */
  game: SilhouettesGame;

  /**
   * The different classes.
   */
  words: Word[];

  /**
   * A correct sentence.
   */
  sentence: SilhouettesSentenceUnit[];

  /**
   * The state of the last given answer.
   */
  stateInfo: ResultState;

  /**
   * Subscription of the drop event of Dragula.
   */
  dropSubscription: any;

  /**
   * Ctor.
   */
  constructor(private appService: AppService,
              private ref: ElementRef,
              private dragulaService: DragulaService,
              private router: Router) {
    super();
    this.words = [];

    // configure Dragula
    dragulaService.createGroup("bag", {
      accepts: function (el, target, source, sibling) {
        return hasClass(target, "empty") || hasClass(target, GameSilhouettesComponent.CLASS_CONTAINER);
      }
    });

    this.dropSubscription = dragulaService.drop("bag").subscribe(({target, source}) => {
      if (!hasClass(target, GameSilhouettesComponent.CLASS_CONTAINER)) {
        target.classList.remove("empty");
      }

      if (!hasClass(source, GameSilhouettesComponent.CLASS_CONTAINER)) {
        source.classList.add("empty");
      }

      this.game.addAction(SilhouettesGame.Actions.drop, this.sentence.map(u => u.ChosenWord[0]?.Value ?? "Aucun").join(" "), {
        words: this.sentence.map(u => u.ChosenWord[0]?.Value ?? null)
      });
    });
  }

  /**
   * Initialization.
   */
  ngOnInit() {
    this.game = <SilhouettesGame>this.appService.Game;
    if (!this.game) {
      this.router.navigate(["/"]);
    } else {

      this.ref.nativeElement.addEventListener("touchmove", this.onTouchmoveHandler);
      this.game.sentenceChanged.subscribe(() => {
        this.setupSentence();
      });
      this.setupSentence();
    }
  }

  /**
   * Destroy.
   */
  ngOnDestroy() {
    if (this.dragulaService.find("bag")) {
      this.dragulaService.destroy("bag");
    }
    if (this.dropSubscription) {
      this.dropSubscription.unsubscribe();
    }
    this.ref.nativeElement.removeEventListener("touchmove", this.onTouchmoveHandler);
  }

  /**
   * Validates the current sentence and
   * changes the state.
   */
  validate() {
    const result: IAnswerFeedback = this.game.validate(this.sentence);
    this.score.show(result.getScoreChange(), result.getResultState());
    this.stateInfo = result.getResultState();
    this.game.addAction(SilhouettesGame.Actions.validate, this.stateInfo);

    switch (this.stateInfo) {
      case ResultState.Incorrect:
        this.environment.getHero().schnetz(true);
        break;
      case ResultState.Correct:
        this.environment.getHero().celebrate(true);
        break;
    }
  }

  /**
   * Goes to the next sentence.
   */
  next() {
    this.stateInfo = ResultState.Void;
    this.game.addAction(SilhouettesGame.Actions.next);
    this.appService.GameService.next();
  }

  /**
   * Sets the sentence up. Builds the necessary
   * objects.
   */
  private setupSentence() {
    this.words = this.game.CurResult.getWords();

    // build the data structure of the answer sentence
    const sentence: Sentence = this.game.getCurrentSentence();
    this.sentence = [];
    const wordsToRemove: number[] = [];
    for (let i = 0; i < sentence.Tirettes.length; i++) {
      const tirette: Tirette = sentence.Tirettes[i];
      const unit: SilhouettesSentenceUnit = new SilhouettesSentenceUnit(sentence.Tirettes[i]);
      if (tirette.GrammaticalClass.IsInvariable) {
        unit.ChosenWord = [this.words[i]];
        unit.lock();
        wordsToRemove.push(i);
      }
      this.sentence.push(unit);
    }

    // remove the invariable words in reverse order
    for (let x = wordsToRemove.length - 1; x >= 0; x--) {
      this.words.splice(wordsToRemove[x], 1);
    }

    this.words = shuffleArray(this.words);
  }

  /**
   * Handles the touch move event.
   */
  private onTouchmoveHandler(event: any) {
    if (hasClass(event.target, "silhouettes-word")) {
      event.preventDefault();
    }
  }
}

/**
 * Function which defines if a given element has
 * a specific class.
 */
export function hasClass(elem, cls) {
  const regexp = new RegExp("(\\s|^)" + cls + "(\\s|$)"),
    trg = (typeof elem.className === "undefined") ? window.event.srcElement : elem;
  return trg.className.match(regexp);
}
