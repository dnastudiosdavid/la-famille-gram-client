import {Component, ElementRef, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {GramClassGame} from "../../model/gram-class/gram-class-game";
import {Router} from "@angular/router";
import {GrammaticalClass} from "../../model/core/grammatical-class";
import {shuffleArray} from "../../../../app.helpers";
import {Sentence} from "../../model/core/sentence";
import {Word} from "../../model/core/word";
import {DragulaService} from "ng2-dragula";
import {GramClassSentenceUnit} from "../../model/gram-class/gram-class-sentence-unit";
import {IAnswerFeedback, ResultState} from "../../model/core/answer-feedback-interface";
import {Tirette} from "../../model/core/tirette";
import {GameBaseComponent} from "../../../../views/game-base.component";
import {AppService} from "../../../../services/app.service";

/**
 * Game view for the game gram mode.
 */
@Component({
  selector: "app-game-gram-class",
  templateUrl: "./game-gram-class.component.html",
  styleUrls: ["./game-gram-class.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class GameGramClassComponent extends GameBaseComponent implements OnInit, OnDestroy {

  /**
   * The class container class.
   */
  static readonly CLASS_CONTAINER: string = "view-game-gram-classes";

  /**
   * The game model in progress.
   */
  game: GramClassGame;

  /**
   * The different classes.
   */
  classes: GrammaticalClass[];

  /**
   * A correct sentence.
   */
  sentence: GramClassSentenceUnit[];

  /**
   * The state of the last given answer.
   */
  stateInfo: ResultState;

  /**
   * Subscription of the drop event of Dragula.
   */
  dropSubscription: any;

  /**
   * Ctor.
   */
  constructor(private appService: AppService,
              private ref: ElementRef,
              private dragulaService: DragulaService,
              private router: Router) {
    super();
    this.classes = [];

    // configure Dragula
    dragulaService.createGroup("bag", {
      accepts: function (el, target, source, sibling) {
        return hasClass(target, "empty") || hasClass(target, GameGramClassComponent.CLASS_CONTAINER);
      }
    });

    this.dropSubscription = dragulaService.drop("bag").subscribe(({target, source}) => {

      if (!hasClass(target, GameGramClassComponent.CLASS_CONTAINER)) {
        target.classList.remove("empty");
      }

      if (!hasClass(source, GameGramClassComponent.CLASS_CONTAINER)) {
        source.classList.add("empty");
      }

      this.game.addAction(GramClassGame.Actions.drop, this.sentence.map(u => u.ChosenClass[0]?.Name ?? "Aucun").join(" "), {
        classes: this.sentence.map(u => u.ChosenClass[0]?.Id ?? null)
      });
    });
  }

  /**
   * Initialization.
   */
  ngOnInit() {
    this.game = <GramClassGame>this.appService.Game;
    if (!this.game) {
      this.router.navigate(["/"]);
    } else {

      this.ref.nativeElement.addEventListener("touchmove", this.onTouchmoveHandler);
      this.game.sentenceChanged.subscribe(() => {
        this.setupSentence();
      });
      this.setupSentence();
    }
  }

  /**
   * Destroy.
   */
  ngOnDestroy() {
    if (this.dragulaService.find("bag")) {
      this.dragulaService.destroy("bag");
    }
    if (this.dropSubscription) {
      this.dropSubscription.unsubscribe();
    }
    document.ontouchmove = null;
  }

  /**
   * Validates the current sentence and
   * changes the state.
   */
  validate() {
    const result: IAnswerFeedback = this.game.validate(this.sentence);
    this.score.show(result.getScoreChange(), result.getResultState());
    this.stateInfo = result.getResultState();
    this.game.addAction(GramClassGame.Actions.validate, this.stateInfo);

    switch (this.stateInfo) {
      case ResultState.Incorrect:
        this.environment.getHero().schnetz(true);
        break;
      case ResultState.Correct:
        this.environment.getHero().cheer(true);
        break;
    }
  }

  /**
   * Goes to the next sentence.
   */
  next() {
    this.stateInfo = ResultState.Void;
    this.game.addAction(GramClassGame.Actions.next);
    this.appService.GameService.next();
  }

  /**
   * Sets the sentence up. Builds the necessary
   * objects.
   */
  private setupSentence() {
    this.classes = [];
    const tirettes: Tirette[] = this.game.getCurrentSentence().Tirettes;
    for (const tirette of tirettes) {
      if (!tirette.GrammaticalClass.IsInvariable) {
        this.classes.push(tirette.GrammaticalClass);
      }
    }
    this.classes = shuffleArray(this.classes);
    const sentence: Sentence = this.game.getCurrentSentence();
    const randomCombination = this.game.CurResult.getCombination();
    const words: Word[] = sentence.getWordsOf(randomCombination);
    this.sentence = [];
    for (let i = 0; i < words.length; i++) {
      const unit: GramClassSentenceUnit = new GramClassSentenceUnit(words[i]);
      const gramClass: GrammaticalClass = tirettes[i].GrammaticalClass;
      if (gramClass.IsInvariable) {
        unit.ChosenClass = [gramClass];
        unit.lock();
      }
      this.sentence.push(unit);
    }
  }

  /**
   * Handles the touch move event.
   */
  private onTouchmoveHandler(event: any) {
    if (hasClass(event.target, "game-gram-class")) {
      event.preventDefault();
    }
  }
}

/**
 * Function which defines if a given element has
 * a specific class.
 */
export function hasClass(elem, cls) {
  const regexp = new RegExp("(\\s|^)" + cls + "(\\s|$)"),
    trg = (typeof elem.className === "undefined") ? window.event.srcElement : elem;
  return trg.className.match(regexp);
}
