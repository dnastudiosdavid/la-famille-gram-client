import {Component, ElementRef, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {Router} from "@angular/router";
import {Sentence} from "../../model/core/sentence";
import {IAnswerFeedback, ResultState} from "../../model/core/answer-feedback-interface";
import {ChainsGame} from "../../model/chains/chains-game";
import {ChainsSentenceUnit} from "../../model/chains/chains-sentence-unit";
import {Chain} from "../../model/core/chain";
import {Word} from "../../model/core/word";
import {GameBaseComponent} from "../../../../views/game-base.component";
import {AppService} from "../../../../services/app.service";

/**
 * Game view for the chains mode.
 */
@Component({
  selector: "app-game-chains",
  templateUrl: "./game-chains.component.html",
  styleUrls: ["./game-chains.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class GameChainsComponent extends GameBaseComponent implements OnInit, OnDestroy {

  /**
   * The game model in progress.
   */
  game: ChainsGame;

  /**
   * The different classes.
   */
  chains: Chain[];

  /**
   * A correct sentence.
   */
  sentence: ChainsSentenceUnit[];

  /**
   * The state of the last given answer.
   */
  stateInfo: ResultState;

  /**
   * Ctor.
   */
  constructor(private appService: AppService,
              private ref: ElementRef,
              private router: Router) {
    super();
    this.chains = [];
  }

  /**
   * Initialization.
   */
  ngOnInit() {
    this.game = <ChainsGame>this.appService.Game;
    if (!this.game) {
      this.router.navigate(["/"]);
    } else {
      this.game.sentenceChanged.subscribe(() => {
        this.setupSentence();
      });
      this.setupSentence();
    }
  }

  /**
   * Destroy.
   */
  ngOnDestroy() {
  }

  /**
   * Validates the current sentence and
   * changes the state.
   */
  validate() {
    const result: IAnswerFeedback = this.game.validate(this.sentence);
    this.score.show(result.getScoreChange(), result.getResultState());
    this.stateInfo = result.getResultState();
    this.game.addAction(ChainsGame.Actions.validate, this.stateInfo);

    switch (this.stateInfo) {
      case ResultState.Incorrect:
        this.environment.getHero().schnetz(true);
        break;
      case ResultState.Correct:
        this.environment.getHero().celebrate(true);
        break;
    }
  }

  /**
   * Goes to the next sentence.
   */
  next() {
    this.stateInfo = ResultState.Void;
    this.game.addAction(ChainsGame.Actions.next);
    this.appService.GameService.next();
  }

  /**
   * Handles when the user selects a chain.
   */
  onChainChangedHandler(): void {
    this.game.addAction(ChainsGame.Actions.chain, this.sentence.map(unit => unit.ChosenChain?.Name ?? "Aucune").join(" "), {
      "chains": this.sentence.map(unit => unit.ChosenChain?.Number ?? null)
    });
  }

  /**
   * Sets the sentence up. Builds the necessary
   * objects.
   */
  private setupSentence() {
    this.chains = this.game.CurResult.getChains(this.game.getCurrentStep() - 1);
    const sentence: Sentence = this.game.getCurrentSentence();
    const randomCombination = this.game.CurResult.getCombination();
    const words: Word[] = sentence.getWordsOf(randomCombination);
    this.sentence = [];
    for (let i = 0; i < words.length; i++) {
      this.sentence.push(new ChainsSentenceUnit(words[i], sentence.Tirettes[i], false));
    }
  }
}

/**
 * Function which defines if a given element has
 * a specific class.
 */
export function hasClass(elem, cls) {
  const regexp = new RegExp("(\\s|^)" + cls + "(\\s|$)"),
    trg = (typeof elem.className === "undefined") ? window.event.srcElement : elem;
  return trg.className.match(regexp);
}
