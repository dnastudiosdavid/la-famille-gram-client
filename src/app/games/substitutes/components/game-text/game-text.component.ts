import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {WordItf} from "../../model/word-interface";
import {ParagraphItf} from "../../model/paragraph-interface";
import {ChainItf} from "../../model/chain-interface";
import {GameBaseComponent} from "../../../../views/game-base.component";
import {IValidationFeedback, SubstitutesGame} from "../../model/substitutes-game";
import {AppService} from "../../../../services/app.service";
import {SubstitutesExercise} from "../../model/substitutes-exercise";
import {SubstitutesUtils} from "../../model/substitutes-utils";
import {AccessibilityService} from "../../../../services/accessibility.service";
import {GlobalActions, makeAccessibleFontAction, makeGlobalAction} from "../../../../model/activity-action";

@Component({
  selector: "app-game-text",
  templateUrl: "./game-text.component.html",
  styleUrls: ["./game-text.component.scss"]
})
export class GameTextComponent extends GameBaseComponent implements OnInit, OnDestroy {

  /**
   * The selected chain.
   */
  chain: ChainItf;

  /**
   * Flag used display / hide the guidelines.
   */
  started: boolean = false;

  /**
   * As its name implies: show the solution.
   */
  showSolution: boolean = false;

  /**
   * An object containing the result of the previous validation.
   */
  feedback: IValidationFeedback;

  /**
   * True if we are in the dyslexia mode.
   */
  dysmode: boolean;

  /**
   * The exercise model.
   */
  exercise: SubstitutesExercise;

  /**
   * The game model.
   */
  game: SubstitutesGame;

  /**
   * Fullscreen element.
   */
  @ViewChild("fs") fs: ElementRef;

  get IsSpeechPaused(): boolean {
    return this.accessibility.IsSpeechPaused;
  }

  get IsSpeechActive(): boolean {
    return this.accessibility.IsSpeechActive;
  }

  /**
   * Ctor.
   */
  constructor(private appService: AppService,
              private accessibility: AccessibilityService) {
    super();
  }

  /**
   * Component initialization. Get the model of the current game
   * and set defaults.
   */
  ngOnInit() {
    this.game = <SubstitutesGame>this.appService.Game;
    this.exercise = <SubstitutesExercise>this.game.getExercise();
    this.selectChain(this.exercise.Chains[0]);
    window.onbeforeunload = () => this.ngOnDestroy();
  }

  /**
   * Releases resources.
   */
  ngOnDestroy() {
    this.stopSpeaking();
  }

  /**
   * Starts the game - this will actually just hide
   * the guidelines.
   */
  start() {
    this.started = true;
  }

  /**
   * Selects a chain to highlight words.
   */
  selectChain(chain: ChainItf) {
    this.chain = chain;
    this.game.addAction(SubstitutesGame.Actions.selectGroup, chain.label, {chain: chain.value});
  }

  /**
   * Selects a word.
   */
  select(word: WordItf) {
    if (this.game.IsSolved || this.showSolution || !word.interactable || this.chain === null) {
      return;
    }
    if (word.chain === this.chain) {
      word.chain = null;
    } else {
      word.chain = this.chain;
    }

    this.game.addAction(SubstitutesGame.Actions.selectWord, word.value, {
      index: this.game.getWordIndex(word),
      chain: word.chain?.value ?? null
    });
  }

  /**
   * Validates the input. We'll get a feedback from
   * the model in return.
   */
  validate() {
    this.feedback = this.game.validate(this.getInput());
    this.fs.nativeElement.scroll(0, 0);
    this.game.addAction(SubstitutesGame.Actions.validate, this.feedback.resultState, {
      correctWords: this.feedback.correctWordsIndexes,
      wrongWords: this.feedback.wrongWordsIndexes,
      step: this.game.CorrectionStep
    });
  }

  /**
   * Gets the background color of a word.
   * TODO: seems like we could REALLY optimize this method by caching the results
   */
  getBackgroundColor(paragraph: ParagraphItf, word: WordItf): string {
    if (this.wordIncludesAny(word, ["?", ".", ":", ","])) {
      return null;
    }
    if (word.interactable) {
      return word.chain ? word.chain.color : null;
    }
    const flat: WordItf[] = paragraph.groups.map(g => g.words)
      .reduce((a, b) => a.concat(b), []);
    const wordIndex: number = flat.indexOf(word);
    const previousIndex: number = wordIndex - 1;
    const nextIndex: number = wordIndex + 1;
    if (previousIndex >= 0 && nextIndex < flat.length) {
      const previousWord: WordItf = flat[previousIndex];
      const nextWord: WordItf = flat[nextIndex];
      if (previousWord.interactable && previousWord.chain && nextWord.interactable && previousWord.chain === nextWord.chain) {
        return previousWord.chain.color;
      }
    }
    return null;
  }

  /**
   * Shows the solution.
   */
  solution() {
    for (const chain in this.exercise.Solution) {
      if (this.exercise.Solution.hasOwnProperty(chain)) {
        const flat: WordItf[] = this.game.Paragraphs.map(p => p.groups)
          .reduce((a, b) => a.concat(b), [])
          .map(g => g.words)
          .reduce((a, b) => a.concat(b), []);
        for (let i = 0; i < flat.length; i++) {
          const word: WordItf = flat[i];
          if (this.exercise.Solution[chain].includes(i)) {
            word.chain = this.exercise.Chains.filter(c => c.value === chain)[0];
          } else if (word.chain && word.chain.value === chain) {
            word.chain = null;
          }
        }
      }
    }
    this.showSolution = true;
    this.game.addAction(SubstitutesGame.Actions.solution);
  }

  /**
   * Continue to game's end panel.
   */
  continue() {
    this.game.end();
  }

  /**
   * Restarts the game.
   */
  restart() {
    this.game.Paragraphs.map(p => p.groups.map(g => g.words.map(w => w.chain = null)));
  }

  /**
   * Returns true if the given word was highlighted and wrong.
   */
  isWordWrong(word: WordItf): boolean {
    return this.feedback && !this.showSolution && this.game.CorrectionStep > 1 && this.feedback.wrongWords.includes(word);
  }

  /**
   * Returns true if the given word was highlighted and correct.
   */
  isWordCorrect(word: WordItf): boolean {
    return this.feedback && !this.showSolution && this.game.CorrectionStep > 1 && this.feedback.correctWords.includes(word);
  }

  /**
   * Starts the speech synthesis.
   */
  speak(): void {
    this.game.getActivity().addAction(makeGlobalAction(GlobalActions.SpeechSynthesis));
    this.accessibility.startSpeech(this.exercise.Text);
  }

  /**
   * Stops the speech synthesis.
   */
  stopSpeaking(): void {
    this.accessibility.stopSpeech();
  }

  /**
   * Toggle the speech synthesis pause state.
   */
  toggleSpeaking(): void {
    this.accessibility.toggleSpeechPause();
  }

  /**
   * Handles when the font changes.
   */
  onFontChangedHandler(): void {
    this.game.getActivity().addAction(makeAccessibleFontAction(this.dysmode));
  }

  /**
   * Gets a data structure representing the player input.
   */
  private getInput(): any {
    if (!this.game.Paragraphs) {
      return [];
    }
    const res: any = {};
    const flat: WordItf[] = SubstitutesUtils.flatten(this.game.Paragraphs);
    for (let i = 0; i < flat.length; i++) {
      const chain: ChainItf = flat[i].chain;
      if (chain) {
        if (!res[chain.value]) {
          res[chain.value] = [];
        }
        res[chain.value].push(i);
      }
    }
    return res;
  }

  /**
   * Returns true if the word includes any needles given.
   */
  private wordIncludesAny(word: WordItf, needles: string[]): boolean {
    for (const needle of needles) {
      if (word.value.includes(needle)) {
        return true;
      }
    }
    return false;
  }
}
