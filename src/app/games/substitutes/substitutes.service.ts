import {Injectable, Type} from "@angular/core";
import {IExercise} from "../../model/exercise-interface";
import {ExerciseHeader} from "../../services/app.service";
import {HttpClient} from "@angular/common/http";
import {SubstitutesExercise} from "./model/substitutes-exercise";
import {GameBaseComponent} from "../../views/game-base.component";
import {GameTextComponent} from "./components/game-text/game-text.component";
import {SubstitutesGame} from "./model/substitutes-game";
import {classToPlain, plainToClass} from "class-transformer";
import {AbstractGameService} from "../../services/game-service-abstract";
import {LocalStorageService} from "../../services/local-storage.service";
import {IGame} from "../../model/game-interface";
import {environment} from "../../../environments/environment";
import {SubstitutesActivity} from "./model/substitutes-activity";

@Injectable({
  providedIn: "root"
})
export class SubstitutesService extends AbstractGameService {

  private static readonly ApiEndpoint: string = `${environment.api_endpoint}/substitutes/p/exercise`;

  /**
   * Game identifier.
   */
  private static readonly GameId: string = "substitutes";

  /**
   * Our game model.
   */
  private game: SubstitutesGame;

  /**
   * The current game being played.
   */
  get Game(): IGame {
    return this.game;
  }

  /**
   * Gets the component's type to inject within PlayComponent
   */
  get ComponentType(): Type<GameBaseComponent> {
    return GameTextComponent;
  }

  /**
   * Ctor.
   */
  constructor(private http: HttpClient,
              protected storageService: LocalStorageService) {
    super(storageService);
    this.exercisesHeaders = this.getDefaultExercises();
  }

  /**
   * Returns the list of exercises.
   */
  getExercisesList(): Promise<IExercise[]> {
    return Promise.resolve(this.exercisesHeaders);
  }

  /**
   * A substitute exercise needs 2 files - one containing the text
   * and another containing the meta (word/groups/...).
   */
  loadExercise(code: string): Promise<IExercise> {
    return new Promise((resolve, reject) => {
      this.http.get(`${SubstitutesService.ApiEndpoint}/${code}`)
        .subscribe((response: any) => {
          const exercise = plainToClass(SubstitutesExercise, response as Object);
          resolve(exercise);
          this.addExerciseCode(SubstitutesService.GameId, exercise);
        }, () => {
          reject();
        });
    });
  }

  /**
   * @see IGameService.hasModes
   */
  hasModes(): boolean {
    return false;
  }

  /**
   * @see IGameService.canLoad
   */
  canLoad(): boolean {
    return true;
  }

  /**
   * Starts a new game.
   */
  startGame(exercise: SubstitutesExercise, hero: string, student: string, mode: string) {
    const g: SubstitutesGame = new SubstitutesGame();
    g.start(exercise, hero, student, "std");
    g.ended.subscribe(() => {
      this.saveScore(exercise.Code, "std", g.getScore());
      this.saveActivity(g.Activity);
    });
    this.setPreviousScores(exercise.Code, "std");
    this.game = g;
    return g;
  }

  /**
   * Sets a game - used when we load a saved game.
   */
  setGame(game: IGame) {
    this.game = <SubstitutesGame>game;
    this.game.ended.subscribe(() => {
      this.saveScore(this.game.Exercise.Code, "std", this.game.getScore());
      this.saveActivity(this.game.Activity);
    });
  }

  /**
   * Not supported currently as there is no step in this exercise.
   */
  next() {
  }

  /**
   * Not supported currently.
   */
  removeExercise(exercise: IExercise): void {
    this.exercisesHeaders = this.exercisesHeaders.filter(c => c.Code !== exercise.Code);
    this.storeCodes(SubstitutesService.GameId, this.exercisesHeaders);
  }

  /**
   * @see IGameService.getDefaultExerciseByDegree
   */
  getDefaultExerciseByDegree(degree: number): string {
    const allEx = ["4_0002", "4_0003", "4_0004", "4_0005", "4_0006", "4_0001", "4_0007"];
    switch (degree) {
      case 4:
        return allEx[1];
      case 5:
        return allEx[2];
      case 6:
        return allEx[3];
      case 7:
        return allEx[4];
      case 8:
        return allEx[5];
      default:
        return allEx[1];
    }
  }

  /**
   * Gets the exercises by default.
   */
  private getDefaultExercises(): ExerciseHeader[] {
    const def: ExerciseHeader[] = [
      new ExerciseHeader("4_0002", "La marmotte n°1", SubstitutesService.GameId, "fr", 4),
      new ExerciseHeader("4_0003", "La marmotte n°2", SubstitutesService.GameId, "fr", 4),
      new ExerciseHeader("4_0004", "Les Ratinos n°1", SubstitutesService.GameId, "fr", 5),
      new ExerciseHeader("4_0005", "Les Ratinos n°2", SubstitutesService.GameId, "fr", 6),
      new ExerciseHeader("4_0006", "Les Ratinos n°3", SubstitutesService.GameId, "fr", 7),
      new ExerciseHeader("4_0001", "Le singe et le crocodile", SubstitutesService.GameId, "fr", 8),
      new ExerciseHeader("4_0007", "L'eau", SubstitutesService.GameId, "fr", 8)
    ];
    return def.concat(this.getCodesArray(SubstitutesService.GameId));
  }

  private saveActivity(activity: SubstitutesActivity): void {
    const code: string = activity.Exercise;
    const url = `${SubstitutesService.ApiEndpoint}/${code}/activity`;
    this.http.post(url, classToPlain(activity)).toPromise();
  }
}
