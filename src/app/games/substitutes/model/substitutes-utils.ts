import {ParagraphItf} from "./paragraph-interface";
import {WordGroupItf} from "./word-group-interface";
import {WordItf} from "./word-interface";

export class SubstitutesUtils {
  static toStructuredText(text: string): ParagraphItf[] {
    if (!text) {
      return [];
    }
    return text.match(/.*/gm)
      .filter(s => s !== "")
      .map(s => {
        const groups: WordGroupItf[] = [];
        // matches[0] is the whole group
        // matches[1] are the opening characters
        // matches[2] is the actual word we want to have
        // matches[3] is the punctuation and spaces
        const regExp = /([«\s]*)([A-Za-zÀ-ÖØ-öø-ÿœ]+)([\s.,?!»"-:;'’]*)/g;
        let matches: string[] = regExp.exec(s);
        let words: WordItf[] = [];

        while (matches != null) {
          this.addWordTo(words, matches[1], false);
          this.addWordTo(words, matches[2], true);
          this.addWordTo(words, matches[3], false);
          const lastMatch = matches[3];
          matches = regExp.exec(s);
          if (lastMatch.includes(" ") || !matches) {
            groups.push({
              words: words
            });
            words = [];
          }
        }

        return <ParagraphItf>{
          groups: groups
        };
      });
  }

  static flatten(paragraphs: ParagraphItf[]) {
    return paragraphs.map(p => p.groups)
      .reduce((a, b) => a.concat(b), [])
      .map(g => g.words)
      .reduce((a, b) => a.concat(b), []);
  }

  /**
   * Adds a word to the given array.
   */
  private static addWordTo(words: WordItf[], value: string, interactable: boolean): void {
    if (value && value !== "") {
      words.push({
        value: value,
        chain: null,
        interactable: interactable
      });
    }
  }
}
