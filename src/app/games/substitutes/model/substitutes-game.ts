import {AbstractGame} from "../../../model/game-abstract";
import {SubstitutesExercise} from "./substitutes-exercise";
import {IExercise} from "../../../model/exercise-interface";
import {SubstitutesGameResult} from "./substitutes-game-result";
import {WordItf} from "./word-interface";
import {plainToInstance, Type} from "class-transformer";
import {ResultState} from "../../sentences/model/core/answer-feedback-interface";
import {ParagraphItf} from "./paragraph-interface";
import {SubstitutesActivity, SubstitutesActivityStep, SubstitutesActivityStepGroup} from "./substitutes-activity";
import {IActivity} from "../../../model/activity-interface";
import {makeAction} from "../../../model/activity-action";
import {SubstitutesUtils} from "./substitutes-utils";

export class SubstitutesGame extends AbstractGame {

  public static readonly Actions = {
    selectGroup: "select-group",
    selectWord: "select-word",
    validate: "validate-input",
    solution: "show-solution"
  };

  private readonly ScorePerWords = 10;
  private readonly PointsPerCorrectionStep = 50;
  private readonly MaxCorrectionStep = 5;

  private score: number = 0;

  private hasEnded: boolean = false;

  /**
   * The amount of words to be found in the game.
   */
  private totalWords: number = 0;

  /**
   * The words already found.
   */
  private foundWords: number = 0;

  /**
   * The amount of correction step - used for scoring.
   */
  private correctionStep: number = 0;

  /**
   * Words structured into paragraphs.
   */
  private paragraphs: ParagraphItf[];

  /**
   * A flat array of all the words.
   */
  private words: WordItf[];

  /**
   * An activity holds all the data related to the
   * progress of a student during the exercise.
   */
  @Type(() => SubstitutesActivity)
  private activity: SubstitutesActivity;

  /**
   * Date at which the game starts.
   */
  @Type(() => Date)
  private stepStart: Date;

  /**
   * The game's exercise.
   */
  @Type(() => SubstitutesExercise)
  protected exercise: IExercise;

  /**
   * The last result produced by the validate method.
   */
  private validationFeedback: IValidationFeedback;

  get Exercise(): SubstitutesExercise {
    return this.exercise as SubstitutesExercise;
  }

  get ValidationFeedback(): IValidationFeedback {
    return this.validationFeedback;
  }

  get FoundWords(): number {
    return this.foundWords;
  }

  get CorrectionStep(): number {
    return this.correctionStep;
  }

  get TotalWords(): number {
    return this.totalWords;
  }

  get IsSolved(): boolean {
    return this.validationFeedback
      && this.foundWords === this.totalWords
      && this.validationFeedback.wrongWords.length === 0;
  }

  get Paragraphs(): ParagraphItf[] {
    return this.paragraphs;
  }

  /**
   * An array of all the words flatten.
   */
  get Words(): WordItf[] {
    return this.words;
  }

  get Activity(): SubstitutesActivity {
    return this.activity;
  }

  /**
   * Crates a new instance of this game based on raw data.
   */
  static load(raw: Object): SubstitutesGame {
    return plainToInstance(SubstitutesGame, raw);
  }

  start(exercise: IExercise, hero: string, student: string, mode: string) {
    super.start(exercise, hero, student, mode);
    this.hasEnded = false;
    const solution = this.Exercise.Solution;
    this.stepStart = new Date();

    let total: number = 0;
    for (const key in solution) {
      if (solution.hasOwnProperty(key)) {
        total += solution[key].length;
      }
    }
    this.totalWords = total;
    this.paragraphs = this.Exercise.getStructuredText();
    this.words = SubstitutesUtils.flatten(this.paragraphs);
    this.activity = new SubstitutesActivity(student, this.Exercise.Code, this.Exercise.Text);
  }

  end() {
    this.hasEnded = true;
    this.computeScore(true);
    this.activity.complete();
    this.ended.emit(null);
  }

  /**
   * @inheritDoc
   */
  getCurrentStep(): number {
    return this.foundWords;
  }

  /**
   * @inheritDoc
   */
  getStepsAmount(): number {
    return this.totalWords;
  }

  /**
   * @inheritDoc
   */
  isInProgress() {
    return !this.hasEnded;
  }

  /**
   * @inheritDoc
   */
  isScoreLocked(): boolean {
    return false;
  }

  /**
   * @inheritDoc
   */
  getProgressLabel(): string {
    return "Mots";
  }

  /**
   * @inheritDoc
   */
  getScore(): number {
    return this.score;
  }

  /**
   * @inheritDoc
   */
  getActivity(): IActivity {
    return this.activity;
  }

  validate(input: any): IValidationFeedback {
    const solution: any = this.Exercise.Solution;
    this.foundWords = 0;
    const result: IValidationFeedback = {
      wrongWords: [],
      wrongWordsIndexes: [],
      correctWords: [],
      correctWordsIndexes: [],
      missingWordsIndexes: [],
      scoreDelta: 0,
      resultState: ResultState.Correct
    };

    const groups = [];
    const step = new SubstitutesActivityStep(Math.round((new Date().getTime() - this.stepStart.getTime()) / 1000), groups);
    this.stepStart = new Date();

    for (const groupKey in input) {
      if (input.hasOwnProperty(groupKey)) {
        if (solution.hasOwnProperty(groupKey)) {
          const chain = this.Exercise.Chains.find(c => c.value === groupKey);
          const groupLabel = chain.label;
          const groupColor = chain.color;
          const groupCorrectIndexes: number[] = [];
          const groupWrongIndexes: number[] = [];

          for (const index of input[groupKey]) {
            if (solution[groupKey].includes(index)) {
              result.correctWords.push(this.getWordAt(index));
              result.correctWordsIndexes.push(index);
              groupCorrectIndexes.push(index);
            } else {
              result.wrongWords.push(this.getWordAt(index));
              result.wrongWordsIndexes.push(index);
              groupWrongIndexes.push(index);
            }
          }

          const groupMissingIndexes: number[] = solution[groupKey].filter(idx => groupCorrectIndexes.indexOf(idx) === -1);
          groups.push(new SubstitutesActivityStepGroup(groupLabel, groupColor, groupCorrectIndexes, groupWrongIndexes, groupMissingIndexes));
        }
      }
    }

    this.activity.addStep(step);
    const prevScore = this.score;
    this.foundWords = result.correctWords.length;
    this.validationFeedback = result;
    this.computeScore(this.IsSolved);
    result.scoreDelta = this.score - prevScore;
    result.resultState = Math.sign(this.score - prevScore) < 0 ? ResultState.Incorrect : ResultState.Correct;
    if (!this.IsSolved) {
      this.correctionStep++;
    }
    return result;
  }

  /**
   * Returns the index of the given word.
   */
  getWordIndex(word: WordItf): number {
    return this.words.indexOf(word);
  }

  /**
   * Provides a convenient way of adding an action to the activity without
   * specifying time and scope each time.
   */
  addAction(action: string, outcome: string = "", data: any = null): void {
    this.activity.addAction(makeAction("substitutes", action, outcome, data));
  }

  /**
   * Computes the score.
   */
  private computeScore(fullResults: boolean): void {

    // 10 points per word found
    const wordsFoundScore = this.foundWords * this.ScorePerWords;
    const wordsFoundScoreMax = this.totalWords * this.ScorePerWords;

    // 100 points per correction step - max 3 steps
    const correctionStepScore = Math.max(this.MaxCorrectionStep - this.CorrectionStep, 0) * this.PointsPerCorrectionStep;
    const correctionStepScoreMax = this.MaxCorrectionStep * this.PointsPerCorrectionStep;

    if (fullResults) {
      this.results = [];
      this.addResult(new SubstitutesGameResult("Mots trouvés", wordsFoundScoreMax, wordsFoundScore));
      this.addResult(new SubstitutesGameResult("Bonus de correction", correctionStepScoreMax, correctionStepScore));
    }

    this.score = wordsFoundScore + (fullResults ? correctionStepScore : 0);
  }

  private getWordAt(index: number): WordItf {
    return this.words[index];
  }
}

export interface IValidationFeedback {
  wrongWords: WordItf[];
  wrongWordsIndexes: number[];
  correctWords: WordItf[];
  correctWordsIndexes: number[];
  missingWordsIndexes: number[];
  scoreDelta: number;
  resultState: ResultState;
}
