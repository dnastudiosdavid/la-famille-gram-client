import {WordItf} from "./word-interface";
import {Exclude, Type} from "class-transformer";
import {SubstitutesUtils} from "./substitutes-utils";
import {ActivityAbstract} from "../../../model/activity-abstract";

export class SubstitutesActivity extends ActivityAbstract {

  @Exclude()
  private words: WordItf[];

  @Type(() => SubstitutesActivityStep)
  private readonly steps: SubstitutesActivityStep[] = [];

  private readonly text: string;

  get Text(): string {
    return this.text;
  }

  get Steps(): SubstitutesActivityStep[] {
    return this.steps;
  }

  constructor(student: string,
              exerciseCode: string,
              text: string) {
    super(student, exerciseCode);
    this.text = text;
    this.words = SubstitutesUtils.flatten(SubstitutesUtils.toStructuredText(this.text));
  }

  addStep(step: SubstitutesActivityStep): void {
    this.steps.push(step);
  }

  getCompletionRate(): number {
    return Math.round(100 * this.steps.reduce((acc, step) => step.getCompletionRate() > acc ? step.getCompletionRate() : acc, 0));
  }

  getErrorRate(): number {
    return Math.round(100 * this.steps.reduce((acc, step) => acc + step.getErrorRate(), 0) / this.steps.length);
  }

  getWordAt(index: number): string {
    if (!this.words || this.words.length === 0) {
      this.words = SubstitutesUtils.flatten(SubstitutesUtils.toStructuredText(this.text));
    }
    return this.words[index] ? this.words[index].value : "";
  }
}

export class SubstitutesActivityStep {

  private readonly time: number = 0;

  @Type(() => SubstitutesActivityStepGroup)
  private readonly groups: SubstitutesActivityStepGroup[] = [];

  get Time(): number {
    return this.time;
  }

  get Groups(): SubstitutesActivityStepGroup[] {
    return this.groups;
  }

  get IsCorrect(): boolean {
    return this.groups.filter(g => !g.IsCorrect).length === 0;
  }

  constructor(time: number,
              groups: SubstitutesActivityStepGroup[]) {
    this.time = time;
    this.groups = groups;
  }

  getCompletionRate(): number {
    return this.groups.reduce((acc, group) => acc + group.getCompletionRate(), 0) / Math.max(this.groups.length, 1);
  }

  getErrorRate(): number {
    return this.groups.reduce((acc, group) => acc + group.getErrorRate(), 0) / Math.max(this.groups.length, 1);
  }
}

export class SubstitutesActivityStepGroup {

  get Name(): string {
    return this.name;
  }

  get Color(): string {
    return this.color;
  }

  get Correct(): number [] {
    return this.correct;
  }

  get Missing(): number[] {
    return this.missing;
  }

  get Wrong(): number[] {
    return this.wrong;
  }

  get IsCorrect(): boolean {
    return this.wrong.length === 0 && this.missing.length === 0;
  }

  constructor(private name: string,
              private color: string,
              private correct: number[],
              private wrong: number[],
              private missing: number[]) {
  }

  getCompletionRate(): number {
    return this.correct.length / (this.correct.length + this.missing.length);
  }

  getErrorRate(): number {
    return this.wrong.length / (this.correct.length + this.missing.length);
  }
}
