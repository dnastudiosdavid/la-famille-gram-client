import {IScoreComponent} from "../../../model/score-component-interface";

export class SubstitutesGameResult implements IScoreComponent {

  public getLabel(): string {
    return this.label;
  }

  public getMaxScore(): number {
    return this.maxScore;
  }

  public getScore(): number {
    return this.score;
  }

  constructor(private label: string,
              private maxScore: number,
              private score: number) {
  }
}
