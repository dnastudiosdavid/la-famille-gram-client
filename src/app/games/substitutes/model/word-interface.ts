import {ChainItf} from "./chain-interface";

export interface WordItf {
  value: string;
  chain: ChainItf;
  interactable: boolean;
}

