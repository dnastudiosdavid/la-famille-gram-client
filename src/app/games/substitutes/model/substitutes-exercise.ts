import {AbstractExercise} from "../../../model/exercise-abstract";
import {ParagraphItf} from "./paragraph-interface";
import {Type} from "class-transformer";
import {SubstitutesUtils} from "./substitutes-utils";

export class SubstitutesExercise extends AbstractExercise {

  get Solution(): any {
    return this.solution;
  }

  get Chains(): IChain[] {
    return this.chains;
  }

  get Text(): string {
    return this.rawText;
  }

  get Title(): string {
    return this.title;
  }

  private title: string;

  private solution: any;

  @Type(() => IChain)
  private chains: IChain[];

  private rawText: string;

  /**
   * Non-words: space, . ! ? - _
   */
  getStructuredText(): ParagraphItf[] {
    return SubstitutesUtils.toStructuredText(this.rawText);
  }

  init(data: string): void {
    this.rawText = data;
  }
}

export class IChain {
  value: string;
  label: string;
  color: string;
}
