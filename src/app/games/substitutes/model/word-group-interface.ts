import {WordItf} from "./word-interface";

export interface WordGroupItf {
  words: WordItf [];
}
