/**
 * The state info represents the state in which
 * the tirette is.
 */
export enum StateInfo {
  Correct = "correct",
  Incorrect = "incorrect",
  Already = "already",
  Clean = "clean"
}

/**
 * This class holds the constants of the app.
 */
export class Constants {
}
