import {animate, style, transition, trigger} from "@angular/animations";

export const bumpAnimation = trigger("bumpAnimation", [
  transition("* <=> *", [
    animate("90ms", style({transform: "scale(.9)"})),
    animate("90ms", style({transform: "scale(1.05)"})),
    animate("90ms", style({transform: "scale(1)"}))
  ])
]);
