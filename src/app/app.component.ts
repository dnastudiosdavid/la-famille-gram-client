import {Component, OnInit} from "@angular/core";
import {LayoutService} from "./services/layout.service";
import {initCursorTracker} from "./model/activity-action";

/**
 * This is the main component of the app module.
 */
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {

  constructor(private layoutService: LayoutService) {
  }

  ngOnInit() {
    this.layoutService.Settings$.subscribe(settings => {
      if (settings.contained) {
        document.body.classList.add("contained");
      }
    });
    initCursorTracker();
  }
}
