/**
 * Shuffles an cArray and return a copy of it.
 * @param array
 * @returns {any}
 */
export function shuffleArray(array) {
  const cArray = array.slice();
  let counter = cArray.length;

  // While there are elements in the cArray
  while (counter > 0) {
    // Pick a random index
    const index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    const temp = cArray[counter];
    cArray[counter] = cArray[index];
    cArray[index] = temp;
  }

  return cArray;
}
