(function ($, window, document) {

    "use strict";

    // window and document are passed through as local variables rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "tirettes",
        defaults = {
            tirettesSelector: ".tirette",
            onChanged: null
        };

    // the plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this._$document = $(document);
        this._$draggedElement = null;
        this._previousY = null;
        this._$tirettes = null;
        this.init();
    }

    // avoid Plugin.prototype conflicts by
    // extending it
    $.extend(Plugin.prototype, {
        init: function () {
            this.build();
            this.registerEvents();
            $("html, body").animate({scrollTop: $(document).height()}, 1000);
        },

        /**
         * Releases the plugin.
         */
        release: function () {
            this._$document.off('touchend mouseup mousemove touchmove');
        },

        /**
         * Builds the plugin.
         */
        build: function () {
        },

        /**
         * Registers all the events.
         */
        registerEvents: function () {

            var self = this;

            this._$tirettes = $(this.element).find(this.settings.tirettesSelector);
            this._$tirettes.each(function (index, item) {
                var $item = $(item);

                // randomly position the tirette element
                var oneElementHeight = parseInt($item.find('li:first-child').css('height'));
                var index = Math.floor(Math.random() * $item.find('li').length);
                $item.css('margin-top', -index * oneElementHeight);
                self.setSelectedIndexOf($item, index);

                /**
                 * Handles when the mouse or touch is down.
                 */
                $item.on('touchstart mousedown', function (e) {
                    self.startDragElement($item, e.pageY);
                });
            });

            // raise the event after randomly
            // placing the items
            if (this.settings.onChanged !== null) {
                this.settings.onChanged(this.getCombination());
            }

            /**
             * Handles when the mouse or the touch is up.
             */
            this._$document.on('touchend mouseup', function (e) {
                self.releaseDrag();
            });

            /**
             * Handles when the mouse moves. If any
             * element is being dragged, update its position.
             */
            this._$document.on('mousemove', function (e) {
                self.tryDrag(e.pageY);
            });

            /**
             * Handles the move of the touch.
             */
            this._$document.on('touchmove', function (e) {
                var touch = e.touches[0];
                if (self.tryDrag(touch.pageY)) {
                    e.preventDefault();
                }
            });
        },

        /**
         * Starts dragging the given element.
         * @param $element
         * @param pageTop
         */
        startDragElement: function ($element, pageTop) {
            this._$draggedElement = $element;
            this._previousY = pageTop;
        },

        /**
         * Stops dragging.
         */
        releaseDrag: function () {
            if (this._$draggedElement !== null) {
                var oneElementHeight = parseInt(this._$draggedElement.find('li:first-child').css('height'));
                var currentTop = parseInt(this._$draggedElement.css('margin-top'));
                var index = Math.abs(Math.round(currentTop / oneElementHeight));
                this._$draggedElement.css('margin-top', -index * oneElementHeight);
                this.setSelectedIndexOf(this._$draggedElement, index);
                // snap the element at any rank
                this._$draggedElement = null;
                this._previousY = null;
                if (this.settings.onChanged !== null) {
                    this.settings.onChanged(this.getCombination());
                }
            }
        },

        /**
         * Sets the selected index of the given list of elements.
         * @param $element
         * @param index
         */
        setSelectedIndexOf: function ($element, index) {
            $element.data('index', index);
        },

        /**
         * Gets the combination represented in a string format. A
         * combination is just a chain of indexes.
         * @returns {string}
         */
        getCombination: function () {
            var combination = "";
            this._$tirettes.each(function (index, item) {
                combination += $(item).data('index');
            });
            return combination;
        },

        /**
         * Tries to drag the current selected
         * element if any.
         *
         * @param pageTop the top value of the input
         * @return {boolean}
         */
        tryDrag: function (pageTop) {
            if (this._$draggedElement !== null) {
                // having the value set to null first
                // prevents the element from jumping to
                // the first position
                if (this._previousY !== null) {
                    this.moveElementTo(this._$draggedElement, parseInt(this._$draggedElement.css('margin-top')) + pageTop - this._previousY);
                }
                this._previousY = pageTop;
                return true;
            }
            return false;
        },

        /**
         * Tries to move the given element to
         * the targeted top target.
         *
         * This method will ensure that the element
         * doesn't go outside the bounds.
         */
        moveElementTo: function ($element, topTarget) {
            var height = parseInt($element.css('height'));
            var oneElementHeight = parseInt($element.find('li:first-child').css('height'));
            $element.css('margin-top', this.clamp(topTarget, -height + oneElementHeight, 0));
        },

        /**
         * Clamps the given value between min (included) and max (included).
         * @param value the value to clamp
         * @param min the min value
         * @param max the max value
         * @returns {int} the clamped value
         */
        clamp: function (value, min, max) {
            if (max < min) {
                console.warn("I can't clamp the given value because the max is below the min value.");
                return value;
            }
            if (value < min) {
                value = min;
            } else if (value > max) {
                value = max;
            }
            return value;
        }
    });

    // a really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return new Plugin(this, options);
    };

})(jQuery, window, document);