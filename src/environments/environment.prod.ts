export const environment = {
  api_endpoint: "https://api.lafamillegram.ch/api",
  languagesEndpoint: "https://api.lafamillegram.ch/languages.json",
  production: true
};
