export const environment = {
  api_endpoint: "https://test-api.lafamillegram.ch/api",
  languagesEndpoint: "https://test-api.lafamillegram.ch/languages.json",
  production: true
};
