# Tirettes - HEP FR

This project uses [Angular CLI](https://github.com/angular/angular-cli) to build, run a dev server and generate components.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding


## BuildRun `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.


Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Build and access documentation

Run `npm install -g compodoc` then `compodoc -p src/tsconfig.app.json` to generate the documentation. Finally, run `compodoc -s` to serve the documentation on local port 8080. 
